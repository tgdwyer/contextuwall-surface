# ContextuWall Client

An implementation of the ContextuWall client for touch-enabled Windows devices.

## License ##
This project is Copyright Monash University, 2016 licensed under the [MIT License](https://opensource.org/licenses/MIT).

### System Requirements ###

* Windows 8 or higher (8.1 recommended)
* .NET Framework (4.5 recommended)

##### For Development #####

* Visual Studio 2013 (Update 3+ recommended)

### Installation ###

* 'Master' branch contains latest stable release.
* 'Cobalt' branch contains latest _usually stable_ dev release.
* No installation required, but currently this is only a test release.
* Run the file: 'contextuwall-surface2/contextuwall-surface2/bin/Debug/ContextuWall.exe'

##### For Development #####

* Open the project file: 'contextuwall-surface2/contextuwall-surface2/ContextuWall.csproj' in Visual Studio.
* Any changes made to the scripts get compiled into the aforementioned .exe when you run the project for debugging.