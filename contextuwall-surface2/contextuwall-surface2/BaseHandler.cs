﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace ContextuWall {
    //http://www.codeproject.com/Articles/607234/Using-Windows-Interaction-Con
    public abstract class BaseHandler : IDisposable, IInteractionHandler {
        IntPtr _context;
        int _lastFrameID = -1;
        private bool disposed = false;

        HashSet<int> _activePointers = new HashSet<int>();

        public BaseHandler() {
            _context = Win32.CreateInteractionContext(this, SynchronizationContext.Current);
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    //Dispose of managed resources. (ie. Other 'Disposable' objects referenced by this object.)
                }
                Win32.DisposeInteractionContext(_context);
                _context = IntPtr.Zero;

                disposed = true;
            }
        }

        public IntPtr Context {
            get { return _context; }
        }

        void IInteractionHandler.ProcessInteractionEvent(InteractionOutput output) {
            ProcessEvent(output);
        }

        internal abstract void ProcessEvent(InteractionOutput output);


        public void ProcessPointerFrames(int pointerID, int frameID) {
            if (_lastFrameID != frameID) {
                _lastFrameID = frameID;
                int entriesCount = 0;
                int pointerCount = 0;
                if (!Win32.GetPointerFrameInfoHistory(pointerID, ref entriesCount, ref pointerCount, IntPtr.Zero)) {
                    Win32.CheckLastError();
                }
                Win32.POINTER_INFO[] piArr = new Win32.POINTER_INFO[entriesCount * pointerCount];
                if (!Win32.GetPointerFrameInfoHistory(pointerID, ref entriesCount, ref pointerCount, piArr)) {
                    Win32.CheckLastError();
                }
                IntPtr hr = Win32.ProcessPointerFramesInteractionContext(_context, entriesCount, pointerCount, piArr);
                if (Win32.FAILED(hr)) {
                    Debug.WriteLine("ProcessPointerFrames failed: " + Win32.GetMessageForHR(hr));
                }
            }
        }

        public HashSet<int> ActivePointers {
            get { return _activePointers; }
        }

        public void AddPointer(int pointerID) {
            try {
                Win32.AddPointerInteractionContext(_context, pointerID);
                _activePointers.Add(pointerID);
            } catch { }
        }

        public void RemovePointer(int pointerID) {
            try {
                Win32.RemovePointerInteractionContext(_context, pointerID);
                _activePointers.Remove(pointerID);
            } catch { }
        }

        public void StopProcessing() {
            try {
                foreach (int pointerID in _activePointers) {
                    Win32.RemovePointerInteractionContext(_context, pointerID);
                }
            } catch { }
            _activePointers.Clear();
            Win32.StopInteractionContext(_context);
        }

        ~BaseHandler() {
            Dispose(false);
        }
    }
}
