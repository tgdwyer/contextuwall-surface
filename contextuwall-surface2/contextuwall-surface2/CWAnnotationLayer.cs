﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace ContextuWall {
    public interface ICWAnnotationLayer {
        string m_ownerID { get; }
        Boolean m_visible { get; set; }
        Bitmap getCommittedCopy();
        Bitmap getCurrentStroke();
        void clear();
        void commit();
    }
    public class CWAnnotationLayer : ICWAnnotationLayer {
        public CWUIForm m_uiForm;

        public string m_ownerID { get; set; }
        public CWImage m_image;
        //private Bitmap m_layer { get; set; } // image to be annotated
        //private Bitmap m_layer_currentStroke;  // the annotation(s)
        public Bitmap m_layer { get; set; } // image to be annotated
        public Bitmap m_layer_currentStroke;  // the annotation(s)
        private Stack<Bitmap> m_history = new Stack<Bitmap>();
        public Rectangle m_rect; // varies with image's aspect ratio.
        public bool m_visible { get; set; }

        private static readonly Object m_propertyLock = new Object();

        private float m_scale; // scaling of the annotation layer

        public CWAnnotationLayer(CWUIForm uiForm, CWImage image) {
            this.m_uiForm = uiForm;
            this.m_ownerID = uiForm.m_cwNetwork.m_socketID;
            this.m_image = image;

            this.m_rect = computeLayerRect();
            this.m_layer = new Bitmap(m_rect.Width, m_rect.Height, PixelFormat.Format32bppArgb);
            this.m_layer_currentStroke = new Bitmap(m_rect.Width, m_rect.Height, PixelFormat.Format32bppArgb);
            //using(Graphics g = Graphics.FromImage(m_layer)) {
            //    g.Clear(Color.Transparent);
            //}
            this.m_visible = true;
        }

        // For annotation layers that come in from the server.
        public CWAnnotationLayer(CWUIForm uiForm, CWImage image, string ownerID, Bitmap bitmap) {
            this.m_uiForm = uiForm;
            this.m_ownerID = ownerID;
            this.m_image = image;
            this.m_rect = computeLayerRect();
            this.m_layer = new Bitmap(bitmap);
            this.m_layer_currentStroke = new Bitmap(m_rect.Width, m_rect.Height, PixelFormat.Format32bppArgb);
            this.m_visible = true;
        }

        //private Rectangle computeLayerRect() {
        public Rectangle computeLayerRect() {
            Rectangle annotationRect = m_uiForm.m_cwWallPanel.m_annotationRect;
            //float scale = Math.Min(annotationRect.Width / (float) m_image.m_width, annotationRect.Height / (float) m_image.m_height);
            m_scale = Math.Min(annotationRect.Width / (float) m_image.m_width, annotationRect.Height / (float) m_image.m_height);
            int annotationWidth = (int) (m_image.m_width * m_scale);
            int annotationHeight = (int) (m_image.m_height * m_scale);
            int annotationLeft = (annotationRect.Width - annotationWidth) / 2;
            int annotationTop = (annotationRect.Height - annotationHeight) / 2;
            return new Rectangle(annotationLeft, annotationTop, annotationWidth, annotationHeight);
        }

        public Bitmap getCurrentStroke() {
            return m_layer_currentStroke;
        }
        public Bitmap getCommittedCopy() {
            return m_layer;
        }

        public void drawDot(Point where) {
            using (Graphics g = Graphics.FromImage(m_layer_currentStroke)) {
                g.CompositingMode = CompositingMode.SourceCopy;
                Pen pen = m_uiForm.m_cwAnnotationToolbox.m_pen;
                float penWidth = pen.Width;
                pen.Width = pen.Width * m_scale; // scale pen according to scaling of annotation layer
                g.FillEllipse(pen.Brush, where.X - pen.Width / 2, where.Y - pen.Width / 2, pen.Width, pen.Width);
                pen.Width = penWidth; // reset pen width
            }
        }

        public void drawLine(Point from, Point to) {
            using (Graphics g = Graphics.FromImage(m_layer_currentStroke)) {
                g.CompositingMode = CompositingMode.SourceCopy;
                Pen pen = m_uiForm.m_cwAnnotationToolbox.m_pen;
                float penWidth = pen.Width;
                pen.Width = pen.Width * m_scale; // scale pen according to scaling of annotation layer
                //g.DrawLine(m_uiForm.m_cwAnnotationToolbox.m_pen, from, to);
                g.DrawLine(pen, from, to);
                pen.Width = penWidth; // reset pen width
            }
        }

        // Replaces the local annotation with one coming from the server.
        public void update(Bitmap image) {
            lock (m_propertyLock) {
                m_layer = new Bitmap(image);
            }
        }

        public void clear() {
            lock (m_propertyLock) {
                m_history.Push(new Bitmap(m_layer));
                m_layer = new Bitmap(m_rect.Width, m_rect.Height, PixelFormat.Format32bppArgb);

                m_image.flattenAnnotations();
                m_uiForm.m_cwNetwork.sendAnnotation(this);
            }
        }

        public void commit() {
            lock (m_propertyLock) {
                m_history.Push(new Bitmap(m_layer));
                using (Graphics graphics = Graphics.FromImage(m_layer)) {
                    graphics.DrawImage(m_layer_currentStroke, Point.Empty);
                }
                m_layer_currentStroke = new Bitmap(m_rect.Width, m_rect.Height, PixelFormat.Format32bppArgb);

                m_image.flattenAnnotations();
                m_uiForm.m_cwNetwork.sendAnnotation(this);
            }
        }

        public void undo() {
            lock (m_propertyLock) {
                if (m_history.Count > 0) {
                    m_layer = m_history.Pop();

                    m_image.flattenAnnotations();
                    m_uiForm.m_cwNetwork.sendAnnotation(this);
                }
            }
        }
    }
}
