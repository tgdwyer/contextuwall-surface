﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace ContextuWall {
    public class CWAnnotationMode : BaseHandler {
        public CWUIForm m_uiForm;

        public bool active {
            get {
                return m_myAnnotationsLayer != null;
            }
        }

        public CWUIAnnotationToolbox.PenChoice m_penChoice = CWUIAnnotationToolbox.PenChoice.Pencil;
        public Color m_penColour = Color.Blue;

        //private Bitmap m_canvas = null;  // canvas showing the annotation area
        public Bitmap m_canvas = null; // canvas showing the annotation area
        private bool canvasIsDirty;

        private Bitmap m_fullImage = null;
        private CWAnnotationLayer m_myAnnotationsLayer = null;

        private readonly Object m_propertyLock = new Object();

        private byte m_opacity;

        public CWAnnotationMode(CWUIForm uiForm) {
            this.m_uiForm = uiForm;
            enableInteraction();
        }

        public void begin(CWImage image) {
            m_opacity = m_uiForm.m_opacity;
            m_uiForm.invokeUpdateOpacity(Math.Max((byte) 244, m_opacity));
            Rectangle annotationRect = m_uiForm.m_cwWallPanel.m_annotationRect;

            // Fetch or create new annotation layer.
            m_myAnnotationsLayer = (CWAnnotationLayer) image.getAnnotationWithOwnerID(m_uiForm.m_cwNetwork.m_socketID);
            if (m_myAnnotationsLayer == null) {
                m_myAnnotationsLayer = new CWAnnotationLayer(m_uiForm, image);
                image.addAnnotation(m_myAnnotationsLayer);
            }
            else {
                // it might be necessary to resize the annotation layer, e.g., when the aspect ratio has been changed
                if (m_myAnnotationsLayer.m_rect.Width != annotationRect.Width || m_myAnnotationsLayer.m_rect.Height != annotationRect.Height) {
                    m_myAnnotationsLayer.m_rect = m_myAnnotationsLayer.computeLayerRect();
                    m_myAnnotationsLayer.m_layer = resizeBitmap(m_myAnnotationsLayer.m_layer);
                    m_myAnnotationsLayer.m_layer_currentStroke = resizeBitmap(m_myAnnotationsLayer.m_layer_currentStroke);
                }
            }

            // Prepare the canvas.
            m_canvas = new Bitmap(annotationRect.Width, annotationRect.Height, PixelFormat.Format32bppArgb);
            m_fullImage = image.getFullResolution();
            paintCanvas();

            // Open annotation toolbox.
            //m_uiForm.m_cwAnnotationToolbox = new CWUIAnnotationToolbox(m_uiForm);
            m_uiForm.m_cwAnnotationToolbox.Show(m_uiForm);
            m_uiForm.m_cwAnnotationToolbox.Location = m_uiForm.m_cwWallPanel.m_annotationToolboxDock;
        }

        public void end() {
            m_uiForm.invokeUpdateOpacity(m_opacity);
            // Close annotation toolbox.
            if (m_uiForm.m_cwAnnotationToolbox != null) {
                // don't close annotation toolbox just hide
                // m_uiForm.m_cwAnnotationToolbox.invokeClose();
                m_uiForm.m_cwAnnotationToolbox.Hide();
            }

            if (m_myAnnotationsLayer != null) {
                // Redraw the form, so that the annotation shows up.
                m_myAnnotationsLayer.m_image.flattenAnnotations();
                m_myAnnotationsLayer.m_image.reposition();
                m_uiForm.Invalidate();
            }
            m_myAnnotationsLayer = null;

            lock (m_propertyLock) {
                if (m_fullImage != null) {
                    m_fullImage.Dispose();
                }
                m_fullImage = null;
            }
        }

        public void undo() {
            lock (m_propertyLock) {
                m_myAnnotationsLayer.undo();
                canvasIsDirty = true;
            }
            m_uiForm.Invalidate();
        }

        public ICWImage getImageBeingAnnotated() {
            return m_myAnnotationsLayer.m_image;
        }

        public void setFullImage(Bitmap b) {
            lock (m_propertyLock) {
                m_fullImage = b;
                canvasIsDirty = true;
            }
        }

        public void clearAnnotation() {
            m_myAnnotationsLayer.clear();
            lock (m_propertyLock) {
                canvasIsDirty = true;
            }
        }

        public void clearAllAnnotations() {
            foreach (ICWAnnotationLayer layer in m_myAnnotationsLayer.m_image.getAnnotations()) {
                layer.clear();
            }
            lock (m_propertyLock) {
                canvasIsDirty = true;
            }
        }

        private void enableInteraction() {
            //http://www.codeproject.com/Articles/607234/Using-Windows-Interaction-Con
            Win32.INTERACTION_CONTEXT_CONFIGURATION[] cfg = new Win32.INTERACTION_CONTEXT_CONFIGURATION[] {
                // TAPPING (Left click)
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.TAP,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.TAP |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.TAP_DOUBLE
                    ),
                // MOVING
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.MANIPULATION,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_X |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_Y
                    ),
            };
            Win32.SetInteractionConfigurationInteractionContext(Context, cfg.Length, cfg);
        }

        private Point lastDrawPoint;
        internal override void ProcessEvent(InteractionOutput output) {
            Rectangle annotationRect = m_uiForm.m_cwWallPanel.m_annotationRect;
            switch (output.Data.Interaction) {
                case Win32.INTERACTION.TAP:
                    // Draw a dot..
                    lastDrawPoint = new Point((int) output.Data.X - m_myAnnotationsLayer.m_rect.Left - annotationRect.Left, (int) output.Data.Y - m_myAnnotationsLayer.m_rect.Top - annotationRect.Top);
                    m_myAnnotationsLayer.drawDot(lastDrawPoint);
                    m_myAnnotationsLayer.commit();
                    canvasIsDirty = true;
                    break;
                case Win32.INTERACTION.MANIPULATION:
                    //Console.WriteLine("Manipulation!");
                    if (output.IsBegin()) {
                        lastDrawPoint = new Point((int) output.Data.X - m_myAnnotationsLayer.m_rect.Left - annotationRect.Left, (int) output.Data.Y - m_myAnnotationsLayer.m_rect.Top - annotationRect.Top);
                    }
                    else {
                        Point newDrawPoint = new Point((int) output.Data.X - m_myAnnotationsLayer.m_rect.Left - annotationRect.Left, (int) output.Data.Y - m_myAnnotationsLayer.m_rect.Top - annotationRect.Top);
                        // Apply stroke to the annotation layer.
                        m_myAnnotationsLayer.drawLine(lastDrawPoint, newDrawPoint);
                        lastDrawPoint = newDrawPoint;

                        if (output.IsEnd()) {
                            m_myAnnotationsLayer.commit();
                            canvasIsDirty = true;
                        }
                    }
                    break;
            }
            m_uiForm.Invalidate();
        }

        public void paintCanvas() {
            Rectangle rect = m_myAnnotationsLayer.m_rect;

            using (Graphics graphics = Graphics.FromImage(m_canvas)) {
                //graphics.Clear(Color.Black);
                graphics.Clear(Color.FromArgb(0, 0, 0, 0));
                // Draw the image
                if (m_fullImage != null) {
                    graphics.DrawImage(m_fullImage, rect.Left, rect.Top, rect.Width, rect.Height);
                }
                else {
                    graphics.DrawImage(m_myAnnotationsLayer.m_image.getThumbnailCopy(), rect.Left, rect.Top, rect.Width, rect.Height);
                }
                // Draw all annotation layers that the user wants to see
                foreach (ICWAnnotationLayer layer in m_myAnnotationsLayer.m_image.getAnnotations()) {
                    if (layer.m_visible) {
                        graphics.DrawImage(layer.getCommittedCopy(), rect.Left, rect.Top, rect.Width, rect.Height);
                    }
                }
            }
        }

        public void setCanvasDirty() {
            lock (m_propertyLock) {
                canvasIsDirty = true;
            }
        }

        public void draw(Graphics graphics) {
            Rectangle annotationRect = m_uiForm.m_cwWallPanel.m_annotationRect;
            if (canvasIsDirty) {
                paintCanvas();
                lock (m_propertyLock) {
                    canvasIsDirty = false;
                }
            }
            graphics.DrawImageUnscaled(m_canvas, annotationRect.Location); // draws the image being annotated into the annotation area
            Point layerLoc = new Point(annotationRect.Location.X + m_myAnnotationsLayer.m_rect.X, annotationRect.Location.Y + m_myAnnotationsLayer.m_rect.Y);
            graphics.DrawImageUnscaled(m_myAnnotationsLayer.getCurrentStroke(), layerLoc); // draws the annotation during the annotation action
        }

        public void updateCanvas() {

            m_canvas = new Bitmap(m_uiForm.m_cwWallPanel.m_annotationRect.Width, m_uiForm.m_cwWallPanel.m_annotationRect.Height, PixelFormat.Format32bppArgb);
            setCanvasDirty();
            m_myAnnotationsLayer.m_rect = m_myAnnotationsLayer.computeLayerRect();
            m_myAnnotationsLayer.m_layer = resizeBitmap(m_myAnnotationsLayer.m_layer);
            m_myAnnotationsLayer.m_layer_currentStroke = resizeBitmap(m_myAnnotationsLayer.m_layer_currentStroke);

        }

        private Bitmap resizeBitmap(Bitmap m_bitmapToResize) {

            Bitmap m_bitmap = new Bitmap(m_myAnnotationsLayer.m_rect.Width, m_myAnnotationsLayer.m_rect.Height, PixelFormat.Format32bppArgb);
            using (Graphics graphics = Graphics.FromImage((Image) m_bitmap)) {
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(m_bitmapToResize, 0, 0, m_myAnnotationsLayer.m_rect.Width, m_myAnnotationsLayer.m_rect.Height);
            }
            m_bitmapToResize.Dispose();
            return m_bitmap;

        }

    }
}
