﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ContextuWall {
    public class CWCommon {
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, bool wParam, Int32 lParam);
        [DllImport("user32.dll")]
        public static extern IntPtr PostMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string className, string windowText);
        [DllImport("user32.dll")]
        public static extern IntPtr ShowWindow(int hwnd, int command);

        [StructLayout(LayoutKind.Sequential)]
        public struct FlashWinInfo {
            public uint Size;
            public IntPtr Handle;
            public FlashWinFlags Flags;
            public uint Count;
            public uint Timeout;
        }
        [Flags]
        public enum FlashWinFlags : uint {
            FlashStop = 0x0000,
            FlashCaption = 0x0001,
            FlashTray = 0x0002,
            FlashAll = 0x0003,
            FlashTimer = 0x0004,
            FlashTimerNoForeground = 0x000C
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FlashWindowEx(ref FlashWinInfo flashWinInfo);

        public static void FlashWindow(IntPtr hWnd, FlashWinFlags flags, int count, int timeout) {
            FlashWinInfo flashWinInfo = new FlashWinInfo();
            flashWinInfo.Size = (uint) Marshal.SizeOf(flashWinInfo);

            flashWinInfo.Handle = hWnd;
            flashWinInfo.Flags = flags;
            flashWinInfo.Count = (uint) count;
            flashWinInfo.Count = (uint) timeout;

            FlashWindowEx(ref flashWinInfo);
        }
        public static void FlashWindow(Form f, FlashWinFlags flags = FlashWinFlags.FlashAll, int count = int.MaxValue, int timeout = 2) {
            FlashWindow(f.Handle, flags, count, timeout);
        }

        public class EventArgs<T> : EventArgs {
            private T m_value;
            public EventArgs(T value) {
                m_value = value;
            }
            public T Value {
                get { return m_value; }
            }
        }

        public static float getDistance(float x1, float x2) {
            return (float) Math.Abs(x1 - x2);
        }
        public static float getDistance(float p1x, float p1y, float p2x, float p2y) {
            return (float) Math.Sqrt(Math.Pow(p1x - p2x, 2) + Math.Pow(p1y - p2y, 2));
        }
        public static float getDistance(PointF p1, PointF p2) {
            return (float) Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        public static bool doesPointIntersectWithRectangle(float x, float y, float xRectTopLeft, float yRectTopLeft, float rectWidth, float rectHeight) {
            bool xHit = false;
            bool yHit = false;
            if ((x > xRectTopLeft) && (x < xRectTopLeft + rectWidth)) {
                xHit = true;
            }
            if ((y > yRectTopLeft) && (y < yRectTopLeft + rectHeight)) {
                yHit = true;
            }
            return xHit && yHit;
        }

        public static void showTaskBar() {
            IntPtr handle = FindWindow("Shell_TrayWnd", "");
            ShowWindow((int) handle, 1);
        }
        public static void hideTaskBar() {
            IntPtr handle = FindWindow("Shell_TrayWnd", "");
            ShowWindow((int) handle, 0);
        }

        public static void runOSCommand(string command) {
            using (Process process = new Process()) {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C" + command;
                process.StartInfo = startInfo;
                process.Start();
            }
        }

        public static string getUniqueFilename() {
            //return Program.WORKING_DIR + Guid.NewGuid().ToString() + ".png";
            return Path.Combine(Program.getTmpImagesFolder(), Guid.NewGuid().ToString() + ".png");
        }
    }
}
