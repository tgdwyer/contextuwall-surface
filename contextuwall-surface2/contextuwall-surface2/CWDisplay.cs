﻿using log4net;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ContextuWall {
    //Represents a connected display, storing its name and dimensions and stuff.
    public class CWDisplayList : List<CWDisplay> {
        public ToolStripMenuItem displaysSubmenu = null;
        public ToolStripMenuItem autoSelectItem = null;
        private CWDisplay noDisplay = null;
        public CWDisplay currentDisplay = null;
        public CWUIForm m_uiForm;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CWDisplayList(CWUIForm uiForm) {
            m_uiForm = uiForm;
        }

        public void init() {
            //Init the "no" display.
            noDisplay = new CWDisplay("No Display", -1, -1);
            noDisplay.toolStripItem = (ToolStripMenuItem) displaysSubmenu.DropDownItems.Add("No Display", null, (s, e) => selectPrimaryDisplay(noDisplay, false));
            noDisplay.toolStripItem.Checked = true;
            currentDisplay = noDisplay;
            //Init the auto-select option.
            displaysSubmenu.DropDownItems.Add(new ToolStripSeparator());
            autoSelectItem = (ToolStripMenuItem) displaysSubmenu.DropDownItems.Add("Auto Select Display", null, (s, e) => toggleAutoSelect());
            autoSelectItem.Checked = true;
        }

        public void selectPrimaryDisplay(CWDisplay display, bool auto) {
            autoSelectItem.Checked = auto;
            if (display == null) {
                display = noDisplay;
            }
            if (display == currentDisplay) {
                return;
            }
            currentDisplay.toolStripItem.Checked = false;
            display.toolStripItem.Checked = true;
            currentDisplay = display;
            m_uiForm.invokeInitUI();
        }

        public void toggleAutoSelect() {
            if (!autoSelectItem.Checked) {
                selectPrimaryDisplay(Find(item => m_uiForm.m_cwNetwork.m_hostIP.StartsWith(item.name)), true);
            }
            else {
                autoSelectItem.Checked = false;
            }
        }

        public void registerDisplay(CWDisplay newDisplay) {
            CWDisplay display;
            string toolStripText = newDisplay.name + "(" + newDisplay.width + "x" + newDisplay.height + ")";
            if (Exists(item => item.name.Equals(newDisplay.name))) {
                if (logger.IsInfoEnabled)
                    logger.Info("Display '" + newDisplay.name + "' already registered! Registration ignored (specs updated though).");
                display = Find(item => item.name.Equals(newDisplay.name));
                display.update(newDisplay);
                display.toolStripItem.Text = toolStripText;
            }
            else {
                display = newDisplay;
                Add(display);
                // Add to the notify menu.
                display.toolStripItem = new ToolStripMenuItem(toolStripText, null, (s, e) => selectPrimaryDisplay(display, false));
                displaysSubmenu.DropDownItems.Insert(displaysSubmenu.DropDownItems.Count - 2, display.toolStripItem);
            }

            if (autoSelectItem.Checked && m_uiForm.m_cwNetwork.m_hostIP.StartsWith(display.name)) {
                selectPrimaryDisplay(display, true);
            }
        }
        public delegate void registerDisplayCallback(CWDisplay newDisplay);
        public void invokeRegisterDisplay(CWDisplay newDisplay) {
            m_uiForm.Invoke(new registerDisplayCallback(this.registerDisplay), new object[] { newDisplay });
        }

        public void deregisterDisplay(string displayName) {
            if (!Exists(item => item.name.Equals(displayName))) {
                if (logger.IsErrorEnabled)
                    logger.Error("Display not registered! Deregistration ignored.");
                return;
            }
            CWDisplay display = Find(item => item.name.Equals(displayName));
            Remove(display);
            // Remove from notify menu.
            if (display.toolStripItem.Checked) {
                selectPrimaryDisplay(noDisplay, autoSelectItem.Checked);
            }
            displaysSubmenu.DropDownItems.Remove(display.toolStripItem);
        }
        public delegate void deregisterDisplayCallback(string displayName);
        public void invokeDeregisterDisplay(string displayName) {
            m_uiForm.Invoke(new deregisterDisplayCallback(this.deregisterDisplay), displayName);
        }
    }
    public class CWDisplay {
        public string name;
        public int width;
        public int height;
        public ToolStripMenuItem toolStripItem = null;


        public CWDisplay(string name, int width, int height) {
            this.name = name;
            this.width = width;
            this.height = height;
        }

        // Updates the specs of a display with new ones.
        public void update(CWDisplay newDisplay) {
            this.name = newDisplay.name;
            this.width = newDisplay.width;
            this.height = newDisplay.height;
        }
        //public void update(int width, int height) {
        //    this.width = width;
        //    this.height = height;
        //}
    }
}
