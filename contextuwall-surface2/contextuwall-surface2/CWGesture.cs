﻿using System;
using System.Threading;

namespace ContextuWall {
    public interface IGesture {
        bool isGestureRecognized();
        void resetGesture();

        // GESTURE EVENTS
        void FingerDown(InteractionOutput io);
        void FingerUp(InteractionOutput io);
        void FingerMove(InteractionOutput io);
    }

    public class CWGesture {
        public int m_numberOfFingersRequired;

        public interface IGestureData {
        }

        public enum GestureTypes {
            QUERY_GESTURE,
            DOUBLE_TAP,
            TRIPLE_TAP,
            DOUBLE_FINGER_DOUBLE_TAP,
            DOUBLE_FINGER_TRIPLE_TAP,
            ZOOM_IN,
            ZOOM_OUT,
            PAN,
            SWIPE
        }
        public enum GestureStates {
            POSSIBLE,
            RECOGNIZED,
            CANCELLED
        }

        public class GestureData : IGestureData {
            public GestureTypes m_gestureType;
            public string m_elementId;

            public GestureData(GestureTypes t, string elementId) {
                m_gestureType = t;
                m_elementId = elementId;
            }
        }

        public delegate void GestureHandler(object source, IGestureData data);

        private AutoResetEvent m_go;
        private Timer m_timer;
        private int m_timeout;
        private Thread m_thread;
        private GestureHandler m_action;
        public IGestureData m_gestureData;
        public Func<bool> m_isGestureRecognized;
        public Action m_resetGesture;
        public GestureStates m_state;

        public CWGesture(GestureHandler action) {
            //Console.WriteLine("CWGesture");
            m_action = action;

            m_go = new AutoResetEvent(false);
            m_thread = new Thread(activateGesture);
            m_thread.Start();

            m_timer = new Timer(new TimerCallback(waitTimer), m_go, Timeout.Infinite, Timeout.Infinite);
            m_timeout = 10;

            m_state = GestureStates.POSSIBLE;
        }

        public void resetWaitTimerToCheckGesture(bool pause) {
            //Console.WriteLine("resetTimer");
            if (pause) {
                m_timer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            else {
                m_timer.Change(m_timeout, Timeout.Infinite);
            }
        }

        private void waitTimer(Object stateInfo) {
            //Console.WriteLine("waitTimer");
            AutoResetEvent autoEvent = (AutoResetEvent) stateInfo;
            autoEvent.Set();
        }

        private void activateGesture() {
            //Console.WriteLine("activateGesture");
            while (true) {
                m_go.WaitOne();
                m_timer.Change(Timeout.Infinite, Timeout.Infinite);
                if (m_isGestureRecognized()) {
                    //Console.WriteLine("m_action");
                    m_state = GestureStates.RECOGNIZED;
                    m_action(this, m_gestureData);
                }
                m_resetGesture();
            }
        }

        public void invalidateGesture() {
            m_thread.Abort();
            //Console.WriteLine("invalidateGesture");
        }
    }
}
