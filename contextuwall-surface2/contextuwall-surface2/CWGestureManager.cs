﻿using log4net;
using System;
using System.Collections.Generic;

namespace ContextuWall {
    public class CWGestureManager : BaseHandler {
        public CWUIForm m_uiForm;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CWGestureManager(CWUIForm uiForm) : base() {
            m_uiForm = uiForm;
            enableInteraction();
        }
        private List<IGesture> m_manipulationGestures = new List<IGesture>();

        public void addManipulationGesture(CWGesture g) {
            m_manipulationGestures.Add((IGesture) g);
        }

        public void invalidateGestures() {
            foreach (CWGesture g in m_manipulationGestures) {
                try {
                    g.invalidateGesture();
                } catch (Exception ex) {
                    if (logger.IsErrorEnabled)
                        logger.Error(ex.StackTrace);
                }
            }
        }

        private void enableInteraction() {
            //http://www.codeproject.com/Articles/607234/Using-Windows-Interaction-Con
            Win32.INTERACTION_CONTEXT_CONFIGURATION[] cfg = new Win32.INTERACTION_CONTEXT_CONFIGURATION[] {
                // TAPPING (Left click)
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.TAP,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.TAP |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.TAP_DOUBLE
                    ),
                // SECONDARY TAPPING (Right click)
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.SECONDARY_TAP,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.SECONDARY_TAP),
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.HOLD,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.HOLD),
                // MOVING & SCALING
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.MANIPULATION,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_X |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_Y |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_SCALING
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_ROTATION |
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_INERTIA |
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_ROTATION_INERTIA |
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_SCALING_INERTIA
                    ),
            };
            Win32.SetInteractionConfigurationInteractionContext(Context, cfg.Length, cfg);
        }
        internal override void ProcessEvent(InteractionOutput output) {
            switch (output.Data.Interaction) {
                case Win32.INTERACTION.MANIPULATION:
                    if (output.IsBegin()) {
                        foreach (IGesture g in m_manipulationGestures) {
                            g.FingerDown(output);
                        }
                    }
                    else {
                        if (output.IsEnd()) {
                            foreach (IGesture g in m_manipulationGestures) {
                                g.FingerUp(output);
                            }
                        }
                        else {
                            foreach (IGesture g in m_manipulationGestures) {
                                g.FingerMove(output);
                            }
                        }
                    }
                    break;
            }
            m_uiForm.Invalidate();
        }
    }
}
