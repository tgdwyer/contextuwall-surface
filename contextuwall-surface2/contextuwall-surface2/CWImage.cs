﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ContextuWall {
    public interface ICWImage {
        string fullFilepath { get; set; }
        string filename { get; }
        bool m_saveRequested { get; set; }
        bool m_saveWithAnnotations { get; set; }
        string m_desiredSaveFilePath { get; }
        ImageFormat m_desiredSaveFileFormat { get; }
        void setLocation(PointF location, bool sendCommandToServer);
        void setScale(float newScale, bool sendCommandToServer);
        void reposition();
        void bringToFront(bool sendCommandToServer);
        void sendToBack(bool sendCommandToServer);
        void remove(bool sendCommandToServer);
        bool locked { get; set; }
        ICWAnnotationLayer getAnnotationWithOwnerID(string ownerID);
        void addAnnotation(ICWAnnotationLayer annotation);
        IEnumerable<ICWAnnotationLayer> getAnnotations();
        void flattenAnnotations();
        void draw(Graphics graphics);
        void drawOutline(Graphics graphics);
        bool containsPoint(Point p);
        void Dispose();
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class CWImage : BaseHandler, ICWImage {
        public string fullFilepath { get; set; }
        [JsonProperty]
        public string filename { get; set; }
        [JsonProperty]
        public string m_image64 { get; set; }
        [JsonProperty]
        public int m_width { get; set; }
        [JsonProperty]
        public int m_height { get; set; }

        [JsonProperty]
        public PointF m_normalisedLocation;
        public PointF m_lastNormalisedLocationSent;
        public float m_normalisedWidth { get; set; }
        public float m_normalisedHeight { get; set; }
        public float m_localScale { get; set; }
        [JsonProperty]
        public float m_scale { get; set; }
        public float m_lastScaleSent;

        public bool isSelected = false;
        [JsonProperty]
        private bool isLocked = false;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool locked {
            get {
                return isLocked;
            }
            set {
                if (value != isLocked) {
                    isOldDistortion = true;
                    isLocked = value;
                }
            }
        }
        public bool isShadow;

        private Bitmap m_thumbnail;
        private List<ICWAnnotationLayer> m_annotations;

        private Bitmap m_thumbnailWithAnnotations = null;
        private List<FastBitmap> m_toDraw;
        private bool isOldDistortion = true;
        public List<Tuple<float, Quadrilateral>> m_thumbnailQuads { get; set; }

        [JsonProperty]
        public string m_thumbnail64 { get; set; }

        public CWUIForm m_uiForm;

        public bool m_saveRequested { get; set; }
        public bool m_saveWithAnnotations { get; set; }
        public string m_desiredSaveFilePath { get; set; }
        public ImageFormat m_desiredSaveFileFormat { get; set; }

        public ContextMenuStrip m_contextMenuStrip;
        public static ContextMenuStrip m_curentOpenContextMenu = null;

        private readonly Object m_graphicsLock = new Object();
        private readonly Object m_propertyLock = new Object();
        private bool disposed = false;

        // Default constructor
        public CWImage(CWUIForm uiForm, string fullFilepath) {
            this.m_uiForm = uiForm;
            this.fullFilepath = fullFilepath;
            this.filename = Guid.NewGuid().ToString() + ".png";

            using (Image image = new Bitmap(fullFilepath)) {
                m_uiForm.m_cwWidget.updateElementsProgress(33);
                this.m_width = image.Size.Width;
                this.m_height = image.Size.Height;
                this.setScale(-1.0f, false); // native dimensions.
                if (m_localScale < m_uiForm.m_cwWallPanel.m_minImageHeightScaling)
                    m_localScale = m_uiForm.m_cwWallPanel.m_minImageHeightScaling;
                this.m_image64 = CWNetwork.ImageToBase64(image);
                m_uiForm.m_cwWidget.updateElementsProgress(66);

                // Generate a thumbnail for export (static max size).
                float thumbScale = (float) ((m_width > m_height) ? (256.0 / m_width) : (256.0 / m_height));
                this.m_thumbnail = new FastBitmap(image.GetThumbnailImage((int) (m_width * thumbScale), (int) (m_height * thumbScale), null, IntPtr.Zero));
                this.m_thumbnail64 = CWNetwork.ImageToBase64(m_thumbnail);
                m_uiForm.m_cwWidget.updateElementsProgress(100);
            }
            this.m_annotations = new List<ICWAnnotationLayer>();
            flattenAnnotations();

            // Compute size for local thumbnail.
            computeThumbnailDimensions();
            computeThumbnailQuad();

            initContextMenu();

            enableInteraction();
        }

        // For creating 'shadow' images (ie. uploads by other clients - this client only needs to keep track of the thumbnail).
        public CWImage(CWUIForm uiForm, string filename, int width, int height, float scale, string thumbnail64) {
            this.m_uiForm = uiForm;
            this.fullFilepath = null;
            this.filename = filename;

            this.m_width = width;
            this.m_height = height;
            this.m_scale = scale;
            this.m_localScale = scale;
            if (this.m_localScale > 1.0f)
                this.m_localScale = 1.0f;
            this.m_image64 = "";

            this.m_thumbnail64 = thumbnail64;
            this.m_thumbnail = new FastBitmap(CWNetwork.Base64ToImage(thumbnail64));

            this.m_annotations = new List<ICWAnnotationLayer>();
            flattenAnnotations();

            // Compute size and orientation for local thumbnail.
            computeThumbnailDimensions();
            computeThumbnailQuad();

            initContextMenu();

            enableInteraction();
        }

        // Returns the full resolution image. If not available locally, method will return null, and send off a request to the server
        //  so that the image will be available when this method is called again later.
        public Bitmap getFullResolution() {
            try {
                return new Bitmap(fullFilepath);
            } catch (ArgumentException) {
                // Filepath points to bogus location. Need to redownload the image after all.
                m_uiForm.m_cwNetwork.sendRequestFullCommand(this);
                return null;
            }
        }

        // Returns the image thumbnail.
        public Bitmap getThumbnailCopy() {
            lock (m_propertyLock) {
                return (Bitmap) m_thumbnail.Clone();
            }
        }

        // Thumbnail dimensions range from 0.0-1.0, with 1.0 being wall height.
        private void computeThumbnailDimensions() {
            lock (m_propertyLock) {
                m_normalisedWidth = m_width * m_uiForm.m_cwWallPanel.m_height / (float) (m_uiForm.m_cwWallPanel.m_width * m_height) * m_localScale;
                m_normalisedHeight = m_localScale;
            }
        }

        // Recompute the bounds, so that the image is ready for the drawing phase.
        public void computeThumbnailQuad() {
            List<Tuple<float, Quadrilateral>> thumbQuads;
            thumbQuads = m_uiForm.m_cwWallPanel.denormaliseRect(
                new RectangleF(m_normalisedLocation.X - m_normalisedWidth / 2,
                                m_normalisedLocation.Y - m_normalisedHeight / 2,
                                m_normalisedWidth,
                                m_normalisedHeight)
            );

            lock (m_propertyLock) {
                m_thumbnailQuads = thumbQuads;
                isOldDistortion = true;
            }
        }

        // Repositions (and rescales) the thumbnail based on its current normalised location,
        //  and current screen settings. (ie. should be called when client display settings change).
        // Depending on the rescaling, this may also cause a need to update the image's normalised location.
        public void reposition() {
            computeThumbnailDimensions();
            setLocation(m_uiForm.m_cwWallPanel.denormaliseLocation(m_normalisedLocation), false);
        }

        // Sets image's location, taking its dimensions into account.
        // NOTE: <location> is taken to be the centre of the image, not the top left corner.
        public void setLocation(PointF newLocation, bool sendCommandToServer, PointF normalisedLocationOffset) {
            // Check if user isn't perhaps trying to drag the image down into annotation mode.
            //Rectangle annotationRect = m_uiForm.m_cwWallPanel.m_annotationRect;
            //PointF midPt = new PointF(annotationRect.Left + annotationRect.Width / 2, annotationRect.Top + annotationRect.Height / 2);
            //float distMidPt = (float) (Math.Pow((newLocation.X - midPt.X) / annotationRect.Width, 2) + Math.Pow((newLocation.Y - midPt.Y) / annotationRect.Height, 2));
            //if (distMidPt <= 0.25) {
            //    if (distMidPt <= 0.15 && !m_uiForm.m_cwAnnotationMode.active) {
            //        // TODO: Briefly pause control and animate thumbnail growing to fill annotation area, OR gradually grow thumbnail size
            //        //  as the user's finger gets closer to the centre of the ellipse.
            //        m_uiForm.m_cwAnnotationMode.begin(this);
            //    }
            //    return;
            //}
            // Normalise the values.
            PointF normLoc = m_uiForm.m_cwWallPanel.normaliseLocation(newLocation);
            // Snap the image back to within bounds, accounting for finger offset from image centre.
            normLoc.X = Math.Max(m_normalisedWidth / 2, Math.Min(1 - m_normalisedWidth / 2, normLoc.X - normalisedLocationOffset.X));
            normLoc.Y = Math.Max(m_normalisedHeight / 2, Math.Min(1 - m_normalisedHeight / 2, normLoc.Y - normalisedLocationOffset.Y));

            lock (m_propertyLock) {
                m_normalisedLocation = normLoc;

                if (sendCommandToServer && CWCommon.getDistance(m_lastNormalisedLocationSent, m_normalisedLocation) > 2e-3) {
                    m_lastNormalisedLocationSent = m_normalisedLocation;
                    m_uiForm.m_cwNetwork.sendLocationUpdateCommand(this);
                }
            }
            computeThumbnailQuad();
        }
        public void setLocation(PointF newLocation, bool sendCommandToServer) {

            setLocation(newLocation, sendCommandToServer, Point.Empty);

        }

        public void setScale(float newScale, bool sendCommandToServer) {
            lock (m_propertyLock) {
                if (newScale == -1) {
                    if (m_uiForm.m_cwDisplayList.currentDisplay.height > 0) {
                        newScale = m_height / (float) m_uiForm.m_cwDisplayList.currentDisplay.height;
                    }
                    else {
                        newScale = m_uiForm.m_cwWallPanel.m_minImageHeightScaling;
                    }
                }
                m_scale = newScale;
                m_localScale = newScale;
                if (m_localScale > 1.0f)
                    m_localScale = 1.0f;
                if (sendCommandToServer && CWCommon.getDistance(m_lastScaleSent, newScale) > 1e-2) {
                    m_lastScaleSent = newScale;
                    m_uiForm.m_cwNetwork.sendResizeCommand(this);
                }
            }
            reposition();
        }

        public void bringToFront(bool sendCommandToServer) {
            m_uiForm.m_cwWall.bringImageToFront(this);
            if (sendCommandToServer) {
                m_uiForm.m_cwNetwork.sendBringToFrontCommand(this);
            }
        }

        public void sendToBack(bool sendCommandToServer) {
            m_uiForm.m_cwWall.sendImageToBack(this);
            if (sendCommandToServer) {
                m_uiForm.m_cwNetwork.sendSendToBackCommand(this);
            }
        }

        public void save(bool saveAnnotations) {
            m_saveWithAnnotations = saveAnnotations;
            using (SaveFileDialog dialog = new SaveFileDialog()) {
                dialog.Filter = "Bitmap|*.bmp|JPEG Image|*.jpg|PNG Image|*.png|GIF Image|*.gif|TIFF Image|*.tif";
                dialog.FilterIndex = 3;
                if (dialog.ShowDialog() == DialogResult.OK && dialog.FileName != "") {
                    switch (dialog.FilterIndex) {
                        case 1:
                            m_desiredSaveFileFormat = ImageFormat.Bmp;
                            break;
                        case 2:
                            m_desiredSaveFileFormat = ImageFormat.Jpeg;
                            break;
                        case 3:
                            m_desiredSaveFileFormat = ImageFormat.Png;
                            break;
                        case 4:
                            m_desiredSaveFileFormat = ImageFormat.Gif;
                            break;
                        case 5:
                            m_desiredSaveFileFormat = ImageFormat.Tiff;
                            break;
                    }
                    using (FileStream fs = (FileStream) dialog.OpenFile()) {
                        //DialogResult dialogResult = MessageBox.Show("Flatten annotations onto saved image?", "Keep Annotations", MessageBoxButtons.YesNoCancel);
                        //switch(dialogResult){
                        //    case DialogResult.Yes:
                        //        m_saveWithAnnotations = true;
                        //        break;
                        //    case DialogResult.No:
                        //        m_saveWithAnnotations = false;
                        //        break;
                        //    case DialogResult.Cancel:
                        //        return;
                        //}
                        try {
                            using (Image img = getFullResolution()) {
                                if (img != null) {
                                    if (m_saveWithAnnotations) {
                                        using (Graphics g = Graphics.FromImage(img)) {
                                            foreach (ICWAnnotationLayer al in getAnnotations()) {
                                                g.DrawImage(al.getCommittedCopy(), 0, 0, img.Width, img.Height);
                                            }
                                        }
                                    }
                                    img.Save(fs, m_desiredSaveFileFormat);
                                }
                                else {
                                    // Set flags so that image is saved properly once it arrives from the server.
                                    m_desiredSaveFilePath = dialog.FileName;
                                    m_saveRequested = true;
                                }
                            }
                        } catch (Exception exception) {
                            if (logger.IsErrorEnabled)
                                logger.Error("Could not save image: " + exception.Message);
                        }
                    }
                }
            }
        }

        public void remove(bool sendCommandToServer) {
            if (sendCommandToServer) {
                m_uiForm.m_cwNetwork.sendRemoveCommand(this);
            }
            m_uiForm.m_cwWall.removeImage(this);
        }

        public bool containsPoint(Point p) {
            foreach (Tuple<float, Quadrilateral> tup in m_thumbnailQuads) {
                if (tup.Item2.containsPoint(p))
                    return true;
            }
            return false;
        }

        public ICWAnnotationLayer getAnnotationWithOwnerID(string ownerID) {
            lock (m_propertyLock) {
                return m_annotations.FirstOrDefault(item => item.m_ownerID.Equals(ownerID));
            }
        }

        public void addAnnotation(ICWAnnotationLayer annotation) {
            lock (m_propertyLock) {
                m_annotations.Add(annotation);
            }
        }

        public IEnumerable<ICWAnnotationLayer> getAnnotations() {
            lock (m_propertyLock) {
                for (int i = 0; i < m_annotations.Count; i++) {
                    yield return m_annotations[i];
                }
            }
        }

        public void flattenAnnotations() {
            Bitmap flattened;
            lock (m_propertyLock) {
                flattened = (Bitmap) m_thumbnail.Clone();

                using (Graphics g = Graphics.FromImage(flattened)) {
                    foreach (ICWAnnotationLayer al in m_annotations) {
                        g.DrawImage(al.getCommittedCopy(), 0, 0, flattened.Width, flattened.Height);
                    }
                }

                m_thumbnailWithAnnotations = flattened;
                isOldDistortion = true;
            }
        }

        // Sets up interaction with thumbnail.
        private void enableInteraction() {
            //http://www.codeproject.com/Articles/607234/Using-Windows-Interaction-Con
            Win32.INTERACTION_CONTEXT_CONFIGURATION[] cfg = new Win32.INTERACTION_CONTEXT_CONFIGURATION[] {
                // TAPPING (Left click)
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.TAP,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.TAP |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.TAP_DOUBLE
                    ),
                // SECONDARY TAPPING (Right click)
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.SECONDARY_TAP,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.SECONDARY_TAP),
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.HOLD,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.HOLD),
                // MOVING & SCALING
                new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.MANIPULATION,
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_X |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_Y |
                    Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_SCALING
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_ROTATION |
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_INERTIA |
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_ROTATION_INERTIA |
                    //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_SCALING_INERTIA
                    ),
            };
            Win32.SetInteractionConfigurationInteractionContext(Context, cfg.Length, cfg);
        }

        private PointF normalisedLocationOffset = Point.Empty;
        private PointF normalisedLocationBeforeMaximize = Point.Empty;
        internal override void ProcessEvent(InteractionOutput output) {
            if (!isLocked) {
                switch (output.Data.Interaction) {
                    case Win32.INTERACTION.TAP:
                        switch (output.Data.Tap.Count) {
                            case 1:
                                //Console.WriteLine("SingleTap!");
                                break;
                            case 2:
                                //Console.WriteLine("DoubleTap!");
                                if (CWCommon.getDistance(m_localScale, 1.0f) > 1e-2) {
                                    normalisedLocationBeforeMaximize = m_normalisedLocation;
                                    setScale(1.0f, true);
                                    // Expanded image will be snapping to new bounds, so need to resend location.
                                    setLocation(m_uiForm.m_cwWallPanel.denormaliseLocation(m_normalisedLocation), true);
                                }
                                else {
                                    setScale(-1.0f, true);
                                    if (normalisedLocationBeforeMaximize != PointF.Empty) {
                                        setLocation(m_uiForm.m_cwWallPanel.denormaliseLocation(normalisedLocationBeforeMaximize), true);
                                    }
                                }
                                break;
                                //Triple+ tap not supported.
                        }
                        break;
                    case Win32.INTERACTION.SECONDARY_TAP:
                        //Console.WriteLine("SecondaryTap!");
                        invokeShowContextMenuStrip(new Point((int) output.Data.X, (int) output.Data.Y));
                        break;
                    case Win32.INTERACTION.MANIPULATION:
                        if (output.IsBegin()) {
                            normalisedLocationBeforeMaximize = PointF.Empty;
                            PointF np = m_uiForm.m_cwWallPanel.normaliseLocation(new PointF(output.Data.X, output.Data.Y));
                            normalisedLocationOffset = new PointF(np.X - m_normalisedLocation.X, np.Y - m_normalisedLocation.Y);
                        }
                        else {
                            Win32.MANIPULATION_TRANSFORM mt = output.Data.Manipulation.Delta;
                            // Scaling
                            // scaling the image larger than wall height is possible but is not shown on the client yet
                            // TODO: show scaling larger than wall height on client
                            if (mt.Scale != 1.0) { // Scale == 1.0 no zoom (100%), Scale < 1.0 zoom out (< 100%), Scale > 1.0 zoom in (> 100%)
                                //setScale(Math.Min(m_localScale * mt.Scale, 1.0f), true);
                                setScale(m_scale * mt.Scale, true);
                            }

                            // Check if user isn't perhaps trying to drag the image down into annotation mode.
                            Rectangle annotationRect = m_uiForm.m_cwWallPanel.m_annotationRect;
                            PointF midPt = new PointF(annotationRect.Left + annotationRect.Width / 2, annotationRect.Top + annotationRect.Height / 2);
                            float distMidPt = (float) (Math.Pow((output.Data.X - midPt.X) / annotationRect.Width, 2) + Math.Pow((output.Data.Y - midPt.Y) / annotationRect.Height, 2));
                            if (distMidPt <= 0.25) {
                                if (output.IsEnd() && distMidPt <= 0.15) {
                                    // TODO: Briefly pause control and animate thumbnail growing to fill annotation area, OR gradually grow thumbnail size
                                    //  as the user's finger gets closer to the centre of the ellipse.
                                    if (m_uiForm.m_cwAnnotationMode.active) {
                                        m_uiForm.m_cwAnnotationMode.end();
                                    }
                                    m_uiForm.m_cwAnnotationMode.begin(this);
                                }
                                break;
                            }

                            // Movement
                            setLocation(new PointF(output.Data.X, output.Data.Y), true, normalisedLocationOffset);
                        }
                        break;
                }
                m_uiForm.Invalidate();
            }
        }

        private void initContextMenu() {
            this.m_contextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            // set font size of the context menu
            int m_fontSize = (int) Math.Max(m_contextMenuStrip.Font.Size, 16);
            m_contextMenuStrip.Font = new Font(m_contextMenuStrip.Font.Name, m_fontSize, FontStyle.Regular);
            m_contextMenuStrip.Items.Add("Send Image to Back", null, (s, e) => { this.sendToBack(true); m_uiForm.Invalidate(); });
            m_contextMenuStrip.Items.Add("Save Image", null, (s, e) => this.save(false));
            m_contextMenuStrip.Items.Add("Save Image with Annotations", null, (s, e) => this.save(true));
            m_contextMenuStrip.Items.Add("Remove Image", null, (s, e) => { this.remove(true); this.m_uiForm.Invalidate(); });
        }

        public delegate void showContextMenuStripCallback(Point where);
        private void showContextMenuStrip(Point where) {
            m_contextMenuStrip.Show(m_uiForm, where);
            m_curentOpenContextMenu = m_contextMenuStrip;
        }
        public void invokeShowContextMenuStrip(Point where) {
            m_uiForm.BeginInvoke(new showContextMenuStripCallback(this.showContextMenuStrip), new object[] { where });
        }

        public void draw(Graphics graphics) {
            // Map thumbnail to quad.
            if (isOldDistortion) {
                List<FastBitmap> toDraw = new List<FastBitmap>();
                float left = 0f;
                foreach (Tuple<float, Quadrilateral> tup in m_thumbnailQuads) {
                    // Ensure this quadrilateral has enough width to be drawn (else we get graphics overflows)
                    if (tup.Item1 > 5e-3) {
                        FastBitmap f = new FastBitmap(tup.Item2.bounds);
                        Point[] p = tup.Item2.relativePoints;
                        Rectangle cropArea = new Rectangle((int) left, 0, (int) (m_thumbnailWithAnnotations.Width * tup.Item1), m_thumbnailWithAnnotations.Height);
                        Bitmap croppedThumbnail = m_thumbnailWithAnnotations.Clone(cropArea, m_thumbnailWithAnnotations.PixelFormat);
                        if (isLocked)
                            croppedThumbnail = FastBitmap.MakeGrayscale(croppedThumbnail);
                        QuadDistort.DrawBitmap(croppedThumbnail, p[0], p[1], p[2], p[3], f);
                        toDraw.Add(f);
                    }
                    else {
                        toDraw.Add(null); //s.t the indexing in toDraw matches the one in thumbnailQuads.
                    }
                    left += m_thumbnail.Width * tup.Item1;
                }
                lock (m_propertyLock) {
                    m_toDraw = toDraw;
                    isOldDistortion = false;
                }
            }
            lock (m_graphicsLock) {
                for (int i = 0; i < m_toDraw.Count; i++) {
                    if (m_toDraw[i] != null) {
                        graphics.DrawImageUnscaled(m_toDraw[i], m_thumbnailQuads[i].Item2.location);
                    }
                    if (!isSelected) {
                        using (Pen pen = new Pen(Brushes.Black, 2)) {
                            graphics.DrawLines(pen, m_thumbnailQuads[i].Item2.points);
                        }
                    }
                }
            }
        }
        // This function should be called for each image after all the images have been drawn;
        //  it draws a nice dashed outline, so that the image can be seen when it's behind another.
        // It also handles drawing of the highlighted border when an image is being manipulated,
        //  or a greyed out border when an image is locked (being manipulated by another client).
        public void drawOutline(Graphics graphics) {
            lock (m_graphicsLock) {
                Pen pen = null;
                try {
                    if (isLocked) {
                        pen = new Pen(Brushes.Gray, 4);
                    }
                    else {
                        if (isSelected) {
                            pen = new Pen(Brushes.Yellow, 4);
                        }
                        else {
                            pen = new Pen(Brushes.Black, 1);
                            pen.DashStyle = DashStyle.Dash;
                        }
                    }
                    foreach (Tuple<float, Quadrilateral> tup in m_thumbnailQuads) {
                        graphics.DrawLines(pen, tup.Item2.points);
                    }
                } finally {
                    if (pen != null) {
                        pen.Dispose();
                    }
                }
            }
        }

        protected override void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    // Dispose of managed resources here. (ie. Other 'Disposable' objects referenced by this object.)
                    lock (m_propertyLock) {
                        m_thumbnail.Dispose();
                        m_contextMenuStrip.Dispose();
                    }
                }
                // Dispose of unmanaged resources here. (ie. any additional fancy cleanups, or dereferencing)

                disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}