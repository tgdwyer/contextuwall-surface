﻿using log4net;
using Newtonsoft.Json.Linq;
using Quobject.EngineIoClientDotNet.Client;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Timers;
using System.Windows.Forms;
using Quobject.Collections.Immutable;
using Quobject.EngineIoClientDotNet.Client.Transports;
using Options = Quobject.SocketIoClientDotNet.Client.IO.Options;
using Socket = Quobject.SocketIoClientDotNet.Client.Socket;

namespace ContextuWall {
    public class CWNetwork {
        public CWUIForm m_uiForm;

        public string m_hostIP { get; set; }
        public ushort m_hostPort { get; set; }
        public string m_socketID { get; set; }
        private bool connected = false; //There should exist a variable in the API that already stores this..
        public string m_lastConnectedHost { get; set; } // last connected host
        private bool m_connectionError = false;
        private bool m_intendedConnectionClose = false;

        public bool m_modelDownloadInProgress = false;
        public bool m_modelDownloadProgressed = false;
        public bool m_ignoreIncomingDownloads = false;
        //private System.Timers.Timer m_modelRequestTimeout = new System.Timers.Timer(500);
        private System.Timers.Timer m_modelRequestTimeout = new System.Timers.Timer(5000);
        private const int MAX_NUM_NUDGES = 5;
        private int m_numNudges;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CWNetwork(CWUIForm uiForm) {
            m_uiForm = uiForm;
            if (Properties.Settings.Default.CommandServerIP.Length > 0) {
                m_hostIP = Properties.Settings.Default.CommandServerIP[0];
            }
            else {
                m_hostIP = "";
            }
            if (Properties.Settings.Default.CommandServerPort.Length > 0) {
                m_hostPort = ushort.Parse(Properties.Settings.Default.CommandServerPort[0]);
            }
            else {
                m_hostPort = 3000;
            }
            m_lastConnectedHost = "";
            m_modelRequestTimeout.AutoReset = false;
            m_modelRequestTimeout.Elapsed += new ElapsedEventHandler(modelRequestTimedOut);
        }

        private void sendPassword(string hash) {

            if (logger.IsDebugEnabled)
                logger.Debug("Password will be sent ...");
            var packet = new {
                password = hash
            };
            emit("authenticate", packet);
            if (logger.IsDebugEnabled)
                logger.Debug("Password sent!");

        }

        //http://www.dailycoding.com/Posts/convert_image_to_base64_string_and_base64_string_to_image.aspx
        public static string ImageToBase64(Image image) {
            using (MemoryStream ms = new MemoryStream()) {
                // Convert Image to byte[]
                try {
                    //image.Save(ms, image.RawFormat);
                    image.Save(ms, ImageFormat.Png);
                } catch (Exception exception) {
                    if (logger.IsErrorEnabled)
                        logger.Error("Could not save png image: " + exception.Message);
                }
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                return Convert.ToBase64String(imageBytes);
            }
        }

        public static Image Base64ToImage(string base64String) {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length)) {
                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                return Image.FromStream(ms, true);
            }
        }

        public void sendImage(CWImage image) {
            m_uiForm.m_cwWidget.showElementsProgress(true, "Uploading image...");
            // Send a short message first, to have some 'instant' feedback on display(s) while actual image is being sent through. 
            var packet = new {
                filename = image.filename,
                m_thumbnail64 = image.m_thumbnail64, // display(s) can render this while waiting on full resolution image.
                m_normalisedLocation = image.m_normalisedLocation,
                m_width = image.m_width,
                m_height = image.m_height,
                m_scale = image.m_scale
            };
            emit("incomingImageNotify", packet);
            // Now send the whole thing.
            emit("incomingImage", image);
            // Free up some RAM.
            image.m_image64 = "";

            if (logger.IsInfoEnabled)
                logger.Info("Sent: " + image.filename);
        }

        public void sendAnnotation(CWAnnotationLayer annotation) {
            var packet = new { imageFilename = annotation.m_image.filename, m_ownerID = annotation.m_ownerID, m_layer64 = ImageToBase64(annotation.getCommittedCopy()) };
            emit("incomingAnnotation", packet);
        }

        public void sendLocationUpdateCommand(CWImage image) {
            var packet = new { filename = image.filename, m_normalisedLocation = image.m_normalisedLocation };
            emit("updateImageLocation", packet);
        }

        public void sendResizeCommand(CWImage image) {
            var packet = new { filename = image.filename, m_scale = image.m_scale };
            emit("resizeImage", packet);
        }


        public void sendBringToFrontCommand(CWImage image) {
            var packet = new { filename = image.filename };
            emit("bringImageToFront", packet);
        }

        public void sendSendToBackCommand(CWImage image) {
            var packet = new { filename = image.filename };
            emit("sendImageToBack", packet);
        }

        public void sendRequestFullCommand(CWImage image) {
            var packet = new { filename = image.filename };
            emit("requestFullImage", packet);
        }

        public void sendRemoveCommand(CWImage image) {
            var packet = new { filename = image.filename };
            emit("removeImage", packet);
        }
        public void sendUnlockCommand(CWImage image) {
            var packet = new { filename = image.filename };
            emit("unlockImage", packet);
        }

        public void sendClearWall() {
            emit("clearWall");
        }

        public void requestModel() {
            emit("requestDisplaySpecs");
            emit("requestModel");
            m_modelDownloadInProgress = true;
            m_uiForm.m_cwWidget.showElementsProgress(true, "Downloading images...");
            if (logger.IsInfoEnabled)
                logger.Info("Downloading model from server...");
            // Start timeout timer in case the model isn't sent back.
            m_modelRequestTimeout.Enabled = true;
            m_numNudges = 0;
        }

        public void emit(string eventName, dynamic payload) {
            m_socket.Emit(eventName, JObject.FromObject(payload));
        }
        public void emit(string eventName) {
            m_socket.Emit(eventName);
        }

        public void modelRequestTimedOut(object sender, ElapsedEventArgs e) {
            if (m_modelDownloadInProgress) {
                // Received part of model since last timeout: reset timer.
                if (m_modelDownloadProgressed) {
                    m_modelDownloadProgressed = false;
                    m_modelRequestTimeout.Enabled = true;
                }
                else {
                    if (m_numNudges >= MAX_NUM_NUDGES) {
                        if (logger.IsInfoEnabled)
                            logger.Info("Model request timed out " + MAX_NUM_NUDGES + " times. Reconnecting...");
                        closeConnection();
                        openConnection();
                    }
                    else {
                        if (logger.IsInfoEnabled)
                            logger.Info("Model request timeout. Server nudged.");
                        emit("nudge!~");
                        m_modelRequestTimeout.Enabled = true;
                        m_numNudges += 1;
                    }
                }
            }
        }

        /// <summary>
        /// https://github.com/Quobject/SocketIoClientDotNet
        /// NOTE: http://socketio4net.codeplex.com/ does not support nodejs' socket.io 1.0.0 or above.
        /// </summary>
        Socket m_socket;
        public void initSocket() {
            if (logger.IsInfoEnabled)
                logger.Info("Connecting to " + m_hostIP + ":" + m_hostPort);

            string url = m_hostIP + ":" + m_hostPort + "/";
            Options options = new Options();
            //options.AutoConnect = false; //Enabling this means we must call .Connect() manually on init.
            options.IgnoreServerCertificateValidation = true;
            options.Reconnection = false; //Prevents the client from automatically attempting to reconnect.
            //options.ReconnectionDelay = 3;
            options.Secure = true;
            options.Timeout = 5000;
            // we need to set transport options
            // otherwise the first connection attempt will be delayed by the pingInterval
            // there is no explanation yet why this happens
            // no transport options -> delay
            //options.Transports = ImmutableList.Create(new string[] { Polling.NAME, WebSocket.NAME }); // default setting; delay
            //options.Transports = ImmutableList.Create(WebSocket.NAME); // no delay
            //options.Transports = ImmutableList.Create(Polling.NAME); // no delay
            options.Transports = ImmutableList.Create(new string[] { WebSocket.NAME, Polling.NAME }); // no delay; WebSocket is default, Polling is fallback
            m_socket = IO.Socket(new Uri(url), options); // url to nodejs
            m_socket.On(Socket.EVENT_CONNECT, () => {
                m_connectionError = false;
                m_socketID = this.m_socket.Io().EngineSocket.Id;
                if (url.StartsWith("https")) {
                    sendPassword(m_uiForm.m_cwSecurity.m_password);
                }
                else {
                    if (logger.IsInfoEnabled)
                        logger.Info("Connected!");
                    if (m_uiForm.m_cwSettings.isCloseSettingsForm) { // Connect, or Reconnect clicked and connection successful
                        m_uiForm.m_cwSettings.DialogResult = DialogResult.OK; // close Settings form
                        m_uiForm.m_cwWidget.enable(); // enable ContextuWall widget
                    }
                    else {
                        m_uiForm.m_cwSettings.setOkButtonText("Ok"); // Apply clicked and connection successful, keep Settings form open, change button text to Ok
                        m_uiForm.m_cwSettings.hideElementsWhileConnecting();
                    }
                    connected = true;
                    m_intendedConnectionClose = false;
                    m_uiForm.refresh(); //Clears the wall and requests the new model from server.
                    m_uiForm.changeActivationContextMenuStripItems(true);
                }
            });

            m_socket.On(Socket.EVENT_CONNECT_ERROR, (e) => {

                string str = "Connection error";
                string message = "";
                if (e.GetType().Equals(typeof(string))) {
                    message = (string) e;
                    if (message.Length > 0) {
                        str = str + ": " + message;
                    }
                }
                if (e.GetType().Equals(typeof(SocketIOException))) {
                    str = str + evaluateException(((SocketIOException) e).code, ((SocketIOException) e).Message);
                    if (logger.IsDebugEnabled) {
                        logger.Debug("Exception\n" + ((SocketIOException) e).ToString());
                    }
                }
                if (logger.IsErrorEnabled)
                    logger.Error(str);
                m_connectionError = true;
                m_uiForm.m_cwSettings.hideElementsWhileConnecting();
                //m_uiForm.m_cwWidget.disable();
                if (message.Equals("wrong password")) {
                    m_intendedConnectionClose = true;
                    MessageBox.Show("Could not connect to " + m_hostIP + ":" + m_hostPort + ".\nPassword is not correct!", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else {
                    if (message.Length == 0) {
                        MessageBox.Show("Could not connect to " + m_hostIP + ":" + m_hostPort + "!", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else {
                        MessageBox.Show("Could not connect to " + m_hostIP + ":" + m_hostPort + "!\nError: " + message, "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                //m_uiForm.m_cwWidget.enable();

            });

            m_socket.On(Socket.EVENT_CONNECT_TIMEOUT, () => {

                if (!m_connectionError) {
                    if (logger.IsInfoEnabled)
                        logger.Info("Connection timeout!");
                    //m_uiForm.m_cwWidget.disable();
                    MessageBox.Show("Could not connect to " + m_hostIP + ":" + m_hostPort + "!", "Connection Timeout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //m_uiForm.m_cwWidget.enable();
                }
                else {
                    m_connectionError = false;
                }
            });

            m_socket.On(Socket.EVENT_DISCONNECT, (reason) => {

                if (logger.IsInfoEnabled)
                    logger.Info("Disconnected from " + m_hostIP + ":" + m_hostPort + " (reason: " + (string) reason + ")");
                connected = false;
                m_lastConnectedHost = m_hostIP + ":" + m_hostPort.ToString();
                m_uiForm.clearWall(false);
                m_uiForm.changeActivationContextMenuStripItems(false);
                m_uiForm.invokeHide(false);
                if (!m_intendedConnectionClose) {
                    DialogResult dialogResult = MessageBox.Show("Disconnected from " + m_hostIP + ":" + m_hostPort + "! Reconnect?",
                        "Disconnected", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (dialogResult == DialogResult.Yes) {
                        m_uiForm.reconnect();
                    }
                }

            });

            // NOTE: Reconnection has been disabled, cause it's difficult to control the socket during reconnection.
            m_socket.On(Socket.EVENT_RECONNECT, () => {
                if (logger.IsInfoEnabled)
                    logger.Info("Reconnected!");
            });

            m_socket.On(Socket.EVENT_RECONNECT_ATTEMPT, () => {
                if (logger.IsInfoEnabled)
                    logger.Info("Attempting reconnect to " + m_hostIP + ":" + m_hostPort);
            });

            m_socket.On(Socket.EVENT_RECONNECT_ERROR, () => {
                //if (logger.IsErrorEnabled)
                //    logger.Error("Reconnection error!"); //This message is overkill.
            });

            m_socket.On(Socket.EVENT_RECONNECT_FAILED, () => {
                if (logger.IsErrorEnabled)
                    logger.Error("Reconnect failed!");
            });

            m_socket.On(Socket.EVENT_RECONNECTING, () => {
                //if (logger.IsInfoEnabled)
                //    logger.Info("Reconnecting..."); //This message is overkill.
            });

            m_socket.On(Socket.EVENT_ERROR, (e) => {
                string str = "Socket error";
                if (e.GetType().Equals(typeof(EngineIOException))) {
                    str = str + evaluateException(((EngineIOException) e).code, ((EngineIOException) e).Message);
                    if (logger.IsDebugEnabled) {
                        logger.Debug("Exception\n" + ((EngineIOException) e).ToString());
                    }
                }
                if (logger.IsErrorEnabled)
                    logger.Error(str);
            });

            // Handles any low-level linuxSocket-type messages.
            m_socket.On(Socket.EVENT_MESSAGE, (msg) => {
                if (logger.IsInfoEnabled)
                    logger.Info("incoming message: " + (string) msg);
            });

            m_socket.On("authenticate_ok", () => {
                if (logger.IsDebugEnabled)
                    logger.Debug("Authentification ok!");
                if (logger.IsInfoEnabled)
                    logger.Info("Connected!");
                if (m_uiForm.m_cwSettings.isCloseSettingsForm) { // Connect, or Reconnect clicked and connection successful
                    m_uiForm.m_cwSettings.DialogResult = DialogResult.OK; // close Settings form
                    m_uiForm.m_cwWidget.enable(); // enable ContextuWall widget
                }
                else {
                    m_uiForm.m_cwSettings.setOkButtonText("Ok"); // Apply clicked and connection successful, keep Settings form open, change button text to Ok
                    m_uiForm.m_cwSettings.hideElementsWhileConnecting();
                }
                connected = true;
                m_intendedConnectionClose = false;
                m_uiForm.refresh(); //Clears the wall and requests the new model from server.
                m_uiForm.changeActivationContextMenuStripItems(true);
            });

            // Handles incoming display specifications of displays connected anywhere on the network.
            m_socket.On("incomingDisplaySpecs", (data) => {
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "name", "width", "height" }, "display specs") &&
                    (int) packet.width > 0 && (int) packet.height > 0) {
                    if (logger.IsInfoEnabled)
                        logger.Info("incoming display specs: " + packet.name + " (" + packet.width + "x" + packet.height + ")");
                    m_modelDownloadProgressed = true;
                    m_uiForm.m_cwDisplayList.invokeRegisterDisplay(new CWDisplay((string) packet.name, (int) packet.width, (int) packet.height));
                }
                else {
                    if (logger.IsErrorEnabled)
                        logger.Error("incoming display specs: Could not verify incoming data!");
                }
            });
            m_socket.On("removeDisplaySpecs", (data) => {
                string packet = (string) data;
                if (logger.IsInfoEnabled)
                    logger.Info("removing display specs: " + packet);
                m_uiForm.m_cwDisplayList.invokeDeregisterDisplay(packet);
            });

            m_socket.On("incomingImageAck", (data) => {

                // dynamic packet = data; // packet.filename
                m_uiForm.m_cwWidget.hideElementsProgress();
                if (logger.IsInfoEnabled)
                    logger.Info("Uploaded full image to server.");

            });

            m_socket.On("startSendingFullImage", (data) => {

                // dynamic packet = data; // packet.filename
                m_uiForm.m_cwWidget.showElementsProgress(true, "Downloading image...");
                if (logger.IsInfoEnabled)
                    logger.Info("Downloading full image from server...");

            });

            m_socket.On("incomingShadowImage", (data) => {
                m_modelDownloadProgressed = true;
                if (!m_ignoreIncomingDownloads) {
                    dynamic packet = data;
                    // ignore packet if fields could not be verified
                    if (verifyPacket(packet, new string[] { "filename", "m_width", "m_height", "m_scale", "m_thumbnail64", "m_normalisedLocation" }, "image data") &&
                        (int) packet.m_width > 0 && (int) packet.m_height > 0) {
                        if (logger.IsInfoEnabled)
                            logger.Info("incoming download: " + packet.filename);
                        //Image image = Base64ToImage(incomingImage.image);
                        //image.Save(incomingImage.filename, System.Drawing.Imaging.ImageFormat.Png);
                        CWImage shadow = new CWImage(m_uiForm, (string) packet.filename, (int) packet.m_width, (int) packet.m_height, (float) packet.m_scale, (string) packet.m_thumbnail64);
                        PointF location = m_uiForm.m_cwWallPanel.denormaliseLocation(new PointF((float) packet.m_normalisedLocation.X, (float) packet.m_normalisedLocation.Y));
                        // Check this a second time, in case the value changed while the image was loading.
                        if (!m_ignoreIncomingDownloads) {
                            m_uiForm.addShadowToWall(shadow, location);
                        }
                        else {
                            shadow.Dispose();
                        }
                    }
                    else {
                        if (logger.IsErrorEnabled)
                            logger.Error("incoming download: Could not verify incoming data!");
                    }
                }
            });

            m_socket.On("incomingAnnotation", (data) => {
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "m_ownerID", "imageFilename", "m_layer64" }, "annotation layer")) {
                    if (logger.IsInfoEnabled)
                        logger.Info("incoming annotation layer: " + (string) packet.m_ownerID);
                    m_modelDownloadProgressed = true;
                    CWImage image = (CWImage) m_uiForm.m_cwWall.getImageWithFilename((string) packet.imageFilename);
                    CWAnnotationLayer annotation = (CWAnnotationLayer) image.getAnnotationWithOwnerID((string) packet.m_ownerID);
                    if (annotation == null) {
                        annotation = new CWAnnotationLayer(m_uiForm, image, (string) packet.m_ownerID, (Bitmap) Base64ToImage((string) packet.m_layer64));
                        image.addAnnotation(annotation);
                    }
                    else {
                        annotation.update((Bitmap) Base64ToImage((string) packet.m_layer64));
                    }
                    image.flattenAnnotations();
                    m_uiForm.m_cwAnnotationMode.setCanvasDirty();
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("updateImageLocation", (data) => {
                //Console.Write("incoming location update: ");
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename", "m_normalisedLocation" }, "image location data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    PointF location = m_uiForm.m_cwWallPanel.denormaliseLocation(new PointF((float) packet.m_normalisedLocation.X, (float) packet.m_normalisedLocation.Y));
                    image.setLocation(location, false);
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("resizeImage", (data) => {
                //Console.Write("incoming image resize: ");
                dynamic packet = data;
                if (verifyPacket(packet, new string[] { "filename", "m_scale" }, "image resize data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    image.setScale((float) packet.m_scale, false);
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("bringImageToFront", (data) => {
                //Console.Write("incoming bring-image-to-front: ");
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename" }, "image order data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    image.bringToFront(false);
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("sendImageToBack", (data) => {
                //Console.Write("incoming bring-image-to-front: ");
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename" }, "image order data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    image.sendToBack(false);
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("fullImageSent", (data) => {
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename", "m_image64" }, "image data")) {
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    try {
                        //using (Bitmap b = new Bitmap(Base64ToImage((string) packet.m_image64))) { // doesn't work here, gives an exception in annotation mode
                        Bitmap b = new Bitmap(Base64ToImage((string) packet.m_image64));
                                //image.fullFilepath = Program.WORKING_DIR + image.filename;
                                image.fullFilepath = Path.Combine(Program.getTmpImagesFolder(), image.filename);
                            b.Save(image.fullFilepath, ImageFormat.Png);
                            //TODO: Use callbacks here..
                            if (image.m_saveRequested) {
                                if (image.m_saveWithAnnotations) {
                                    using (Graphics g = Graphics.FromImage(b)) {
                                        foreach (ICWAnnotationLayer al in image.getAnnotations()) {
                                            g.DrawImage(al.getCommittedCopy(), 0, 0, b.Width, b.Height);
                                        }
                                    }
                                }
                                b.Save(image.m_desiredSaveFilePath, image.m_desiredSaveFileFormat);
                                image.m_saveRequested = false;
                            }
                            if (m_uiForm.m_cwAnnotationMode.active) {
                                m_uiForm.m_cwAnnotationMode.setFullImage(b);
                                m_uiForm.Invalidate();
                            }
                        //}
                    } catch (Exception exception) {
                        if (logger.IsErrorEnabled)
                            logger.Error("Could not save image: " + exception.Message);
                    }
                }
                m_uiForm.m_cwWidget.hideElementsProgress();
            });

            m_socket.On("removeImage", (data) => {
                //Console.Write("incoming image removal: ");
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename" }, "remove image data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    image.remove(false);
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("lockImage", (data) => {
                //Console.Write("incoming image lock: ");
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename" }, "lock image data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    image.locked = true;
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("unlockImage", (data) => {
                //Console.Write("incoming image unlock: ");
                dynamic packet = data;
                // ignore packet if fields could not be verified
                if (verifyPacket(packet, new string[] { "filename" }, "unlock image data")) {
                    //Console.WriteLine(packet);
                    ICWImage image = m_uiForm.m_cwWall.getImageWithFilename((string) packet.filename);
                    if (image == null)
                        return;
                    image.locked = false;
                    m_uiForm.Invalidate();
                }
            });

            m_socket.On("clearWall", () => {
                //Console.Write("incoming clear wall command");
                m_uiForm.clearWall(false);
            });

            m_socket.On("modelSent", () => {
                //Console.Write("incoming clear wall command");
                m_modelDownloadInProgress = false;
                m_ignoreIncomingDownloads = false;
                m_uiForm.Invalidate();
                m_uiForm.m_cwWidget.hideElementsProgress();
                if (logger.IsInfoEnabled)
                    logger.Info("Model downloaded.");
            });
        }

        public void assertConnected() {
            if (!connected) {
                m_uiForm.m_cwWidget.disable();
                MessageBox.Show("ContextuWall is not connected to a ContextuWall server!\n" +
                    "Cannot perform this action without a server connection!", "No Server Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                m_uiForm.m_cwWidget.enable();
                throw new Exception();
            }
        }

        public bool isConnected() {
            return connected;
        }

        public void openConnection(String ip, ushort port) {
            m_hostIP = ip;
            m_hostPort = port;
            initSocket();
        }
        public void openConnection() {
            initSocket();
        }

        public void closeConnection() {

            m_intendedConnectionClose = true;
            m_socket.Close();

        }

        ///<summary>
        ///Verifies if a received <code>packet</code> contains the expected data <code>fields</code>.
        ///</summary>
        ///<param name="packet">the received packet</param>
        ///<param name="fields">fields to verify</param>
        ///<param name="item">text describing the packet</param>
        ///<exception cref="Exception">
        ///Thrown if the <paramref name="packet"/> cannot be casted as <code>JObject</code>.
        ///Thrown if the <paramref name="fields"/> cannot be casted as <code>JToken</code>.
        ///</exception>
        ///<returns><code>true</code> or <code>false</code></returns>
        private bool verifyPacket(dynamic packet, string[] fields, string item) {

            bool verified = true;
            try {
                JObject jObject = (JObject) packet;
                if (!jObject.HasValues)
                    verified = false;
                foreach (string field in fields) {
                    JToken value;
                    if (!jObject.TryGetValue(field, out value))
                        verified = false;
                }
            } catch (Exception exception) {
                if (logger.IsErrorEnabled)
                    logger.Error("Could not receive " + item + ": " + exception.Message);
                return false;
            }
            if (!verified)
                if (logger.IsErrorEnabled)
                    logger.Error("Could not verify " + item + "!");
            return verified;

        }

        ///<summary>
        ///Evaluates an <code>exception</code>.
        ///</summary>
        ///<param name="code">exception code</param>
        ///<param name="message">exception message</param>
        ///<returns><code>string</code> containing the evaluated exception</returns>
        private string evaluateException(object code, string message) {

            string str = "";
            if (code != null && code.ToString().Length > 0 && message.Length > 0) {
                str = str + ": " + code.ToString() + ", " + message;
            }
            else {
                if (code != null && code.ToString().Length > 0) {
                    str = str + ": " + code.ToString();
                }
                else {
                    if (message.Length > 0) {
                        str = str + ": " + message;
                    }
                }
            }
            return str;

        }

    }
}

