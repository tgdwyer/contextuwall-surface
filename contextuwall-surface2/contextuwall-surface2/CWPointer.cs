﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ContextuWall {
    public class CWPointer {
        public CWUIForm m_uiForm;
        public int m_numberOfActivePointers = 0;
        public CWPointer(CWUIForm uiForm) {
            m_uiForm = uiForm;
            //Win32.EnableMouseInPointer(true);
        }
        public bool DecodePointer(ref Message m) {
            //Console.WriteLine(@"DecodePointer");
            bool handled = false;
            int pointerID = Win32.GET_POINTER_ID(m.WParam);
            Win32.POINTER_INFO pi = new Win32.POINTER_INFO();
            if (!Win32.GetPointerInfo(pointerID, ref pi)) {
                Win32.CheckLastError();
            }
            IEnumerable<ICWImage> images = m_uiForm.m_cwWall.getImagesReversed();
            switch (m.Msg) {
                case Win32.WM_POINTERDOWN: {
                        if ((pi.PointerFlags & Win32.POINTER_FLAGS.PRIMARY) != 0) {
                            m_uiForm.Capture = true;
                        }
                        // Kill the image context menu if it's open (it doesn't always autohide for some reason..)
                        if (CWImage.m_curentOpenContextMenu != null) {
                            CWImage.m_curentOpenContextMenu.Hide();
                            CWImage.m_curentOpenContextMenu = null;
                        }

                        Point pt = m_uiForm.PointToClient(pi.PtPixelLocation.ToPoint());
                        if (m_uiForm.m_cwWallPanel.containsPoint(pt)) {
                            // Iterate backwards over the images, so that the topmost one is considered first.
                            foreach (CWImage f in images) {
                                if (f.containsPoint(pt)) {
                                    m_uiForm.m_cwWallPanel.highlight(this, true);
                                    f.isSelected = true;
                                    m_numberOfActivePointers++;

                                    f.bringToFront(true);
                                    f.AddPointer(pointerID);
                                    f.ProcessPointerFrames(pointerID, pi.FrameID);
                                    //if (f.Pivot)
                                    //{
                                    //    Point pt1 = Point.Ceiling(f.Points[0]);
                                    //    Point pt0 = Point.Ceiling(f.Points[3]);
                                    //    pt0.X = (pt0.X + pt1.X) / 2;
                                    //    pt0.Y = (pt0.Y + pt1.Y) / 2;
                                    //    pt0 = m_uiPanel.m_uiForm.PointToScreen(pt0);
                                    //    Win32.SetPivotInteractionContext(f.Context, pt0.X, pt0.Y, 0f);
                                    //}
                                    handled = true;
                                    break;
                                }
                            }
                            if (!handled) {
                                m_uiForm.m_cwWallPanel.AddPointer(pointerID);
                                m_uiForm.m_cwWallPanel.ProcessPointerFrames(pointerID, pi.FrameID);
                                handled = true;
                            }
                        }
                        else {
                            if (m_uiForm.m_cwAnnotationMode.active && m_uiForm.m_cwWallPanel.m_annotationRect.Contains(pt)) {
                                m_uiForm.m_cwAnnotationMode.AddPointer(pointerID);
                                m_uiForm.m_cwAnnotationMode.ProcessPointerFrames(pointerID, pi.FrameID);
                            }
                            else {
                                m_uiForm.m_cwGestureManager.AddPointer(pointerID);
                                m_uiForm.m_cwGestureManager.ProcessPointerFrames(pointerID, pi.FrameID);
                                m_uiForm.m_cwWallPanel.highlight(m_uiForm.m_cwGestureManager, m_uiForm.m_cwGestureManager.ActivePointers.Count == 1);
                                handled = true;
                            }
                        }
                    }
                    break;

                case Win32.WM_POINTERUP:
                    if ((pi.PointerFlags & Win32.POINTER_FLAGS.PRIMARY) != 0) {
                        m_uiForm.Capture = false;
                    }
                    foreach (CWImage f in images) {
                        if (f.ActivePointers.Contains(pointerID)) {
                            if (--m_numberOfActivePointers == 0)
                                m_uiForm.m_cwWallPanel.highlight(this, false);
                            f.isSelected = false;
                            m_uiForm.m_cwNetwork.sendUnlockCommand(f);

                            f.ProcessPointerFrames(pointerID, pi.FrameID);
                            f.RemovePointer(pointerID);
                            handled = true;
                            break;
                        }
                    }
                    if (!handled) {
                        if (m_uiForm.m_cwWallPanel.ActivePointers.Contains(pointerID)) {
                            m_uiForm.m_cwWallPanel.ProcessPointerFrames(pointerID, pi.FrameID);
                            m_uiForm.m_cwWallPanel.RemovePointer(pointerID);
                        }
                        else {
                            if (m_uiForm.m_cwAnnotationMode.active && m_uiForm.m_cwAnnotationMode.ActivePointers.Contains(pointerID)) {
                                m_uiForm.m_cwAnnotationMode.ProcessPointerFrames(pointerID, pi.FrameID);
                                m_uiForm.m_cwAnnotationMode.RemovePointer(pointerID);
                            }
                            else {
                                if (m_uiForm.m_cwGestureManager.ActivePointers.Contains(pointerID)) {
                                    m_uiForm.m_cwGestureManager.ProcessPointerFrames(pointerID, pi.FrameID);
                                    m_uiForm.m_cwGestureManager.RemovePointer(pointerID);
                                    m_uiForm.m_cwWallPanel.highlight(m_uiForm.m_cwGestureManager, false);
                                }
                            }
                        }
                        handled = true;
                    }
                    break;

                case Win32.WM_POINTERUPDATE:
                    foreach (CWImage f in images) {
                        if (f.ActivePointers.Contains(pointerID)) {
                            f.ProcessPointerFrames(pointerID, pi.FrameID);
                            handled = true;
                            break;
                        }
                    }
                    if (!handled) {
                        if (m_uiForm.m_cwWallPanel.ActivePointers.Contains(pointerID)) {
                            m_uiForm.m_cwWallPanel.ProcessPointerFrames(pointerID, pi.FrameID);
                        }
                        else {
                            if (m_uiForm.m_cwAnnotationMode.active && m_uiForm.m_cwAnnotationMode.ActivePointers.Contains(pointerID)) {
                                m_uiForm.m_cwAnnotationMode.ProcessPointerFrames(pointerID, pi.FrameID);
                            }
                            else {
                                if (m_uiForm.m_cwGestureManager.ActivePointers.Contains(pointerID)) {
                                    m_uiForm.m_cwGestureManager.ProcessPointerFrames(pointerID, pi.FrameID);
                                }
                            }
                        }
                        handled = true;
                    }
                    break;

                case Win32.WM_POINTERCAPTURECHANGED:
                    m_uiForm.Capture = false;
                    foreach (CWImage f in images) {
                        if (f.ActivePointers.Contains(pointerID)) {
                            f.StopProcessing();
                            handled = true;
                            break;
                        }
                    }
                    if (!handled) {
                        if (m_uiForm.m_cwWallPanel.ActivePointers.Contains(pointerID)) {
                            m_uiForm.m_cwWallPanel.StopProcessing();
                        }
                        else {
                            if (m_uiForm.m_cwAnnotationMode.active && m_uiForm.m_cwAnnotationMode.ActivePointers.Contains(pointerID)) {
                                m_uiForm.m_cwAnnotationMode.StopProcessing();
                            }
                            else {
                                if (m_uiForm.m_cwGestureManager.ActivePointers.Contains(pointerID)) {
                                    m_uiForm.m_cwGestureManager.StopProcessing();
                                }
                            }
                        }
                        handled = true;
                    }
                    break;
            }
            m.Result = IntPtr.Zero;
            return handled;
        }
    }
}
