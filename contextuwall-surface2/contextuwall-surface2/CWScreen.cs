﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ContextuWall {
    public class CWScreen {

        //http://stackoverflow.com/questions/891345/get-a-screenshot-of-a-specific-application
        [StructLayout(LayoutKind.Sequential)]
        public struct Rect {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

        public void captureApplication(string procName) {
            var rect = new Rect();
            Process proc = Process.GetProcessesByName(procName)[0];
            GetWindowRect(proc.MainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            using (Bitmap bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb)) {
                using (Graphics g = Graphics.FromImage(bmp)) {
                    g.CopyFromScreen(rect.left, rect.top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
                }
                //bmp.Save("c:\\tmp\\test.png", ImageFormat.Png);
                bmp.Save("test.png", ImageFormat.Png);
            }
        }

        public String getScreenshot(int x, int y, int width, int height) {
            String filename = CWCommon.getUniqueFilename();

            using (Bitmap bmpScreenCapture = new Bitmap((int) (width), (int) (height))) {
                using (Graphics g = Graphics.FromImage(bmpScreenCapture as Image)) {
                    g.CopyFromScreen(x, y,
                                     0, 0,
                                     bmpScreenCapture.Size,
                                     CopyPixelOperation.SourceCopy);
                }
                bmpScreenCapture.Save(filename, ImageFormat.Png);
            }
            return filename;
        }

        public void getClippedScreenshot() {

        }


    }
}
