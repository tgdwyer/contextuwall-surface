﻿using log4net;
using System;
using System.Security.Cryptography;
using System.Text;

namespace ContextuWall {
    public class CWSecurity {

        public CWUIForm m_uiForm;
        public string m_password { get; set; }

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CWSecurity(CWUIForm uiForm) {

            this.m_uiForm = uiForm;
            if (Properties.Settings.Default.Password.Length > 0) {
                this.m_password = Properties.Settings.Default.Password[0];
            }
            else {
                this.m_password = "";
            }

        }

        ///<summary>
        ///Calculates the MD5 hash of an <code>input</code> string and returns the MD5 hash as string in the specified <code>format</code>.
        ///</summary>
        ///<param name="input">input string</param>
        ///<param name="format">format of MD5 hash string</param>
        ///<exception cref="Exception">
        ///Thrown if an error occurs.
        ///</exception>
        ///<returns>MD5 hash string in specified <code>format</code></returns>
        public string calculateMD5Hash(string input, string format) {

            try {
                MD5 md5 = MD5.Create();
                byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
                StringBuilder stringBuilder = new StringBuilder();
                for (int k = 0; k < hash.Length; k++) {
                    stringBuilder.Append(hash[k].ToString(format));
                }
                return stringBuilder.ToString();
            } catch (Exception exception) {
                if (logger.IsErrorEnabled)
                    logger.Error(exception.Message);
                return null;
            }

        }

    }

}
