﻿namespace ContextuWall {
    partial class CWSettingsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.cbIPs = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOpacity = new System.Windows.Forms.TextBox();
            this.labelWallAspectRatio = new System.Windows.Forms.Label();
            this.txtWallAspectRatio = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.chkShowSidebars = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtMinImageHeightScaling = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.progressBarConnecting = new System.Windows.Forms.ProgressBar();
            this.labelConnecting = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(178, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Address";
            this.toolTip.SetToolTip(this.label1, "IP address or URL of the ContexuWall server.");
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(211, 500);
            this.btnApply.Margin = new System.Windows.Forms.Padding(4);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(150, 40);
            this.btnApply.TabIndex = 8;
            this.btnApply.Text = "Apply";
            this.toolTip.SetToolTip(this.btnApply, "Apply current settings and connect to server.");
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // cbIPs
            // 
            this.cbIPs.AllowDrop = true;
            this.cbIPs.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIPs.FormattingEnabled = true;
            this.cbIPs.Location = new System.Drawing.Point(321, 53);
            this.cbIPs.Margin = new System.Windows.Forms.Padding(4);
            this.cbIPs.MaxDropDownItems = 10;
            this.cbIPs.MaxLength = 255;
            this.cbIPs.Name = "cbIPs";
            this.cbIPs.Size = new System.Drawing.Size(380, 37);
            this.cbIPs.TabIndex = 0;
            this.toolTip.SetToolTip(this.cbIPs, "IP address or URL of the ContexuWall server.");
            this.cbIPs.SelectedIndexChanged += new System.EventHandler(this.cbIPs_SelectedIndexChanged);
            this.cbIPs.TextChanged += new System.EventHandler(this.cbIPs_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(122, 267);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 29);
            this.label3.TabIndex = 0;
            this.label3.Text = "Opacity (0...255)";
            this.toolTip.SetToolTip(this.label3, "Opacity of the ContextuWall panel (integer value between 0 and 255).");
            // 
            // txtOpacity
            // 
            this.txtOpacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOpacity.Location = new System.Drawing.Point(321, 264);
            this.txtOpacity.Margin = new System.Windows.Forms.Padding(1);
            this.txtOpacity.MaxLength = 3;
            this.txtOpacity.Name = "txtOpacity";
            this.txtOpacity.Size = new System.Drawing.Size(90, 35);
            this.txtOpacity.TabIndex = 3;
            this.toolTip.SetToolTip(this.txtOpacity, "Opacity of the ContextuWall panel (integer value between 0 and 255).");
            // 
            // labelWallAspectRatio
            // 
            this.labelWallAspectRatio.AutoSize = true;
            this.labelWallAspectRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWallAspectRatio.Location = new System.Drawing.Point(160, 321);
            this.labelWallAspectRatio.Margin = new System.Windows.Forms.Padding(0);
            this.labelWallAspectRatio.Name = "labelWallAspectRatio";
            this.labelWallAspectRatio.Size = new System.Drawing.Size(148, 29);
            this.labelWallAspectRatio.TabIndex = 0;
            this.labelWallAspectRatio.Text = "Aspect Ratio";
            this.toolTip.SetToolTip(this.labelWallAspectRatio, "Aspect ratio of the ContextuWall panel.");
            // 
            // txtWallAspectRatio
            // 
            this.txtWallAspectRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWallAspectRatio.Location = new System.Drawing.Point(321, 318);
            this.txtWallAspectRatio.Margin = new System.Windows.Forms.Padding(1);
            this.txtWallAspectRatio.MaxLength = 5;
            this.txtWallAspectRatio.Name = "txtWallAspectRatio";
            this.txtWallAspectRatio.Size = new System.Drawing.Size(90, 35);
            this.txtWallAspectRatio.TabIndex = 4;
            this.toolTip.SetToolTip(this.txtWallAspectRatio, "Aspect ratio of the ContextuWall panel.");
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(381, 500);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(150, 40);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "Ok";
            this.toolTip.SetToolTip(this.btnOk, "Apply current settings, connect to server and close dialog.");
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(133, 112);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 29);
            this.label2.TabIndex = 0;
            this.label2.Text = "Port (0...65535)";
            this.toolTip.SetToolTip(this.label2, "Port number of the ContexuWall server.");
            // 
            // txtPort
            // 
            this.txtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(321, 109);
            this.txtPort.Margin = new System.Windows.Forms.Padding(1);
            this.txtPort.MaxLength = 5;
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(90, 35);
            this.txtPort.TabIndex = 1;
            this.toolTip.SetToolTip(this.txtPort, "Port number of the ContexuWall server.");
            // 
            // chkShowSidebars
            // 
            this.chkShowSidebars.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkShowSidebars.Location = new System.Drawing.Point(321, 427);
            this.chkShowSidebars.Margin = new System.Windows.Forms.Padding(0);
            this.chkShowSidebars.Name = "chkShowSidebars";
            this.chkShowSidebars.Size = new System.Drawing.Size(50, 33);
            this.chkShowSidebars.TabIndex = 6;
            this.toolTip.SetToolTip(this.chkShowSidebars, "Show or hide sidebars of the ContextuWall panel.");
            this.chkShowSidebars.UseVisualStyleBackColor = true;
            this.chkShowSidebars.CheckedChanged += new System.EventHandler(this.chkShowSidebars_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(131, 429);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 29);
            this.label5.TabIndex = 0;
            this.label5.Text = "Show Sidebars";
            this.toolTip.SetToolTip(this.label5, "Show or hide sidebars of the ContextuWall panel.");
            // 
            // txtMinImageHeightScaling
            // 
            this.txtMinImageHeightScaling.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinImageHeightScaling.Location = new System.Drawing.Point(321, 372);
            this.txtMinImageHeightScaling.Margin = new System.Windows.Forms.Padding(1);
            this.txtMinImageHeightScaling.MaxLength = 5;
            this.txtMinImageHeightScaling.Name = "txtMinImageHeightScaling";
            this.txtMinImageHeightScaling.Size = new System.Drawing.Size(90, 35);
            this.txtMinImageHeightScaling.TabIndex = 5;
            this.toolTip.SetToolTip(this.txtMinImageHeightScaling, "Minimal initial height of images on the ContextuWall panel (value between 0 and 1" +
        " = height of panel).");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 375);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(267, 29);
            this.label6.TabIndex = 0;
            this.label6.Text = "Min Image Height (0...1)";
            this.toolTip.SetToolTip(this.label6, "Minimal initial height of images on the ContextuWall panel (value between 0 and 1" +
        " = height of panel).");
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(321, 165);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(1);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(380, 35);
            this.txtPassword.TabIndex = 2;
            this.toolTip.SetToolTip(this.txtPassword, "Password for the ContexuWall server.");
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword.Location = new System.Drawing.Point(188, 168);
            this.labelPassword.Margin = new System.Windows.Forms.Padding(0);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(120, 29);
            this.labelPassword.TabIndex = 0;
            this.labelPassword.Text = "Password";
            this.toolTip.SetToolTip(this.labelPassword, "Password for the ContexuWall server.");
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(21, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 200);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(21, 230);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(700, 251);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ContextuWall";
            // 
            // progressBarConnecting
            // 
            this.progressBarConnecting.Location = new System.Drawing.Point(381, 505);
            this.progressBarConnecting.MarqueeAnimationSpeed = 0;
            this.progressBarConnecting.Name = "progressBarConnecting";
            this.progressBarConnecting.Size = new System.Drawing.Size(200, 30);
            this.progressBarConnecting.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarConnecting.TabIndex = 0;
            this.progressBarConnecting.Visible = false;
            // 
            // labelConnecting
            // 
            this.labelConnecting.AutoSize = true;
            this.labelConnecting.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnecting.Location = new System.Drawing.Point(202, 505);
            this.labelConnecting.Margin = new System.Windows.Forms.Padding(0);
            this.labelConnecting.Name = "labelConnecting";
            this.labelConnecting.Size = new System.Drawing.Size(159, 29);
            this.labelConnecting.TabIndex = 9;
            this.labelConnecting.Text = "Connecting ...";
            this.labelConnecting.Visible = false;
            // 
            // CWSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(110F, 110F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(742, 559);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.labelConnecting);
            this.Controls.Add(this.progressBarConnecting);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMinImageHeightScaling);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkShowSidebars);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelWallAspectRatio);
            this.Controls.Add(this.txtWallAspectRatio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtOpacity);
            this.Controls.Add(this.cbIPs);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CWSettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CWSettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.CWSettingsForm_Load);
            this.Shown += new System.EventHandler(this.CWSettingsForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CWSettingsForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.ComboBox cbIPs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOpacity;
        private System.Windows.Forms.Label labelWallAspectRatio;
        private System.Windows.Forms.TextBox txtWallAspectRatio;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.CheckBox chkShowSidebars;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TextBox txtMinImageHeightScaling;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ProgressBar progressBarConnecting;
        private System.Windows.Forms.Label labelConnecting;
    }
}