﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ContextuWall {
    public partial class CWSettingsForm : Form {
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        public CWUIForm m_uiForm;
        bool m_appInit;
        public bool isCloseSettingsForm { get; set; }
        private float minWallAspectRatio;
        private static readonly string dummyPassword = "*************************";
        private bool passwordChanged = false;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CWSettingsForm(CWUIForm uiForm, bool appInit) {
            InitializeComponent();
            if (appInit) {
                //hide Apply button
                btnApply.Hide();
                btnOk.Text = "Connect";
                //move Ok button horizontally into the centre
                btnOk.Location = new Point((btnApply.Location.X + btnApply.Size.Width + btnOk.Location.X - btnOk.Size.Width) / 2, btnOk.Location.Y);
            }
            else {
                if (!uiForm.m_cwNetwork.isConnected()) {
                    btnOk.Text = "Reconnect";
                }
            }
            this.m_uiForm = uiForm;
            this.m_appInit = appInit;
            this.isCloseSettingsForm = false;
        }

        private void applyChanges() {

            this.DialogResult = DialogResult.None;
            bool redrawUI = false;
            int index;

            string ip = cbIPs.Text;
            if (ip.Length == 0 || !ip.StartsWith("http")) {
                return;
            }

            ushort port;
            try {
                port = ushort.Parse(txtPort.Text);
            } catch (Exception) {
                txtPort.Text = m_uiForm.m_cwNetwork.m_hostPort.ToString();
                return;
            }

            string password = null;
            if (ip.StartsWith("https")) {
                index = Properties.Settings.Default.CommandServerIP.ToList().IndexOf(ip);
                if (!passwordChanged && index > -1 && Properties.Settings.Default.Password.Length > index) { // user selected server from list and didn't change password
                    password = Properties.Settings.Default.Password[index];
                }
                else {
                    if ((passwordChanged && !txtPassword.Text.Equals(dummyPassword)) || // user changed password and it's different from the dummy password
                        (!passwordChanged && index == -1)) { // user added a new server and left the password empty
                        password = m_uiForm.m_cwSecurity.calculateMD5Hash(txtPassword.Text, "x2");
                    }
                }
                if (password == null) {
                    MessageBox.Show("Could not evaluate password!", "Password Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else {
                password = "empty";
            }

            byte opacity;
            try {
                opacity = byte.Parse(txtOpacity.Text);
            } catch (Exception) {
                txtOpacity.Text = m_uiForm.m_opacity.ToString();
                return;
            }

            float wallAspectRatio;
            try {
                wallAspectRatio = float.Parse(txtWallAspectRatio.Text);
                if (wallAspectRatio < 0.001) {
                    txtWallAspectRatio.Text = m_uiForm.m_cwWallPanel.m_aspectRatio.ToString();
                    return;
                }
                if (wallAspectRatio < minWallAspectRatio) {
                    DialogResult dialogResult = MessageBox.Show("Aspect Ratio should be larger than " + minWallAspectRatio +
                        "! Continue?", "Bad Aspect Ratio", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.No) {
                        return;
                    }
                }
            } catch (Exception) {
                txtWallAspectRatio.Text = m_uiForm.m_cwWallPanel.m_aspectRatio.ToString();
                return;
            }

            float minImageHeightScaling;
            try {
                minImageHeightScaling = float.Parse(txtMinImageHeightScaling.Text);
                if (minImageHeightScaling < 0 || minImageHeightScaling > 1) {
                    txtMinImageHeightScaling.Text = m_uiForm.m_cwWallPanel.m_minImageHeightScaling.ToString();
                    return;
                }
            } catch (Exception) {
                txtMinImageHeightScaling.Text = m_uiForm.m_cwWallPanel.m_minImageHeightScaling.ToString();
                return;
            }

            // connect on app init or change of ip or port
            if (m_appInit ||
                (!ip.Equals(m_uiForm.m_cwNetwork.m_hostIP) || port != m_uiForm.m_cwNetwork.m_hostPort || !password.Equals(m_uiForm.m_cwSecurity.m_password))) {
                try {
                    m_uiForm.m_cwSecurity.m_password = password;
                    m_uiForm.connect(ip, port);
                    showElementsWhileConnecting();
                } catch (UriFormatException) {
                    MessageBox.Show("The IP address is invalid!", "Invalid IP Address", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //m_uiForm.m_cwNetwork.m_hostIP = Properties.Settings.Default.CommandServerIP[0];
                    //m_uiForm.m_cwNetwork.m_hostPort = ushort.Parse(Properties.Settings.Default.CommandServerPort[0]);
                    return;
                }
                // Bring to top of list (create new item if it doesn't exist yet). 
                List<string> ips = Properties.Settings.Default.CommandServerIP.ToList();
                List<string> ports = Properties.Settings.Default.CommandServerPort.ToList();
                List<string> passwords = Properties.Settings.Default.Password.ToList();
                if (ips.Contains(ip)) {
                    index = ips.IndexOf(ip);
                    ips.RemoveAt(index);
                    if (ports.Count > index) {
                        ports.RemoveAt(index);
                    }
                    if (passwords.Count > index) {
                        passwords.RemoveAt(index);
                    }
                    //ips.Remove(ip);
                }
                ips.Insert(0, ip);
                ports.Insert(0, port.ToString());
                passwords.Insert(0, password);
                Properties.Settings.Default.CommandServerIP = ips.ToArray<string>();
                Properties.Settings.Default.CommandServerPort = ports.ToArray<string>();
                Properties.Settings.Default.Password = passwords.ToArray<string>();
            }
            // otherwise reconnect
            else {
                if (!m_uiForm.m_cwNetwork.isConnected()) {
                    m_uiForm.connect();
                    showElementsWhileConnecting();
                }
            }

            if (m_uiForm.m_opacity != opacity) {
                Properties.Settings.Default.Opacity = opacity;
                m_uiForm.invokeUpdateOpacity(opacity);
            }

            if (m_uiForm.m_cwWallPanel.m_aspectRatio != wallAspectRatio) {
                Properties.Settings.Default.WallAspectRatio = wallAspectRatio;
                m_uiForm.m_cwWallPanel.m_aspectRatio = wallAspectRatio;
                redrawUI = true;
            }

            if (m_uiForm.m_cwWallPanel.m_minImageHeightScaling != minImageHeightScaling) {
                Properties.Settings.Default.MinImageHeightScaling = minImageHeightScaling;
                m_uiForm.m_cwWallPanel.m_minImageHeightScaling = minImageHeightScaling;
            }

            if (m_uiForm.m_cwWallPanel.m_showSidebars != chkShowSidebars.Checked) {
                Properties.Settings.Default.ShowSideBars = chkShowSidebars.Checked;
                m_uiForm.m_cwWallPanel.m_showSidebars = chkShowSidebars.Checked;
                redrawUI = true;
            }

            if (redrawUI) {
                m_uiForm.invokeInitUI();
            }
            Properties.Settings.Default.Save();
            //m_uiForm.invokeShow();
        }

        private void btnApply_Click(object sender, EventArgs e) {

            this.isCloseSettingsForm = false;
            applyChanges();

        }

        private void btnOk_Click(object sender, EventArgs e) {

            this.isCloseSettingsForm = true;
            if (m_appInit) {
                m_uiForm.m_cwWidget.animateShrink();
            }
            applyChanges();
            if (m_uiForm.m_cwNetwork.isConnected()) {
                DialogResult = DialogResult.OK;
                m_uiForm.m_cwWidget.enable();
            }

        }

        private void CWSettingsForm_Load(object sender, EventArgs e) {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            foreach (string s in Properties.Settings.Default.CommandServerIP) {
                cbIPs.Items.Add(s);
            }
            cbIPs.SelectedIndex = (m_uiForm.m_cwNetwork.m_hostIP == null) ? 0 : cbIPs.Items.IndexOf(m_uiForm.m_cwNetwork.m_hostIP);
            txtPort.Text = m_uiForm.m_cwNetwork.m_hostPort.ToString();
            if (cbIPs.Text.StartsWith("https")) {
                txtPassword.Text = dummyPassword;
            }
            else {
                txtPassword.Text = "";
            }

            txtOpacity.Text = m_uiForm.m_opacity.ToString();
            txtWallAspectRatio.Text = m_uiForm.m_cwWallPanel.m_aspectRatio.ToString();
            txtMinImageHeightScaling.Text = m_uiForm.m_cwWallPanel.m_minImageHeightScaling.ToString();
            chkShowSidebars.Checked = m_uiForm.m_cwWallPanel.m_showSidebars;
            m_uiForm.m_cwWidget.disable();
        }

        private void CWSettingsForm_Shown(object sender, EventArgs e) {
            SetForegroundWindow(this.Handle);
            Focus();
        }

        private void CWSettingsForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                DialogResult = DialogResult.OK;
                btnOk_Click(sender, e);
            }
        }

        private void CWSettingsForm_FormClosing(object sender, FormClosingEventArgs e) {
            // If windows ordered the close, don't hold it up.
            if (e.CloseReason == CloseReason.WindowsShutDown)
                return;
            // User closed the form through the 'x'.
            if (DialogResult == DialogResult.Cancel) {
                if (m_appInit) {
                    // No settings selected, close the application.
                    Application.Exit();
                }
                else {
                    // No changes made, recover the widget.
                    m_uiForm.m_cwWidget.enable();
                }
            }
        }

        private void cbIPs_SelectedIndexChanged(object sender, EventArgs e) {

            if (m_appInit)
                return;

            if (m_uiForm.m_cwNetwork.isConnected()) {
                if ((cbIPs.Text + ":" + txtPort.Text).Equals(m_uiForm.m_cwNetwork.m_hostIP + ":" + m_uiForm.m_cwNetwork.m_hostPort.ToString())) {
                    btnOk.Text = "Ok";
                    return;
                }
            } else {
                if ((cbIPs.Text + ":" + txtPort.Text).Equals(m_uiForm.m_cwNetwork.m_lastConnectedHost)) {
                    btnOk.Text = "Reconnect";
                    return;
                }
            }
            btnOk.Text = "Connect";

        }

        private void cbIPs_TextChanged(object sender, EventArgs e) {

            ComboBox comboBox = (ComboBox) sender;
            string text = comboBox.Text;
            int index = Array.IndexOf(Properties.Settings.Default.CommandServerIP, text);

            if (index > -1 && Properties.Settings.Default.CommandServerPort.Length > index) {
                txtPort.Text = Properties.Settings.Default.CommandServerPort[index];
            }
            else {
                txtPort.Text = "3000";
            }

            if (text.StartsWith("https")) {
                labelPassword.Enabled = true;
                txtPassword.Enabled = true;
                if (index > -1) {
                    txtPassword.Text = dummyPassword;
                }
                else {
                    txtPassword.Text = "";
                }
            }
            else {
                txtPassword.Text = "";
                txtPassword.Enabled = false;
                labelPassword.Enabled = false;
            }

        }

        private void chkShowSidebars_CheckedChanged(object sender, EventArgs e) {

            CheckBox checkBox = (CheckBox) sender;
            Point location = labelWallAspectRatio.Location;
            Size size = labelWallAspectRatio.Size;
            if (checkBox.Checked) {
                minWallAspectRatio = 2.01f * (m_uiForm.SCREEN_HEIGHT * 2 + m_uiForm.SCREEN_WIDTH) / m_uiForm.SCREEN_HEIGHT - 3;
            }
            else {
                minWallAspectRatio = 2.01f * m_uiForm.SCREEN_WIDTH / m_uiForm.SCREEN_HEIGHT;
            }
            minWallAspectRatio = (float) Math.Round(minWallAspectRatio * 1000) / 1000;
            labelWallAspectRatio.Text = "Aspect Ratio (> " + minWallAspectRatio + ")";
            Size newSize = labelWallAspectRatio.Size;
            labelWallAspectRatio.Location = new Point(location.X + size.Width - newSize.Width, location.Y);

        }

        private void txtPassword_TextChanged(object sender, EventArgs e) {

            TextBox textBox = (TextBox) sender;
            if (!textBox.Modified || textBox.Text == textBox.SelectedText) {
                return;
            }
            passwordChanged = true;

        }

        public void setOkButtonText(string text) {

            btnOk.Text = text;

        }

        public delegate void showElementsWhileConnectingCallback();
        public delegate void hideElementsWhileConnectingCallback();

        public void invokeShowElementsWhileConnecting() {

            this.Invoke(new showElementsWhileConnectingCallback(this._showElementsWhileConnecting));

        }

        public void invokeHideElementsWhileConnecting() {

            this.Invoke(new hideElementsWhileConnectingCallback(this._hideElementsWhileConnecting));

        }

        public void showElementsWhileConnecting() {

            if (this.InvokeRequired) {
                invokeShowElementsWhileConnecting();
            } else
                _showElementsWhileConnecting();

        }

        public void hideElementsWhileConnecting() {

            if (this.InvokeRequired) {
                invokeHideElementsWhileConnecting();
            } else
                _hideElementsWhileConnecting();

        }

        public void _showElementsWhileConnecting() {

            btnApply.Hide();
            btnOk.Hide();
            labelConnecting.Show();
            progressBarConnecting.Show();
            progressBarConnecting.MarqueeAnimationSpeed = 50;

        }

        public void _hideElementsWhileConnecting() {

            labelConnecting.Hide();
            progressBarConnecting.MarqueeAnimationSpeed = 0;
            progressBarConnecting.Hide();
            if (!this.m_appInit)
                btnApply.Show();
            btnOk.Show();

        }

    }
}
