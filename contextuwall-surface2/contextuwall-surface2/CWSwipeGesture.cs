﻿using System;

namespace ContextuWall {
    public class CWSwipeGesture : CWGesture, IGesture {
        public float m_minSwipeDistance = 100.0f;
        public int m_numberOfFingers = 0;
        private Object m_theLock = new Object();

        public class SwipeGestureData : GestureData, IGestureData {
            public InteractionOutput m_io;
            public float m_distance;
            //public float m_angle;

            public SwipeGestureData(int fingerId, InteractionOutput io) : base(CWGesture.GestureTypes.SWIPE, null) {
                m_io = io;
            }
        }
        InteractionOutput m_io = null;

        public void resetGesture() {
            m_io = null;
            m_numberOfFingers = 0;
            m_state = GestureStates.POSSIBLE;
        }
        public bool isGestureRecognized() {
            bool recognized = false;
            if ((m_io != null) && (m_state == GestureStates.POSSIBLE)) {
                //Console.WriteLine("({0},{1})", m_io.Data.Manipulation.Cumulative.TranslationX, m_io.Data.Manipulation.Cumulative.TranslationY);
                float d = CWCommon.getDistance(m_io.Data.Manipulation.Cumulative.TranslationX, m_io.Data.Manipulation.Cumulative.TranslationY);
                if (d > m_minSwipeDistance) {
                    SwipeGestureData data = ((SwipeGestureData) this.m_gestureData);
                    data.m_io = m_io;
                    data.m_distance = d;
                    recognized = true;
                }
            }
            return recognized;
        }

        public CWSwipeGesture(CWGesture.GestureHandler action) : base(action) {
            this.m_gestureData = new SwipeGestureData(0, null);
            base.m_isGestureRecognized = isGestureRecognized;
            base.m_resetGesture = resetGesture;
        }

        public void FingerDown(InteractionOutput io) {
            lock (m_theLock) {
                m_numberOfFingers++;
                //Console.WriteLine("CWSwipeGesture::FingerDown: m_numberOfFingers ={0} m_numberOfFingersRequired = {1}", m_numberOfFingers, m_numberOfFingersRequired);
                if (m_state == GestureStates.CANCELLED)
                    return;

                if (m_numberOfFingers > m_numberOfFingersRequired) {
                    m_state = GestureStates.CANCELLED;
                }
            }
        }

        public void FingerMove(InteractionOutput io) {
            //Console.WriteLine("CWSwipeGesture::FingerMove");
        }

        public void FingerUp(InteractionOutput io) {
            lock (m_theLock) {
                // A bandaid bugfix: Capping lowerbound at zero, since the display occasionally won't register finger-down events..
                m_numberOfFingers = Math.Max(m_numberOfFingers - 1, 0);
                //Console.WriteLine("CWSwipeGesture::FingerUp: m_numberOfFingers ={0} m_numberOfFingersRequired = {1}", m_numberOfFingers, m_numberOfFingersRequired);

                if ((m_numberOfFingers < 1) && m_state == GestureStates.CANCELLED) {
                    resetGesture();
                }
                else {
                    if (m_state == GestureStates.CANCELLED)
                        return;

                    //m_touches.RemoveAll(x => x.id == fe.id);
                    if (m_numberOfFingers == m_numberOfFingersRequired - 1) {
                        m_io = io;
                        base.resetWaitTimerToCheckGesture(false);
                    }
                }
            }
        }
    }
}
