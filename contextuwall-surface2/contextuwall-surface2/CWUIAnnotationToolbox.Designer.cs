﻿namespace ContextuWall {
    partial class CWUIAnnotationToolbox {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CWUIAnnotationToolbox));
            this.tstControls = new ActiveToolStrip();
            this.btnOK = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ddnPens = new System.Windows.Forms.ToolStripDropDownButton();
            this.pencilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddnColours = new System.Windows.Forms.ToolStripDropDownButton();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yellowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.violetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddnDeletes = new System.Windows.Forms.ToolStripDropDownButton();
            this.myLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allLayersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tstControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // tstControls
            // 
            this.tstControls.Dock = System.Windows.Forms.DockStyle.None;
            this.tstControls.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tstControls.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tstControls.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOK,
            this.btnUndo,
            this.toolStripSeparator1,
            this.ddnPens,
            this.ddnColours,
            this.ddnDeletes});
            this.tstControls.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tstControls.Location = new System.Drawing.Point(6, 6);
            this.tstControls.MaximumSize = new System.Drawing.Size(46, 230);
            this.tstControls.Name = "tstControls";
            this.tstControls.Padding = new System.Windows.Forms.Padding(0);
            this.tstControls.Size = new System.Drawing.Size(45, 225);
            this.tstControls.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.AutoSize = false;
            this.btnOK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
            this.btnOK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(44, 36);
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.AutoSize = false;
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(44, 36);
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(44, 6);
            // 
            // ddnPens
            // 
            this.ddnPens.AutoSize = false;
            this.ddnPens.AutoToolTip = false;
            this.ddnPens.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ddnPens.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pencilToolStripMenuItem,
            this.markerToolStripMenuItem});
            this.ddnPens.Image = ((System.Drawing.Image)(resources.GetObject("ddnPens.Image")));
            this.ddnPens.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddnPens.Name = "ddnPens";
            this.ddnPens.Size = new System.Drawing.Size(45, 36);
            this.ddnPens.Text = "toolStripDropDownButton2";
            // 
            // pencilToolStripMenuItem
            // 
            this.pencilToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pencilToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pencilToolStripMenuItem.Image")));
            this.pencilToolStripMenuItem.Name = "pencilToolStripMenuItem";
            this.pencilToolStripMenuItem.Size = new System.Drawing.Size(183, 40);
            this.pencilToolStripMenuItem.Text = "Pencil";
            this.pencilToolStripMenuItem.Click += new System.EventHandler(this.ddnPen_Click);
            // 
            // markerToolStripMenuItem
            // 
            this.markerToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("markerToolStripMenuItem.Image")));
            this.markerToolStripMenuItem.Name = "markerToolStripMenuItem";
            this.markerToolStripMenuItem.Size = new System.Drawing.Size(183, 40);
            this.markerToolStripMenuItem.Text = "Marker";
            this.markerToolStripMenuItem.Click += new System.EventHandler(this.ddnPen_Click);
            // 
            // ddnColours
            // 
            this.ddnColours.AutoSize = false;
            this.ddnColours.AutoToolTip = false;
            this.ddnColours.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ddnColours.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redToolStripMenuItem,
            this.orangeToolStripMenuItem,
            this.yellowToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.blueToolStripMenuItem,
            this.indigoToolStripMenuItem,
            this.violetToolStripMenuItem});
            this.ddnColours.Image = ((System.Drawing.Image)(resources.GetObject("ddnColours.Image")));
            this.ddnColours.Name = "ddnColours";
            this.ddnColours.Size = new System.Drawing.Size(45, 36);
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.redToolStripMenuItem.Text = "Red";
            this.redToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // orangeToolStripMenuItem
            // 
            this.orangeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orangeToolStripMenuItem.Name = "orangeToolStripMenuItem";
            this.orangeToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.orangeToolStripMenuItem.Text = "Orange";
            this.orangeToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // yellowToolStripMenuItem
            // 
            this.yellowToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yellowToolStripMenuItem.Name = "yellowToolStripMenuItem";
            this.yellowToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.yellowToolStripMenuItem.Text = "Yellow";
            this.yellowToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.greenToolStripMenuItem.Text = "Green";
            this.greenToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.blueToolStripMenuItem.Text = "Blue";
            this.blueToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // indigoToolStripMenuItem
            // 
            this.indigoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indigoToolStripMenuItem.Name = "indigoToolStripMenuItem";
            this.indigoToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.indigoToolStripMenuItem.Text = "Indigo";
            this.indigoToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // violetToolStripMenuItem
            // 
            this.violetToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.violetToolStripMenuItem.Name = "violetToolStripMenuItem";
            this.violetToolStripMenuItem.Size = new System.Drawing.Size(171, 40);
            this.violetToolStripMenuItem.Text = "Violet";
            this.violetToolStripMenuItem.Click += new System.EventHandler(this.ddnColour_Click);
            // 
            // ddnDeletes
            // 
            this.ddnDeletes.AutoSize = false;
            this.ddnDeletes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ddnDeletes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myLayerToolStripMenuItem,
            this.allLayersToolStripMenuItem});
            this.ddnDeletes.Image = ((System.Drawing.Image)(resources.GetObject("ddnDeletes.Image")));
            this.ddnDeletes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddnDeletes.Name = "ddnDeletes";
            this.ddnDeletes.Size = new System.Drawing.Size(45, 36);
            // 
            // myLayerToolStripMenuItem
            // 
            this.myLayerToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myLayerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("myLayerToolStripMenuItem.Image")));
            this.myLayerToolStripMenuItem.Name = "myLayerToolStripMenuItem";
            this.myLayerToolStripMenuItem.Size = new System.Drawing.Size(277, 40);
            this.myLayerToolStripMenuItem.Text = "My Annotations";
            this.myLayerToolStripMenuItem.Click += new System.EventHandler(this.ddnDelete_Click);
            // 
            // allLayersToolStripMenuItem
            // 
            this.allLayersToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allLayersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("allLayersToolStripMenuItem.Image")));
            this.allLayersToolStripMenuItem.Name = "allLayersToolStripMenuItem";
            this.allLayersToolStripMenuItem.Size = new System.Drawing.Size(277, 40);
            this.allLayersToolStripMenuItem.Text = "All Annotations";
            this.allLayersToolStripMenuItem.Click += new System.EventHandler(this.ddnDelete_Click);
            // 
            // CWUIAnnotationToolbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(110F, 110F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(37)))));
            this.ClientSize = new System.Drawing.Size(57, 286);
            this.Controls.Add(this.tstControls);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(1);
            this.MaximumSize = new System.Drawing.Size(57, 286);
            this.Name = "CWUIAnnotationToolbox";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.ShowInTaskbar = false;
            this.Text = "\"\"";
            this.tstControls.ResumeLayout(false);
            this.tstControls.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton btnOK;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton ddnPens;
        private System.Windows.Forms.ToolStripMenuItem pencilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markerToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ddnColours;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yellowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem violetToolStripMenuItem;
        private ActiveToolStrip tstControls;
        private System.Windows.Forms.ToolStripDropDownButton ddnDeletes;
        private System.Windows.Forms.ToolStripMenuItem myLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allLayersToolStripMenuItem;


    }
}