﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ContextuWall {
    public partial class CWUIAnnotationToolbox : Form {

        public enum PenChoice {
            Pencil,
            Marker,
        }

        public CWUIForm m_uiForm;

        public Pen m_pen = new Pen(Color.Red); // Note the colour used here is arbitrary and has no effect on what is really used.

        public CWUIAnnotationToolbox(CWUIForm uiForm) {
            InitializeComponent();
            this.m_uiForm = uiForm;
            ddnColours.Image = new Bitmap(72, 72);
            // Restore marker preferences from last use.
            updateSelectedPen();
        }

        public void updateSelectedPen(PenChoice penChoice, Color colour) {
            m_pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            m_pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            switch (penChoice) {
                case PenChoice.Pencil:
                    ddnPens.Image = pencilToolStripMenuItem.Image;
                    m_pen.Width = 5;
                    m_pen.Color = Color.FromArgb(255, colour.R, colour.G, colour.B);
                    break;
                case PenChoice.Marker:
                    ddnPens.Image = markerToolStripMenuItem.Image;
                    m_pen.Width = 25;
                    m_pen.Color = Color.FromArgb(100, colour.R, colour.G, colour.B);
                    break;
                default:
                    break;
            }
            Image cThumb = ddnColours.Image;
            Color c = Color.FromArgb(255, colour.R, colour.G, colour.B);
            using (Graphics g = Graphics.FromImage(cThumb)) {
                g.FillRectangle(new SolidBrush(c), 0 + 12, 0 + 12, cThumb.Width - 24, cThumb.Height - 24);
            }
            m_uiForm.m_cwAnnotationMode.m_penChoice = penChoice;
            m_uiForm.m_cwAnnotationMode.m_penColour = colour;
        }
        public void updateSelectedPen(PenChoice penChoice) { updateSelectedPen(penChoice, m_uiForm.m_cwAnnotationMode.m_penColour); }
        public void updateSelectedPen(Color colour) { updateSelectedPen(m_uiForm.m_cwAnnotationMode.m_penChoice, colour); }
        public void updateSelectedPen() { updateSelectedPen(m_uiForm.m_cwAnnotationMode.m_penChoice); }

        private void btnOK_Click(object sender, EventArgs e) {
            m_uiForm.m_cwAnnotationMode.end();
        }

        private void btnUndo_Click(object sender, EventArgs e) {
            m_uiForm.m_cwAnnotationMode.undo();
        }

        private void ddnPen_Click(object sender, EventArgs e) {
            switch (((ToolStripMenuItem) sender).Text) {
                case "Pencil":
                    updateSelectedPen(PenChoice.Pencil);
                    break;
                case "Marker":
                    updateSelectedPen(PenChoice.Marker);
                    break;
                default:
                    break;
            }
        }

        private void ddnColour_Click(object sender, EventArgs e) {
            Color colour = Color.FromName(((ToolStripMenuItem) sender).Text);
            updateSelectedPen(colour);
            this.Refresh();
        }

        private void ddnDelete_Click(object sender, EventArgs e) {
            switch (((ToolStripMenuItem) sender).Text) {
                case "My Annotations":
                    m_uiForm.m_cwAnnotationMode.clearAnnotation();
                    m_uiForm.Invalidate();
                    break;
                case "All Annotations":
                    m_uiForm.m_cwAnnotationMode.clearAllAnnotations();
                    m_uiForm.Invalidate();
                    break;
                default:
                    break;
            }
        }

        public delegate void formCallback();
        public void invokeClose() {
            this.Invoke(new formCallback(this.Close));
        }
    }

    // This version of ToolStrip enables mouse clicks even when the main form is NOT active.
    // Source: http://stackoverflow.com/questions/3427696/windows-requires-a-click-to-activate-a-window-before-a-second-click-will-select
    public class ActiveToolStrip : ToolStrip {
        const uint WM_LBUTTONDOWN = 0x201;
        const uint WM_LBUTTONUP = 0x202;

        static private bool down = false;

        protected override void WndProc(ref Message m) {
            if (m.Msg == WM_LBUTTONUP && !down) {
                m.Msg = (int) WM_LBUTTONDOWN;
                base.WndProc(ref m);
                m.Msg = (int) WM_LBUTTONUP;
            }

            if (m.Msg == WM_LBUTTONDOWN)
                down = true;
            if (m.Msg == WM_LBUTTONUP)
                down = false;

            base.WndProc(ref m);
        }
    }
}
