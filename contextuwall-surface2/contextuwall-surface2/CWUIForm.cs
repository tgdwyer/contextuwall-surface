﻿using log4net;
using Microsoft.Win32;
using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

namespace ContextuWall {
    public partial class CWUIForm : Form {
        //http://social.msdn.microsoft.com/Forums/windows/en-US/49205592-47d8-48b7-ad29-be53aa8be7c0/c-how-to-make-a-form-clickthruable-hard-to-explain-in-one-line?forum=winforms
        [DllImport("user32.dll", SetLastError = true)]
        private static extern UInt32 GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        public const int GWL_EXSTYLE = -20;
        public const int GWL_STYLE = -16;
        public const UInt32 WS_CAPTION = 0x00C00000;
        const UInt32 WS_DLGFRAME = 0x400000;
        const UInt32 WS_MAXIMIZE = 0x1000000;
        const UInt32 WS_OVERLAPPED = 0;

        public const UInt32 WS_EX_LAYERED = 0x80000;
        public const UInt32 WS_EX_TRANSPARENT = 0x20;
        const UInt32 WS_EX_WINDOWEDGE = 0x0100;
        const UInt32 WS_EX_CLIENTEDGE = 0x0200;
        const UInt32 WS_BORDER = 0x800000;
        const UInt32 WS_EX_OVERLAPPEDWINDOW = (WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE);
        private const int WS_SYSMENU = 0x80000;
        public const int LWA_ALPHA = 0x2;
        public const int LWA_COLORKEY = 0x1;
        const UInt32 CS_NOCLOSE = 0x200;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern System.IntPtr FindWindowByCaption(int ZeroOnly, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetWindow(IntPtr hWnd, GetWindow_Cmd uCmd);

        enum GetWindow_Cmd : uint {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int PostMessage(IntPtr hwnd, int wMsg, int wParam, [MarshalAs(UnmanagedType.LPStr)] ref string lParam);

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public const int WM_SYSCOMMAND = 0x0112;
        public const int WM_APP = 0x8000;

        [DllImport("user32.dll")]
        static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);
        [DllImport("user32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32.dll")]
        static extern bool EnableMenuItem(IntPtr hMenu, uint uIDEnableItem, uint uEnable);
        internal const UInt32 SC_CLOSE = 0xF060;
        internal const UInt32 MF_ENABLED = 0x00000000;
        internal const UInt32 MF_GRAYED = 0x00000001;
        internal const UInt32 MF_DISABLED = 0x00000002;
        internal const uint MF_BYCOMMAND = 0x00000000;

        [DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
        public static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern int GetDeviceCaps(IntPtr hDC, int nIndex);
        public enum DeviceCap {
            /// <summary>
            /// Device driver version
            /// </summary>
            DRIVERVERSION = 0,
            /// <summary>
            /// Device classification
            /// </summary>
            TECHNOLOGY = 2,
            /// <summary>
            /// Horizontal size in millimeters
            /// </summary>
            HORZSIZE = 4,
            /// <summary>
            /// Vertical size in millimeters
            /// </summary>
            VERTSIZE = 6,
            /// <summary>
            /// Horizontal width in pixels
            /// </summary>
            HORZRES = 8,
            /// <summary>
            /// Vertical height in pixels
            /// </summary>
            VERTRES = 10,
            /// <summary>
            /// Number of bits per pixel
            /// </summary>
            BITSPIXEL = 12,
            /// <summary>
            /// Number of planes
            /// </summary>
            PLANES = 14,
            /// <summary>
            /// Number of brushes the device has
            /// </summary>
            NUMBRUSHES = 16,
            /// <summary>
            /// Number of pens the device has
            /// </summary>
            NUMPENS = 18,
            /// <summary>
            /// Number of markers the device has
            /// </summary>
            NUMMARKERS = 20,
            /// <summary>
            /// Number of fonts the device has
            /// </summary>
            NUMFONTS = 22,
            /// <summary>
            /// Number of colors the device supports
            /// </summary>
            NUMCOLORS = 24,
            /// <summary>
            /// Size required for device descriptor
            /// </summary>
            PDEVICESIZE = 26,
            /// <summary>
            /// Curve capabilities
            /// </summary>
            CURVECAPS = 28,
            /// <summary>
            /// Line capabilities
            /// </summary>
            LINECAPS = 30,
            /// <summary>
            /// Polygonal capabilities
            /// </summary>
            POLYGONALCAPS = 32,
            /// <summary>
            /// Text capabilities
            /// </summary>
            TEXTCAPS = 34,
            /// <summary>
            /// Clipping capabilities
            /// </summary>
            CLIPCAPS = 36,
            /// <summary>
            /// Bitblt capabilities
            /// </summary>
            RASTERCAPS = 38,
            /// <summary>
            /// Length of the X leg
            /// </summary>
            ASPECTX = 40,
            /// <summary>
            /// Length of the Y leg
            /// </summary>
            ASPECTY = 42,
            /// <summary>
            /// Length of the hypotenuse
            /// </summary>
            ASPECTXY = 44,
            /// <summary>
            /// Shading and Blending caps
            /// </summary>
            SHADEBLENDCAPS = 45,

            /// <summary>
            /// Logical pixels inch in X
            /// </summary>
            LOGPIXELSX = 88,
            /// <summary>
            /// Logical pixels inch in Y
            /// </summary>
            LOGPIXELSY = 90,

            /// <summary>
            /// Number of entries in physical palette
            /// </summary>
            SIZEPALETTE = 104,
            /// <summary>
            /// Number of reserved entries in palette
            /// </summary>
            NUMRESERVED = 106,
            /// <summary>
            /// Actual color resolution
            /// </summary>
            COLORRES = 108,

            // Printing related DeviceCaps. These replace the appropriate Escapes
            /// <summary>
            /// Physical Width in device units
            /// </summary>
            PHYSICALWIDTH = 110,
            /// <summary>
            /// Physical Height in device units
            /// </summary>
            PHYSICALHEIGHT = 111,
            /// <summary>
            /// Physical Printable Area x margin
            /// </summary>
            PHYSICALOFFSETX = 112,
            /// <summary>
            /// Physical Printable Area y margin
            /// </summary>
            PHYSICALOFFSETY = 113,
            /// <summary>
            /// Scaling factor x
            /// </summary>
            SCALINGFACTORX = 114,
            /// <summary>
            /// Scaling factor y
            /// </summary>
            SCALINGFACTORY = 115,

            /// <summary>
            /// Current vertical refresh rate of the display device (for displays only) in Hz
            /// </summary>
            VREFRESH = 116,
            /// <summary>
            /// Vertical height of entire desktop in pixels
            /// </summary>
            DESKTOPVERTRES = 117,
            /// <summary>
            /// Horizontal width of entire desktop in pixels
            /// </summary>
            DESKTOPHORZRES = 118,
            /// <summary>
            /// Preferred blt alignment
            /// </summary>
            BLTALIGNMENT = 119
        }

        const int SC_MOVE = 0xF010;

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern uint RegisterWindowMessage(string lpString);

        public int SCREEN_WIDTH;
        public int SCREEN_HEIGHT;

        public byte m_opacity { get; set; }
        //public float m_thumbnailScale { get; set; }

        public CWDisplayList m_cwDisplayList;

        public CWUIWidget m_cwWidget;
        public CWSettingsForm m_cwSettings;
        public CWAnnotationMode m_cwAnnotationMode;
        public CWUIAnnotationToolbox m_cwAnnotationToolbox;
        public CWScreen m_cwScreen;

        public CWNetwork m_cwNetwork;
        public CWSecurity m_cwSecurity;

        public CWGestureManager m_cwGestureManager;
        public CWWall m_cwWall;

        public CWWallPanel m_cwWallPanel;
        public CWPointer m_cwPointer;

        private CWSwipeGesture m_swipeGesture;
        public CWUIForm() {
            m_opacity = Properties.Settings.Default.Opacity;

            InitializeComponent();
            m_cwDisplayList = new CWDisplayList(this);
            InitializeNotifyIcon();

            Win32.EnableMouseInPointer(true);

            //SetStyle
            this.MaximizedBounds = Screen.PrimaryScreen.WorkingArea; //such that it doesn't cover taskbar.
            this.WindowState = FormWindowState.Maximized;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.DoubleBuffered = true;

            SetWindowLong(this.Handle, GWL_EXSTYLE,
            (IntPtr) (GetWindowLong(this.Handle, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_OVERLAPPEDWINDOW));

            //http://stackoverflow.com/questions/743906/how-to-hide-close-button-in-wpf-window
            SetWindowLong(this.Handle, GWL_STYLE, (IntPtr) (GetWindowLong(this.Handle, GWL_STYLE) & ~WS_SYSMENU));

            // set opacity to 100 (0-256)           
            SetLayeredWindowAttributes(this.Handle, 100, this.m_opacity, LWA_COLORKEY | LWA_ALPHA);

            Load += new System.EventHandler(this.OnLoadHandler);
            FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnCloseHandler);

            Paint += new PaintEventHandler(CWUIForm_Paint);
            SystemEvents.DisplaySettingsChanged += new System.EventHandler(this.OnDisplaySettingsChangedHandler);

            m_cwScreen = new CWScreen();

            m_cwNetwork = new CWNetwork(this);
            m_cwSecurity = new CWSecurity(this);

            m_cwGestureManager = new CWGestureManager(this);
            m_cwWall = new CWWall(this);

            m_cwAnnotationMode = new CWAnnotationMode(this);
            m_cwAnnotationToolbox = new CWUIAnnotationToolbox(this);
            m_cwAnnotationToolbox.Hide();

            m_cwWallPanel = new CWWallPanel(this);
            m_cwPointer = new CWPointer(this);

            m_swipeGesture = new CWSwipeGesture(this.handleSwipeGesture);
            m_swipeGesture.m_numberOfFingersRequired = 1;
            m_swipeGesture.m_minSwipeDistance = 150.0f;

            m_cwGestureManager.addManipulationGesture(m_swipeGesture);
        }


        public delegate void updateOpacityCallback(byte val);
        private void updateOpacity(byte val) {
            SetLayeredWindowAttributes(this.Handle, 100, val, LWA_COLORKEY | LWA_ALPHA);
        }
        public void invokeUpdateOpacity(byte val) {
            this.m_opacity = val;
            this.Invoke(new updateOpacityCallback(this.updateOpacity), new object[] { val });
        }

        public delegate void formCallback();
        public void invokeHide(bool hideWidget = true) {
            this.Invoke(new formCallback(this.Hide));
            if (hideWidget) {
                this.Invoke(new formCallback(this.m_cwWidget.Hide));
            }
            if (this.m_cwAnnotationMode.active) {
                this.Invoke(new formCallback(this.m_cwAnnotationToolbox.Hide));
            }
        }
        public void invokeShow() {
            this.Invoke(new formCallback(this.Show));
            this.Invoke(new formCallback(this.m_cwWidget.Show));
            if (this.m_cwAnnotationMode.active) {
                this.Invoke(new formCallback(this.m_cwAnnotationToolbox.Show));
            }
        }

        private void handleSwipeGesture(object source, CWGesture.IGestureData data) {
            CWSwipeGesture.SwipeGestureData gestureData = (CWSwipeGesture.SwipeGestureData) data;
            try { m_cwNetwork.assertConnected(); } catch { return; }
            String filename = null;
            //TODO: Pass inertia and release point here instead of two points
            PointF downTouch = new PointF(gestureData.m_io.Data.X - gestureData.m_io.Data.Manipulation.Cumulative.TranslationX, gestureData.m_io.Data.Y - gestureData.m_io.Data.Manipulation.Cumulative.TranslationY);
            PointF upTouch = new PointF(gestureData.m_io.Data.X, gestureData.m_io.Data.Y);
            PointF location = m_cwWallPanel.getSurfaceLocation(downTouch, upTouch);

            if (!location.IsEmpty) {
                if (m_selection_rect.IsEmpty) {
                    sendScreenshot(location);
                }
                else {
                    if (m_selection_rect.Width > 0 && m_selection_rect.Height > 0) {
                        invokeHide();
                        filename = m_cwScreen.getScreenshot(
                             m_selection_rect.X,
                             m_selection_rect.Y,
                             m_selection_rect.Width,
                             m_selection_rect.Height);
                        invokeShow();
                        m_selection_rect = Rectangle.Empty;
                        addFileToWall(filename, location);
                    }
                }
            }
        }

        public delegate void initUICallback(bool appInit = false);
        public void initUI(bool appInit = false) {
            this.MaximizedBounds = Screen.PrimaryScreen.WorkingArea;
            SCREEN_WIDTH = Screen.PrimaryScreen.WorkingArea.Width - 4; // to account for padding
            SCREEN_HEIGHT = Screen.PrimaryScreen.WorkingArea.Height - 4; // to account for padding
            m_cwWallPanel.computeRegion();

            //if (m_cwDisplayList.defaultDisplay.toolStripItem.Checked) {
            //    m_cwDisplayList.curChecked.update(SCREEN_WIDTH * 8, SCREEN_HEIGHT * 8);
            //}

            //m_thumbnailScale = m_cwWallPanel.m_height / (float)m_cwDisplayList.curChecked.height;

            if (!appInit) {
                m_cwWidget.setDefaultPosition();
            }

            // in case the user switches the side bars on or off the area available for annotations might change
            // resize and repaint the canvas for the annotation area, resize the image to be annotated and the annotation(s)
            if (!appInit && m_cwAnnotationMode.active) {
                m_cwAnnotationMode.updateCanvas();
            }
            // reposition the annotation toolbox
            if (!appInit && m_cwAnnotationToolbox.Visible) {
                m_cwAnnotationToolbox.Location = m_cwWallPanel.m_annotationToolboxDock;
            }

            m_cwWall.repositionImages();
            this.Invalidate();
        }
        public void invokeInitUI(bool appInit = false) {
            this.Invoke(new initUICallback(this.initUI), new object[] { appInit });
        }

        private void OnLoadHandler(Object sender, EventArgs e) {
            m_cwWidget = new CWUIWidget(this);
            m_cwWidget.Show();
            initUI(true);
            Visible = false;
            this.updateSettings(true);
        }

        private void OnCloseHandler(Object sender, FormClosedEventArgs e) {
            m_cwGestureManager.invalidateGestures();
            //Hide icon in system tray - windows doesn't clean it up properly..
            //Note that this does not work if the program is terminated in a BAD way.
            m_notify.Visible = false;
            //Clean all the images out of the working directory.
            //foreach(string file in System.IO.Directory.GetFiles(@".\", "*.png")) {
            //    try {
            //        System.IO.File.Delete(file);
            //    } catch(IOException) { } //File in use! Leave it be.
            //}
        }

        //Handles screen rotation during program runtime (ie. for tablets)
        private void OnDisplaySettingsChangedHandler(object sender, EventArgs e) {
            invokeInitUI(); //Redraw the wall panel.
        }

        Rectangle m_selection_rect;
        private void CWUIForm_MouseDown(object sender, MouseEventArgs e) {
            // "e.X" and "e.Y" are used to get MousePositionX and MousePositionY
            if (e.Button == MouseButtons.Right) {
                m_selection_rect = new Rectangle(e.X, e.Y, 0, 0);

                this.Invalidate(m_selection_rect);
            }
            //System.Drawing.Graphics graphics = this.CreateGraphics();
            //System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(100, 100, 700, 700);
            //graphics.DrawEllipse(System.Drawing.Pens.Black, rectangle);
            //graphics.DrawRectangle(System.Drawing.Pens.Red, rectangle);
        }

        private void CWUIForm_MouseMove(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                m_selection_rect = new Rectangle(m_selection_rect.Left, m_selection_rect.Top, e.X - m_selection_rect.Left, e.Y - m_selection_rect.Top);
            }
            //this.Invalidate(m_selection_rect);
        }

        private void CWUIForm_MouseUp(object sender, MouseEventArgs e) {
            //this.Invalidate(m_selection_rect);
        }

        private void CWUIForm_Paint(object sender, PaintEventArgs e) {
            //e.Graphics.Clear(m_uiForm.BackColor);
            //if(!m_selection_rect.IsEmpty) {
            //    using(Pen pen = new Pen(Color.Red, 2)) {
            //        e.Graphics.DrawRectangle(pen, m_selection_rect);
            //    }
            //}
            //Console.WriteLine("CWUIForm rect:{0}", e.ClipRectangle.ToString());
            m_cwWallPanel.paint(e);
            m_cwWall.drawImages(e);
        }

        [SecurityPermission(SecurityAction.LinkDemand)]
        protected override void WndProc(ref Message m) {
            bool handled = false;
            switch (m.Msg) {
                case WM_SYSCOMMAND:
                    if ((m.WParam.ToInt32() & 0xfff0) == SC_MOVE)
                        return;
                    break;
                case Win32.WM_POINTERDOWN:
                case Win32.WM_POINTERUP:
                case Win32.WM_POINTERUPDATE:
                case Win32.WM_POINTERCAPTURECHANGED:
                    handled = m_cwPointer.DecodePointer(ref m);
                    //Console.WriteLine("CWUIForm WM_POINTERDOWN Msg: {0:X}", m.Msg);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
            if (handled)
                m.Result = new System.IntPtr(1); // Acknowledge event if handled.
        }

        public NotifyIcon m_notify;
        private void InitializeNotifyIcon() {
            m_notify = new NotifyIcon();
            m_notify.Text = "ContexuWall";
            m_notify.Icon = Properties.Resources.ContextuWallIcon;
            m_notify.Visible = true;
            m_notify.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            // set font size of the context menu
            int m_fontSize = (int) Math.Max(m_notify.ContextMenuStrip.Font.Size, 18);
            m_notify.ContextMenuStrip.Font = new Font(m_notify.ContextMenuStrip.Font.Name, m_fontSize, FontStyle.Regular);
            m_notify.ContextMenuStrip.Items.Add("Send Screenshot", null, (s, e) => this.sendScreenshot(m_cwWallPanel.getDefaultSurfaceLocation()));
            //m_notify.ContextMenuStrip.Items.Add("Send Application Screenshot", null, (s, e) => this.sendApplicationScreenshot("calc"));
            //new System.Windows.Forms.MenuItem("Send Clip Area", (s, e) => this.sendClipScreenshot()),
            m_notify.ContextMenuStrip.Items.Add("Send File", null, (s, e) => this.sendFile());
            //m_notify.ContextMenuStrip.Items.Add("VLC Thingy", null, (s, e) => this.runVLCThingy());
            m_notify.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            m_notify.ContextMenuStrip.Items.Add("Clear Wall", null, (s, e) => this.clearWall(true));
            m_notify.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            m_cwDisplayList.displaysSubmenu = (ToolStripMenuItem) m_notify.ContextMenuStrip.Items.Add("Primary Display");
            m_cwDisplayList.init();
            m_notify.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            m_notify.ContextMenuStrip.Items.Add("Refresh", null, (s, e) => this.refresh());
            m_notify.ContextMenuStrip.Items.Add("Reconnect", null, (s, e) => this.reconnect());
            m_notify.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            m_notify.ContextMenuStrip.Items.Add("Settings", null, (s, e) => this.updateSettings(false));
            m_notify.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            m_notify.ContextMenuStrip.Items.Add("Exit", null, (s, e) => this.exitContextuWall());
            m_notify.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NotifyClicked);

            changeActivationContextMenuStripItems(false);

        }
        //Enables left-clicking on notify icon to open context menu (convenient for a touchscreen environment).
        private void NotifyClicked(object sender, MouseEventArgs e) {
            // Need to use reflection here, as MouseEventArgs don't provide the mouse location relative to the form.
            MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
            mi.Invoke(m_notify, null);
        }

        public delegate void showContextMenuStripCallback(Point where);
        private void showContextMenuStrip(Point where) {
            m_notify.ContextMenuStrip.Show(this, where);
        }
        public void invokeShowContextMenuStrip(Point where) {
            BeginInvoke(new showContextMenuStripCallback(this.showContextMenuStrip), new object[] { where });
        }

        public void runVLCThingy() {
            try { m_cwNetwork.assertConnected(); } catch { return; }
            // TODO: some input dialog box here (no native c# ones! :O)
            CWCommon.runOSCommand("calc");
        }

        private void AddControlToPanel(Panel panel, Control ctrl) {
            if (panel.InvokeRequired) {
                panel.Invoke(new Action<Panel, Control>(AddControlToPanel), panel, ctrl);
                return;
            }
            else {
                panel.Controls.Add(ctrl);
            }
        }

        //public void sendApplicationScreenshot(string name, PointF location) {
        //    Process[] p = Process.GetProcessesByName(name);
        //    if(p.Count() > 0) {
        //        SetForegroundWindow(p[0].MainWindowHandle);
        //    }
        //    this.sendScreenshot(m_cwWallPanel.getDefaultSurfaceLocation());
        //}
        //public void sendApplicationScreenshot(string name) { sendApplicationScreenshot(name, m_cwWallPanel.getDefaultSurfaceLocation()); }

        public void sendScreenshot(PointF location) {
            try { m_cwNetwork.assertConnected(); } catch { return; }
            invokeHide();
            int width = Screen.PrimaryScreen.WorkingArea.Width;
            int height = Screen.PrimaryScreen.WorkingArea.Height;
            // adapt width and height to a custimised scaling of the display set by the user in the Windows settings
            // because the graphics.CopyFromScreen method works on the screen buffer and therefore needs a different width and height
            // than the logical width and height from Screen.PrimaryScreen.WorkingArea
            // https://stackoverflow.com/questions/5977445/how-to-get-windows-display-settings
            //Graphics graphics = Graphics.FromHwnd(this.Handle);
            using (Graphics graphics = Graphics.FromHwnd(this.Handle)) {
                    IntPtr desktop = graphics.GetHdc();
                int logicalScreenHeight = GetDeviceCaps(desktop, (int) DeviceCap.VERTRES);
                int physicalScreenHeight = GetDeviceCaps(desktop, (int) DeviceCap.DESKTOPVERTRES);
            //graphics.Dispose();
                float screenScalingFactor = (float) physicalScreenHeight / (float) logicalScreenHeight;
                if (screenScalingFactor > 1) {
                    width = (int) (width * screenScalingFactor);
                    height = (int) (height * screenScalingFactor);
                }
            }
            string filename = m_cwScreen.getScreenshot(Screen.PrimaryScreen.WorkingArea.X,
                                                       Screen.PrimaryScreen.WorkingArea.Y,
                                                       width, height);
            invokeShow();
            addFileToWall(filename, location);
        }
        public void sendScreenShot() { sendScreenshot(m_cwWallPanel.getDefaultSurfaceLocation()); }

        //public void sendClippedScreenshot() {
        //    try { m_cwNetwork.assertConnected(); } catch { return; }
        //    //m_cwScreen.getClippedScreenshot();
        //    using(Form f = new CaptureForm()) {
        //        f.ShowDialog(this);
        //    }
        //}

        public void sendFile() {

            try {
                m_cwNetwork.assertConnected();
            } catch {
                return;
            }
            using (OpenFileDialog fd = new OpenFileDialog()) {
                fd.Filter = "Image Files (*.bmp, *.jpg, *.png, *.gif, *.tif)|*.bmp; *.jpg; *.png; *.gif; *.tif";
                fd.RestoreDirectory = true;
                if (fd.ShowDialog() != DialogResult.OK) {
                    return;
                }
                addFileToWall(fd.FileName);
            }

        }

        public void addFileToWall(string filename, PointF location) {
            if (!location.IsEmpty) {
                try {
                    this.m_cwWidget.showElementsProgress(false, "Loading image...");
                    CWImage image = new CWImage(this, filename);
                    image.setLocation(location, false);
                    m_cwWall.addImage(image);
                    m_cwNetwork.sendImage(image);
                    this.Invalidate();
                } catch (Exception exception) {
                    if (logger.IsErrorEnabled)
                        logger.Error("Could not add image: " + exception.Message);
                }
            }
        }
        public void addFileToWall(string filename) { addFileToWall(filename, m_cwWallPanel.getDefaultSurfaceLocation()); }

        // For displaying images uploaded by other clients.
        internal void addShadowToWall(CWImage shadow, PointF location) {
            shadow.setLocation(location, false);
            m_cwWall.addImage(shadow);
            this.Invalidate();
        }

        public void updateSettings(bool firstTime) {
            if (m_cwSettings == null || !m_cwSettings.Visible) {
                m_cwSettings = new CWSettingsForm(this, firstTime);
                m_cwSettings.ShowDialog(this);
            }
            else {
                // We don't want the form opened a second time..
                CWCommon.FlashWindow(m_cwSettings);
            }
        }

        public void connect(string ip, ushort port) {
            if (m_cwNetwork.isConnected()) {
                m_cwNetwork.closeConnection();
            }
            m_cwNetwork.openConnection(ip, port);
        }
        public void connect() {
            if (m_cwNetwork.isConnected()) {
                m_cwNetwork.closeConnection();
            }
            m_cwNetwork.openConnection();
        }

        public void reconnect() {
            if (m_cwSettings == null || !m_cwSettings.Visible) {
                connect();
            }
            else {
                // Settings window open..
                CWCommon.FlashWindow(m_cwSettings);
            }
        }
        public void refresh() {
            try { m_cwNetwork.assertConnected(); } catch { return; }
            clearWall(false);
            m_cwNetwork.requestModel();
            invokeShow();
        }

        //Clears the wall. If <sendCommandToServer> is true it will also send a command to the server
        // to clear the display wall and walls of other clients.
        public void clearWall(bool sendCommandToServer) {
            m_cwWall.clearImages();
            this.Invalidate();
            if (sendCommandToServer) {
                try { m_cwNetwork.assertConnected(); } catch { return; }
                if (m_cwNetwork.m_modelDownloadInProgress) {
                    m_cwNetwork.m_ignoreIncomingDownloads = true;
                }
                m_cwNetwork.sendClearWall();
                if (logger.IsInfoEnabled)
                    logger.Info("Wall cleared");
            }
        }

        public void exitContextuWall() {
            System.Windows.Forms.Application.Exit();
        }

        /////<summary>
        /////Activates or deactivates items (hard coded list) in the context menu strip of the ContextuWall widget.
        /////</summary>
        /////<param name="enable">if set to true the items are enabled, if set to false the items are not enabled</param>
        //public void changeActivationContextMenuStripItems(bool enable)
        //{

        //    string[] items = new string[] { "Send Screenshot", "Send File", "Clear Wall", "Primary Display", "Refresh" };

        //    foreach (ToolStripItem toolStripItem in m_notify.ContextMenuStrip.Items)
        //    {
        //        if (items.Contains(toolStripItem.Text))
        //        {
        //            toolStripItem.Enabled = enable;
        //        }
        //    }

        //}

        public delegate void changeActivationContextMenuStripItemsCallback(bool enable);

        public void InvokeChangeActivationContextMenuStripItems(bool enable) {
            Invoke(new changeActivationContextMenuStripItemsCallback(_changeActivationContextMenuStripItems), new object[] { enable });
        }

        public void changeActivationContextMenuStripItems(bool enable) {
            if (InvokeRequired) {
                InvokeChangeActivationContextMenuStripItems(enable);
            }
            else {
                _changeActivationContextMenuStripItems(enable);
            }
        }

        ///<summary>
        ///Activates or deactivates items (hard coded list) in the context menu strip of the ContextuWall widget.
        ///</summary>
        ///<param name="enable">if set to true the items are enabled, if set to false the items are not enabled</param>
        public void _changeActivationContextMenuStripItems(bool enable) {

            string[] items = new string[] { "Send Screenshot", "Send File", "Clear Wall", "Primary Display", "Refresh" };

            foreach (ToolStripItem toolStripItem in m_notify.ContextMenuStrip.Items) {
                if (items.Contains(toolStripItem.Text)) {
                    toolStripItem.Enabled = enable;
                }
            }

        }

        private void CWUIForm_DragEnter(object sender, DragEventArgs e) {

            // only accept files being dropped
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
                e.Effect = DragDropEffects.Copy;
            }

        }

        private void CWUIForm_DragDrop(object sender, DragEventArgs e) {

            try { m_cwNetwork.assertConnected(); } catch { return; }
            // TODO e.X, e.Y x- and y-coordinates of the mouse pointer, in screen coordinates
            // use this to drop files on the appropriate position on the wall
            string[] fileNames = (string[]) e.Data.GetData(DataFormats.FileDrop);
            foreach (string fileName in fileNames)
                if (fileName.EndsWith(".bmp") || fileName.EndsWith(".jpg") || fileName.EndsWith(".png") ||
                    fileName.EndsWith(".gif") || fileName.EndsWith(".tif")) {
                    this.addFileToWall(fileName);
                }

        }

    }
}