﻿namespace ContextuWall {
    partial class CWUIWidget {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMenu = new System.Windows.Forms.Button();
            this.labelProgress = new System.Windows.Forms.Label();
            this.progressBarProgress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.29091F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(284, 10);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.MinimumSize = new System.Drawing.Size(60, 60);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(60, 60);
            this.btnClose.TabIndex = 0;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMenu
            // 
            this.btnMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.29091F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu.Location = new System.Drawing.Point(284, 180);
            this.btnMenu.Margin = new System.Windows.Forms.Padding(2);
            this.btnMenu.MinimumSize = new System.Drawing.Size(60, 60);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(60, 60);
            this.btnMenu.TabIndex = 1;
            this.btnMenu.TabStop = false;
            this.btnMenu.Text = "☰";
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.BackColor = System.Drawing.Color.Transparent;
            this.labelProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.labelProgress.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelProgress.Location = new System.Drawing.Point(10, 186);
            this.labelProgress.Margin = new System.Windows.Forms.Padding(0);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(34, 29);
            this.labelProgress.TabIndex = 0;
            this.labelProgress.Text = "...";
            this.labelProgress.Visible = false;
            // 
            // progressBarProgress
            // 
            this.progressBarProgress.Location = new System.Drawing.Point(15, 220);
            this.progressBarProgress.Margin = new System.Windows.Forms.Padding(0);
            this.progressBarProgress.MarqueeAnimationSpeed = 0;
            this.progressBarProgress.Name = "progressBarProgress";
            this.progressBarProgress.Size = new System.Drawing.Size(200, 20);
            this.progressBarProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarProgress.TabIndex = 0;
            this.progressBarProgress.Visible = false;
            // 
            // CWUIWidget
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(110F, 110F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(37)))));
            this.BackgroundImage = global::ContextuWall.Properties.Resources.ContextuWallSplash;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(354, 250);
            this.Controls.Add(this.progressBarProgress);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.btnClose);
            this.ForeColor = System.Drawing.SystemColors.MenuText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(35, 25);
            this.Name = "CWUIWidget";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "ContextuWall";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CWUIWidget_FormClosed);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.CWUIWidget_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.CWUIWidget_DragEnter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ProgressBar progressBarProgress;
    }
}