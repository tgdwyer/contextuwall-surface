﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace ContextuWall {
    public partial class CWUIWidget : Form, IInteractionHandler {
        IntPtr _context;
        bool disposed = false;
        Image m_backgroundImage = Properties.Resources.ContextuWallSplash;
        PointF m_location;
        int m_minHeight;
        float m_widgetAspectRatio;

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CWUIForm m_uiForm;
        public CWUIWidget(CWUIForm uiForm) {
            InitializeComponent();
            _context = Win32.CreateInteractionContext(this, SynchronizationContext.Current);
            enableInteraction();
            DoubleBuffered = true; // to prevent background image from flickering.
            this.m_uiForm = uiForm;
            this.m_location = Location;
            this.m_minHeight = btnClose.Size.Height * 4 + 50; // a value that will vary with screen dpi.

            // store widget aspect ratio
            m_widgetAspectRatio = (float) Width / Height;

            // Initially display the widget fullscreen as a backdrop.
            this.StartPosition = FormStartPosition.Manual;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        // Shrinks the widget down from fullscreen to its default position and size
        //TODO: add some lightweight animation overlay, as resizing the form itself has horrible performance..
        public void animateShrink() {
            setDefaultPosition();
            //int screenWidth = m_uiForm.SCREEN_WIDTH;
            //int screenHeight = m_uiForm.SCREEN_HEIGHT;
            //int wallWidth = (int)m_uiForm.m_wallWidth;

            //int targetLength = m_minHeight * 2;
            //int targetX = 2 + screenWidth - wallWidth - targetLength;
            //int targetY = 2 + screenHeight - targetLength;

            //int reps = 500;
            //float widthStep = (screenWidth - targetLength) / (float)reps;
            //float heightStep = (screenHeight - targetLength) / (float)reps;
            //float leftStep = targetX / (float)reps;
            //float topStep = targetY / (float)reps;

            //for(int i = 1; i <= reps; i++) {
            //    this.Size = new Size((int)(screenWidth - i * widthStep), (int)(screenHeight - i * heightStep));
            //    this.Location = new Point((int)(i * leftStep), (int)(i * topStep));
            //    Application.DoEvents();
            //}
            //m_location = this.Location;
            //m_uiForm.invokeHide(false);
        }

        //public void enable() {
        //    btnClose.Show();
        //    btnMenu.Show();
        //    TopMost = true;
        //}
        //public void disable() {
        //    btnClose.Hide();
        //    btnMenu.Hide();
        //    TopMost = false;
        //}

        public delegate void enableCallback();
        public delegate void disableCallback();

        public void InvokeEnable() {
            Invoke(new enableCallback(_enable));
        }

        public void InvokeDisable() {
            Invoke(new disableCallback(_disable));
        }

        public void enable() {
            if (InvokeRequired) {
                InvokeEnable();
            }
            else {
                _enable();
            }
        }

        public void disable() {
            if (InvokeRequired) {
                InvokeDisable();
            }
            else {
                _disable();
            }
        }

        public void _enable() {
            btnClose.Show();
            btnMenu.Show();
            TopMost = true;
        }

        public void _disable() {
            btnClose.Hide();
            btnMenu.Hide();
            TopMost = false;
        }

        public void setDefaultPosition() {
            Point dock = m_uiForm.m_cwWallPanel.m_widgetDock;
            int x = (int) (dock.X - m_minHeight * m_widgetAspectRatio);
            int y = dock.Y - m_minHeight;
            setLocation(new Point(x, y));
            this.Width = (int) (m_minHeight * m_widgetAspectRatio);
            this.Height = m_minHeight;
            // store widget aspect ratio
            m_widgetAspectRatio = (float) Width / Height;
            // set buttons relative to border of the widget
            btnClose.Location = new Point(Width - btnClose.Width - 10, btnClose.Location.Y);
            btnMenu.Location = new Point(Width - btnMenu.Width - 10, Height - btnMenu.Height - 10);
            progressBarProgress.Location = new Point(progressBarProgress.Location.X, Height - progressBarProgress.Height - 10);
            labelProgress.Location = new Point(labelProgress.Location.X, Height - labelProgress.Height - progressBarProgress.Height - 15);
        }

        public void setLocation(PointF newLocation) {
            m_location = newLocation;
            Location = new Point((int) m_location.X, (int) m_location.Y);
        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnMenu_Click(object sender, EventArgs e) {
            m_uiForm.invokeShowContextMenuStrip(new Point(this.Left + this.Width, this.Top));
        }

        // http://stackoverflow.com/questions/68598/how-do-i-drag-and-drop-files-into-a-c-sharp-application
        private void CWUIWidget_DragEnter(object sender, DragEventArgs e) {

            if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
                e.Effect = DragDropEffects.Copy;
            }

        }

        private void CWUIWidget_DragDrop(object sender, DragEventArgs e) {

            try { m_uiForm.m_cwNetwork.assertConnected(); } catch { return; }
            string[] filenames = (string[]) e.Data.GetData(DataFormats.FileDrop);
            foreach (string filename in filenames)
                if (filename.EndsWith(".bmp") || filename.EndsWith(".jpg") || filename.EndsWith(".png") ||
                    filename.EndsWith(".gif") || filename.EndsWith(".tif")) {
                    m_uiForm.addFileToWall(filename);
                }            

        }

        private void CWUIWidget_FormClosed(object sender, EventArgs e) {
            Application.Exit();
        }

        protected override void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing && components != null) {
                    components.Dispose();
                }
                Win32.DisposeInteractionContext(_context);
                _context = IntPtr.Zero;

                disposed = true;
            }
            base.Dispose(disposing);
        }


        // **************************************************************************
        // *************************** WIDGET INTERACTION ***************************
        //  http://www.codeproject.com/Articles/607234/Using-Windows-Interaction-Con
        // **************************************************************************

        // Handles the various interactions.
        void IInteractionHandler.ProcessInteractionEvent(InteractionOutput output) {
            switch (output.Data.Interaction) {
                case Win32.INTERACTION.TAP:
                    if (m_uiForm.Visible) {
                        m_uiForm.invokeHide(false);
                    }
                    else {
                        m_uiForm.invokeShow();
                    }
                    break;
                case Win32.INTERACTION.SECONDARY_TAP:
                    //Console.WriteLine("SecondaryTap!");
                    m_uiForm.invokeShowContextMenuStrip(new Point((int) output.Data.X, (int) output.Data.Y));
                    break;
                case Win32.INTERACTION.MANIPULATION:
                    if (!output.IsBegin()) {
                        Win32.MANIPULATION_TRANSFORM mt = output.Data.Manipulation.Delta;
                        PointF newLocation = m_location;
                        // Scaling
                        if (mt.Scale != 1.0) {
                            PointF midPoint = new PointF(m_location.X + Size.Width / 2.0f, m_location.Y + Size.Height / 2.0f);
                            this.Size = new Size(Math.Max((int) (Size.Width * mt.Scale), m_minHeight), Math.Max((int) (Size.Height * mt.Scale), m_minHeight));
                            newLocation = new PointF(midPoint.X - Size.Width / 2.0f, midPoint.Y - Size.Height / 2.0f);
                        }
                        // Movement
                        setLocation(new PointF(newLocation.X + mt.TranslationX, newLocation.Y + mt.TranslationY));
                    }
                    break;
            }
        }

        // Determines which interactions are to be handled.
        private void enableInteraction() {
            Win32.INTERACTION_CONTEXT_CONFIGURATION[] cfg = new Win32.INTERACTION_CONTEXT_CONFIGURATION[]{
            // TAPPING (Left click)
            new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.TAP,
                Win32.INTERACTION_CONFIGURATION_FLAGS.TAP |
                Win32.INTERACTION_CONFIGURATION_FLAGS.TAP_DOUBLE
                ),
            // SECONDARY TAPPING (Right click)
            new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.SECONDARY_TAP,
                Win32.INTERACTION_CONFIGURATION_FLAGS.SECONDARY_TAP),
            new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.HOLD,
                Win32.INTERACTION_CONFIGURATION_FLAGS.HOLD),
            // MOVING & SCALING
            new Win32.INTERACTION_CONTEXT_CONFIGURATION(Win32.INTERACTION.MANIPULATION,
                Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION |
                Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_X |
                Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_Y |
                Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_SCALING
                //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_TRANSLATION_INERTIA |
                //Win32.INTERACTION_CONFIGURATION_FLAGS.MANIPULATION_SCALING_INERTIA
                ),
            };
            Win32.SetInteractionConfigurationInteractionContext(_context, cfg.Length, cfg);
        }

        // Catches the relevant WM events and hands them off to the InteractionContext.
        HashSet<int> _activePointers = new HashSet<int>();
        protected override void WndProc(ref Message m) {
            int pointerID = Win32.GET_POINTER_ID(m.WParam);
            Win32.POINTER_INFO pi = new Win32.POINTER_INFO();
            Win32.GetPointerInfo(pointerID, ref pi);
            switch (m.Msg) {
                case Win32.WM_POINTERDOWN:
                    Win32.AddPointerInteractionContext(_context, pointerID);
                    _activePointers.Add(pointerID);
                    ProcessPointerFrames(pointerID, pi.FrameID);
                    break;
                case Win32.WM_POINTERUP:
                    if (_activePointers.Contains(pointerID)) {
                        ProcessPointerFrames(pointerID, pi.FrameID);
                        Win32.RemovePointerInteractionContext(_context, pointerID);
                        _activePointers.Remove(pointerID);
                    }
                    break;
                case Win32.WM_POINTERUPDATE:
                    if (_activePointers.Contains(pointerID)) {
                        ProcessPointerFrames(pointerID, pi.FrameID);
                    }
                    break;
                case Win32.WM_POINTERCAPTURECHANGED:
                    foreach (int pID in _activePointers) {
                        Win32.RemovePointerInteractionContext(_context, pID);
                    }
                    _activePointers.Clear();
                    Win32.StopInteractionContext(_context);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        int _lastFrameID = -1;

        // Processes the active pointers on the widget.
        public void ProcessPointerFrames(int pointerID, int frameID) {
            if (_lastFrameID != frameID) {
                _lastFrameID = frameID;
                int entriesCount = 0;
                int pointerCount = 0;
                if (!Win32.GetPointerFrameInfoHistory(pointerID, ref entriesCount,
                ref pointerCount, IntPtr.Zero)) {
                    Win32.CheckLastError();
                }
                Win32.POINTER_INFO[] piArr = new Win32.POINTER_INFO[entriesCount * pointerCount];
                if (!Win32.GetPointerFrameInfoHistory(pointerID, ref entriesCount, ref pointerCount, piArr)) {
                    Win32.CheckLastError();
                }
                IntPtr hr = Win32.ProcessPointerFramesInteractionContext(_context,
                            entriesCount, pointerCount, piArr);
                if (Win32.FAILED(hr)) {
                    Debug.WriteLine("ProcessPointerFrames failed: " + Win32.GetMessageForHR(hr));
                }
            }
        }

        // the widget is used to show the progress during download and upload of images
        public delegate void showElementsProgressCallback(bool marquee, string text);
        public delegate void updateElementsProgressCallback(int value);
        public delegate void hideElementsProgressCallback();

        public void invokeShowElementsProgress(bool marquee, string text) {

            this.Invoke(new showElementsProgressCallback(this._showElementsProgress), new object[] { marquee, text });

        }

        public void invokeUpdateElementsProgress(int value) {

            this.Invoke(new updateElementsProgressCallback(this._updateElementsProgress), new object[] { value });

        }

        public void invokeHideElementsProgress() {

            this.Invoke(new hideElementsProgressCallback(this._hideElementsProgress));

        }

        public void showElementsProgress(bool marquee, string text) {

            if (this.InvokeRequired) {
                invokeShowElementsProgress(marquee, text);
            } else
                _showElementsProgress(marquee, text);

        }

        public void updateElementsProgress(int value) {

            if (this.InvokeRequired) {
                invokeUpdateElementsProgress(value);
            } else
                _updateElementsProgress(value);

        }

        public void hideElementsProgress() {

            if (this.InvokeRequired) {
                invokeHideElementsProgress();
            } else
                _hideElementsProgress();

        }

        public void _showElementsProgress(bool marquee, string text) {

            labelProgress.Text = text;
            labelProgress.Show();
            progressBarProgress.Show();
            if (marquee) {
                progressBarProgress.Style = ProgressBarStyle.Marquee;
                progressBarProgress.MarqueeAnimationSpeed = 50;
            }
            else {
                progressBarProgress.Style = ProgressBarStyle.Blocks;
                progressBarProgress.Value = 0;
            }
            this.Invalidate(true); // invalidate children
            this.Update(); // and redraw
            // this.Refresh(); // complete redraw

        }

        public void _updateElementsProgress(int value) {

            // this is a workaround for the animation problem of the progress bar
            // increasing the progress is animated and the bar is set too late
            // decreasing the progress is not animated and the bar is set immediately
            // in addition it is necessary to increase the maximum if value == maximum
            if (value == 100)
                progressBarProgress.Maximum = value + 1;
            progressBarProgress.Value = value + 1;
            progressBarProgress.Value = progressBarProgress.Value - 1;
            if (value == 100)
                progressBarProgress.Maximum = value;
            this.Invalidate(true); // invalidate children
            this.Update(); // and redraw
            //this.Refresh(); // complete redraw

        }

        public void _hideElementsProgress() {

            labelProgress.Hide();
            progressBarProgress.MarqueeAnimationSpeed = 0;
            progressBarProgress.Value = 0;
            progressBarProgress.Hide();
            this.Invalidate(true); // invalidate children
            this.Update(); // and redraw
            // this.Refresh(); // complete redraw

        }

        // **************************************************************************
        // *********************** END OF WIDGET INTERACTION ************************
        // **************************************************************************
        // **************************************************************************


        //protected override CreateParams CreateParams {
        //    get {
        //        CreateParams cp = base.CreateParams;
        //        // Adds a border to the 'borderless' form, to allow resizing by grabbing its edges. (Useful for mouse interaction)
        //        cp.Style |= 0x40000;
        //        return cp;
        //    }
        //}

        // Cleans up everything when the widget is deleted.
        ~CWUIWidget() {
            Dispose(false);
        }

    }
}
