﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ContextuWall {
    public class CWWall {
        public CWUIForm m_uiForm;

        private List<ICWImage> m_images;

        private readonly Object m_lock = new Object();

        public CWWall(CWUIForm uiForm) {
            m_uiForm = uiForm;
            m_images = new List<ICWImage>();
        }

        public void addImage(ICWImage image) {
            lock (m_lock) {
                m_images.Add(image);
            }
        }

        public ICWImage getImageWithFilename(string filename) {
            lock (m_lock) {
                return m_images.FirstOrDefault(item => item.filename.Equals(filename));
            }
        }

        // Moves the image to the end of the list, s.t. it renders last, and thus ends up topmost.
        public void bringImageToFront(ICWImage image) {
            lock (m_lock) {
                m_images.Remove(image);
                m_images.Add(image);
            }
        }

        // Moves the image to the front of the list, s.t. it renders first, and thus ends up bottommost.
        public void sendImageToBack(ICWImage image) {
            lock (m_lock) {
                m_images.Remove(image);
                m_images.Insert(0, image);
            }
        }
        public void removeImage(ICWImage image) {
            if (m_uiForm.m_cwAnnotationMode.active && image == m_uiForm.m_cwAnnotationMode.getImageBeingAnnotated()) {
                m_uiForm.m_cwAnnotationMode.end();
            }
            lock (m_lock) {
                m_images.Remove(image);
                image.Dispose();
            }
        }
        public IEnumerable<ICWImage> getImages() {
            lock (m_lock) {
                for (int i = 0; i < m_images.Count; i++) {
                    yield return m_images[i];
                }
            }
        }
        public IEnumerable<ICWImage> getImagesReversed() {
            lock (m_lock) {
                for (int i = m_images.Count - 1; i >= 0; i--) {
                    yield return m_images[i];
                }
            }
        }
        public void repositionImages() {
            lock (m_lock) {
                foreach (ICWImage img in m_images) {
                    img.reposition();
                }
            }
        }

        public void flattenAnnotationsOntoImages() {
            lock (m_lock) {
                foreach (ICWImage img in m_images) {
                    img.flattenAnnotations();
                }
            }
        }

        public void drawImages(PaintEventArgs e) {
            lock (m_lock) {
                foreach (ICWImage img in m_images) {
                    img.draw(e.Graphics);
                }
                // Iterate a second time, to draw the outlines over the images
                //  s.t. we can see where there are images behind other images.
                foreach (ICWImage img in m_images) {
                    img.drawOutline(e.Graphics);
                }
                // Draw the uncommitted annotation layer.
                if (m_uiForm.m_cwAnnotationMode.active) {
                    m_uiForm.m_cwAnnotationMode.draw(e.Graphics);
                }
            }
        }
        public void clearImages() {
            if (m_uiForm.m_cwAnnotationMode.active) {
                m_uiForm.m_cwAnnotationMode.end();
            }
            lock (m_lock) {
                foreach (ICWImage img in m_images) {
                    img.Dispose();
                }
                m_images.Clear();
            }
        }
    }
}