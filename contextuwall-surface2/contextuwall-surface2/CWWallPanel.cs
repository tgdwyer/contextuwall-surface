﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ContextuWall {
    public class CWWallPanel : BaseHandler {
        public CWUIForm m_uiForm;
        // Screen dims duplicated from CWUIForm, since they get used frequently here and code would otherwise get messy.
        public int SCREEN_WIDTH;
        public int SCREEN_HEIGHT;
        public Region m_region;
        public Point m_widgetDock;
        public Rectangle m_annotationRect;
        public Point m_annotationToolboxDock;
        public Color m_colour = Color.LightBlue;
        private HashSet<Object> m_objectsWantingHighlight = new HashSet<Object>();
        private readonly Object m_lock = new Object();
        public int m_width { get; set; }
        public int m_height { get; set; }
        public float m_aspectRatio { get; set; }
        public float m_minImageHeightScaling { get; set; }
        public bool m_showSidebars { get; set; }
        public int m_sideLength { get; set; } // only meaningful when sidebars are enabled.

        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public CWWallPanel(CWUIForm uiForm) : base() {
            m_uiForm = uiForm;
            m_aspectRatio = Properties.Settings.Default.WallAspectRatio;
            m_minImageHeightScaling = Properties.Settings.Default.MinImageHeightScaling;
            m_showSidebars = Properties.Settings.Default.ShowSideBars;
            enableInteraction();
        }

        // NOTE: To handle interaction with this panel, complete the below functions as done in CWImage
        private void enableInteraction() { }
        internal override void ProcessEvent(InteractionOutput output) { }

        public void computeRegion() {
            SCREEN_WIDTH = m_uiForm.SCREEN_WIDTH;
            SCREEN_HEIGHT = m_uiForm.SCREEN_HEIGHT;

            GraphicsPath gp = new GraphicsPath();
            gp.StartFigure();
            if (m_showSidebars) {
                // This formula is obtained when taking wall length to be the length at mid-height of the wall
                //  (due to the bends, the length varies.. ...but that's okay, since we'll skew all image dimensions to compensate!)
                m_height = Convert.ToInt32((SCREEN_HEIGHT * 2 + SCREEN_WIDTH) / (m_aspectRatio + 3));
                m_width = Convert.ToInt32(m_aspectRatio * m_height);
                m_sideLength = SCREEN_HEIGHT - m_height;
                gp.AddLines(new Point[]{
                                new Point(0,0),
                                new Point(SCREEN_WIDTH, 0),
                                new Point(SCREEN_WIDTH, SCREEN_HEIGHT),
                                new Point(SCREEN_WIDTH - m_height, m_sideLength),
                                new Point(SCREEN_WIDTH - m_height, m_height),
                                new Point(m_height, m_height),
                                new Point(m_height, m_sideLength),
                                new Point(0, SCREEN_HEIGHT)
                            }
                );
                m_widgetDock = new Point(SCREEN_WIDTH - m_height, SCREEN_HEIGHT);
                m_annotationRect = new Rectangle(m_height, m_height, SCREEN_WIDTH - 2 * m_height, m_sideLength);
                m_annotationToolboxDock = new Point(m_annotationRect.Right + 2, m_height + m_sideLength / 8);
            }
            else {
                // Single strip along the top.
                m_width = SCREEN_WIDTH;
                m_height = Convert.ToInt32(m_width / m_aspectRatio);
                m_sideLength = SCREEN_HEIGHT - m_height;
                gp.AddLines(new Point[]{
                                new Point(0,0),
                                new Point(SCREEN_WIDTH, 0),
                                //new Point(SCREEN_WIDTH, SCREEN_HEIGHT),
                                new Point(SCREEN_WIDTH, m_height),
                                new Point(0, m_height)
                            }
                );
                m_widgetDock = new Point(SCREEN_WIDTH, SCREEN_HEIGHT);
                //m_annotationRect = new Rectangle(0, m_height, SCREEN_WIDTH, m_sideLength);
                m_annotationRect = new Rectangle(0, m_height, SCREEN_WIDTH - m_uiForm.m_cwAnnotationToolbox.Size.Width - 2, m_sideLength);
                //m_annotationToolboxDock = new Point(SCREEN_WIDTH - 10, m_height + m_sideLength / 8);
                m_annotationToolboxDock = new Point(m_annotationRect.Right + 2, m_height + 2);
            }
            gp.CloseFigure();
            lock (m_lock) {
                m_region = new Region(gp);
            }

        }

        // Returns a point within the wall panel based on a linear
        //  extrapolation of the two provided points.   
        public PointF getSurfaceLocation(PointF p1, PointF p2) {
            // Release point already in wall? Return it!
            if (containsPoint(p2)) {
                return p2;
            }
            else {
                float x, y;
                float dx = p2.X - p1.X, dy = p2.Y - p1.Y;
                // Assume we can project the swipe to top wall.
                y = 0;
                x = p1.X - p1.Y * dx / dy;
                if (dy > 0 || x < 0 || x > SCREEN_WIDTH) {
                    // Assumption wrong; try side walls.
                    if (dx < 0) {
                        x = 0;
                        y = p1.Y - p1.X * dy / dx;
                    }
                    else {
                        x = SCREEN_WIDTH;
                        y = p1.Y - (p1.X - SCREEN_WIDTH) * dy / dx;
                    }
                    if (y < 0 || y > SCREEN_HEIGHT || !m_showSidebars) {
                        // Side walls didn't work either.
                        return PointF.Empty;
                    }
                }
                return new PointF(x, y);
            }
        }
        public PointF getDefaultSurfaceLocation() { return new PointF(SCREEN_WIDTH / 2, 0); }

        // Maps a wall panel location to a normalised display location.
        public PointF normaliseLocation(PointF loc) {
            float x = loc.X, y = loc.Y;
            if (m_showSidebars) {
                // Map x,y segmented wall panel coords to a flat rectangular strip.
                // Within left wall?
                if (x <= m_height && y - x >= 0) {
                    return new PointF(
                        (SCREEN_HEIGHT - y - x) / (SCREEN_HEIGHT - 2 * x) * (m_sideLength) / m_width,
                        x / m_height
                    );
                }
                // Within right wall?
                if (x >= SCREEN_WIDTH - m_height && y + x - SCREEN_WIDTH >= 0) {
                    return new PointF(
                        ((y - (SCREEN_WIDTH - x)) / (SCREEN_HEIGHT - 2 * (SCREEN_WIDTH - x)) * (m_sideLength) + SCREEN_HEIGHT + SCREEN_WIDTH - 2 * m_height) / m_width,
                        (SCREEN_WIDTH - x) / m_height
                    );
                }
                // Within top wall!
                return new PointF(
                    ((x - y) / (SCREEN_WIDTH - 2 * y) * (SCREEN_WIDTH - m_height) + m_sideLength) / m_width,
                    y / m_height
                );
            }
            return new PointF(x / m_width, y / m_height);
        }

        // Maps a normalised display location to a corresponding location on the wall panel.
        public Point denormaliseLocation(PointF normLoc) {
            float x = normLoc.X * m_width, y = normLoc.Y * m_height;
            if (m_showSidebars) {
                // Map normalised x,y coords from flat rectangular strip to segmented wall panel.
                //  +-------------------------+
                //  |\                       /|
                //  | \                     / |
                //  |  +-------------------+  |
                //  |  |                   |  | 
                //  |  |                   |  |
                //  |  |                   |  |
                //  |  +                   +  |
                //  | /                     \ |
                //  |/                       \|
                //  +                         +
                // Within left wall?
                if (x <= m_sideLength) {
                    return new Point(Convert.ToInt32(y), SCREEN_HEIGHT - Convert.ToInt32(x * (SCREEN_HEIGHT - 2 * y) / (m_sideLength) + y));
                }
                // Within right wall?
                if (x >= m_width - m_sideLength) {
                    return new Point(SCREEN_WIDTH - Convert.ToInt32(y), Convert.ToInt32((x - m_width + m_sideLength) * (SCREEN_HEIGHT - 2 * y) / (m_sideLength) + y));
                }
                // Within top wall!
                return new Point(Convert.ToInt32((x - m_sideLength) * (SCREEN_WIDTH - 2 * y) / (m_width - 2 * m_sideLength) + y), Convert.ToInt32(y));
            }
            return new Point(Convert.ToInt32(x), Convert.ToInt32(y));
        }

        public void paint(PaintEventArgs e) {
            Graphics g = e.Graphics;

            using (SolidBrush myBrush = new System.Drawing.SolidBrush(m_colour)) {
                lock (m_lock) {
                    // Draw the region to your Graphics object
                    g.FillRegion(myBrush, m_region);
                }
            }
        }

        public void highlight(Object o, Boolean highlight) {
            if (highlight) {
                m_objectsWantingHighlight.Add(o);
                m_colour = System.Drawing.Color.DarkBlue;
            }
            else {
                m_objectsWantingHighlight.Remove(o);
                if (m_objectsWantingHighlight.Count == 0) {
                    m_colour = System.Drawing.Color.LightBlue;
                }
            }
            m_uiForm.Invalidate();
        }

        public bool containsPoint(PointF p) {
            using (Graphics g = m_uiForm.CreateGraphics()) {
                lock (m_lock) {
                    return m_region.IsVisible(p, g);
                }
            }
        }

        internal List<Tuple<float, Quadrilateral>> denormaliseRect(RectangleF rect) {
            List<Tuple<float, Quadrilateral>> quads = new List<Tuple<float, Quadrilateral>>();
            using (Graphics g = m_uiForm.CreateGraphics()) {
                if (m_showSidebars &&
                   ((rect.Left < m_sideLength / (float) m_width && rect.Right > m_sideLength / (float) m_width) ||
                   (rect.Left < (m_width - m_sideLength) / (float) m_width && rect.Right > (m_width - m_sideLength) / (float) m_width))) {
                    //We need to split the rect into multiple quadrilaterals!
                    if (rect.Left < m_sideLength / (float) m_width) {
                        quads.Add(Tuple.Create(
                                    (m_sideLength / (float) m_width - rect.Left) / (rect.Right - rect.Left),
                                    new Quadrilateral(new Point[]{
                                        denormaliseLocation(new PointF(rect.Left, rect.Top)),
                                        denormaliseLocation(new PointF(m_sideLength / (float)m_width, rect.Top)),
                                        denormaliseLocation(new PointF(m_sideLength / (float)m_width, rect.Bottom)),
                                        denormaliseLocation(new PointF(rect.Left, rect.Bottom)),
                                    }, g)
                        ));
                        if (rect.Right < (m_width - m_sideLength) / (float) m_width) {
                            quads.Add(Tuple.Create(
                                        (rect.Right - m_sideLength / (float) m_width) / (rect.Right - rect.Left),
                                        new Quadrilateral(new Point[]{
                                            denormaliseLocation(new PointF(m_sideLength / (float)m_width, rect.Top)),
                                            denormaliseLocation(new PointF(rect.Right, rect.Top)),
                                            denormaliseLocation(new PointF(rect.Right, rect.Bottom)),
                                            denormaliseLocation(new PointF(m_sideLength / (float)m_width, rect.Bottom)),
                                        }, g)
                            ));
                        }
                        else {
                            quads.Add(Tuple.Create(
                                        (m_width - 2 * m_sideLength) / (float) m_width / (rect.Right - rect.Left),
                                        new Quadrilateral(new Point[]{
                                            denormaliseLocation(new PointF(m_sideLength / (float)m_width, rect.Top)),
                                            denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Top)),
                                            denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Bottom)),
                                            denormaliseLocation(new PointF(m_sideLength / (float)m_width, rect.Bottom)),
                                        }, g)
                            ));
                            quads.Add(Tuple.Create(
                                        (rect.Right - (m_width - m_sideLength) / (float) m_width) / (rect.Right - rect.Left),
                                        new Quadrilateral(new Point[]{
                                            denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Top)),
                                            denormaliseLocation(new PointF(rect.Right, rect.Top)),
                                            denormaliseLocation(new PointF(rect.Right, rect.Bottom)),
                                            denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Bottom)),
                                        }, g)
                            ));
                        }
                    }
                    else {
                        quads.Add(Tuple.Create(
                                    ((m_width - m_sideLength) / (float) m_width - rect.Left) / (rect.Right - rect.Left),
                                    new Quadrilateral(new Point[]{
                                        denormaliseLocation(new PointF(rect.Left, rect.Top)),
                                        denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Top)),
                                        denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Bottom)),
                                        denormaliseLocation(new PointF(rect.Left, rect.Bottom)),
                                    }, g)
                        ));
                        quads.Add(Tuple.Create(
                                    (rect.Right - (m_width - m_sideLength) / (float) m_width) / (rect.Right - rect.Left),
                                    new Quadrilateral(new Point[]{
                                        denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Top)),
                                        denormaliseLocation(new PointF(rect.Right, rect.Top)),
                                        denormaliseLocation(new PointF(rect.Right, rect.Bottom)),
                                        denormaliseLocation(new PointF((m_width - m_sideLength) / (float)m_width, rect.Bottom)),
                                    }, g)
                        ));
                    }
                }
                else {
                    quads.Add(Tuple.Create(
                                1f,
                                new Quadrilateral(new Point[]{
                                    denormaliseLocation(new PointF(rect.Left, rect.Top)),
                                    denormaliseLocation(new PointF(rect.Right, rect.Top)),
                                    denormaliseLocation(new PointF(rect.Right, rect.Bottom)),
                                    denormaliseLocation(new PointF(rect.Left, rect.Bottom)),
                                }, g)
                    ));
                }
                return quads;
            }
        }
    }
}