﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace ContextuWall {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private static CWUIForm m_uiForm;
        //public const string WORKING_DIR = @".\";
        private static String m_localApplicationDataPath = null;
        private static String m_tmpImagesPath = null;

        [STAThread]
        static void Main() {

            // change logging programmatically
            // ((Hierarchy) LogManager.GetRepository()).Root.Level = Level.All;
            // ((Hierarchy) LogManager.GetRepository()).RaiseConfigurationChanged(EventArgs.Empty);

            // Verify Windows version.
            Version win8version = new Version(6, 2, 9200, 0);
            if (Environment.OSVersion.Platform != PlatformID.Win32NT || Environment.OSVersion.Version < win8version) {
                MessageBox.Show("ContextuWall runs only on Windows 8 or higher!", "Platform Not Supported", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // http://msdn.microsoft.com/en-us/library/system.windows.forms.application.threadexception(v=vs.110).aspx
            // Add the event handler for handling UI thread exceptions to the event.
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            // Set the unhandled exception mode to force all Windows Forms errors to go through 
            // our handler.
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            // Add the event handler for handling non-UI thread exceptions to the event. 
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

            //http://blogs.msdn.com/b/microsoft_press/archive/2010/02/03/jeffrey-richter-excerpt-2-from-clr-via-c-third-edition.aspx
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) => {

                String resourceName = "AssemblyLoadingAndReflection." + new AssemblyName(args.Name).Name + ".dll";
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName)) {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            setFolders();

            m_uiForm = new CWUIForm();
            Application.Run(m_uiForm);
        }

        static void OnProcessExit(object sender, EventArgs e) {
            m_uiForm.m_notify.Visible = false;
            m_uiForm.m_notify.Icon = null;
        }

        static string getExceptionInfo(Exception exception) {
            var st = new StackTrace(exception, true);
            var frame = st.GetFrame(0);
            var line = frame.GetFileLineNumber();
            string body = exception.Message + "\n" + exception.Data + "\n" + exception.StackTrace + "\n" + exception.Source + "\n" + line;

            return body;
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e) {
            DialogResult result = DialogResult.Cancel;
            try {
                //File.WriteAllText(@".\dump.txt", getExceptionInfo(e.Exception) + Environment.NewLine);
                File.WriteAllText(Path.Combine(getLocalApplicationDataFolder(), "dump.txt"), getExceptionInfo(e.Exception) + Environment.NewLine);
                result = ShowThreadExceptionDialog("Fatal Error", e.Exception);
            } catch {
                try {
                    MessageBox.Show("Fatal Error", "Fatal Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
                } finally {
                    Application.Exit();
                }
            }

            // Exits the program when the user clicks Abort. 
            if (result == DialogResult.Abort)
                Application.Exit();
        }

        // Handle the UI exceptions by showing a dialog box, and asking the user whether 
        // or not they wish to abort execution. 
        // NOTE: This exception cannot be kept from terminating the application - it can only  
        // log the event, and inform the user about it.  
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            try {
                Exception ex = (Exception) e.ExceptionObject;
                //string errorMsg = "An application error occurred. Please contact the adminstrator " +
                //    "with the following information:\n\n";

                // This only works with admin rights!
                // Since we can't prevent the app from terminating, log this to the event log. 
                //if (!EventLog.SourceExists("ThreadException")) {
                //    EventLog.CreateEventSource("ThreadException", "Application");
                //}

                // Create an EventLog instance and assign its source.
                //EventLog myLog = new EventLog();
                //myLog.Source = "ThreadException";
                //myLog.WriteEntry(errorMsg + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);

                //File.WriteAllText(@".\dump.txt", getExceptionInfo(ex) + Environment.NewLine);
                File.WriteAllText(Path.Combine(getLocalApplicationDataFolder(), "dump.txt"), getExceptionInfo(ex) + Environment.NewLine);
                ShowThreadExceptionDialog("Windows Forms Error", ex);
            } catch (Exception exc) {
                try {
                    MessageBox.Show("Fatal Error. Could not write the error to the event log. Reason: "
                        + exc.Message, "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                } finally {
                    Application.Exit();
                }
            }
        }

        // Creates the error message and displays it. 
        private static DialogResult ShowThreadExceptionDialog(string title, Exception e) {
            string errorMsg = "An application error occurred. Please contact the adminstrator " +
                "with the following information:\n\n";
            errorMsg = errorMsg + e.Message + "\n\nStack Trace:\n" + e.StackTrace;
            return MessageBox.Show(errorMsg, title, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
        }

        private static void setFolders() {

            // get local application folder name, e.g., "C:\Users\user_name\AppData\Local"
            // get application name, e.g., ContextuWall
            // set folder names
            m_localApplicationDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Application.ProductName);
            m_tmpImagesPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Application.ProductName, "tmpImages");
            // create local application data folder if it doesn't exist
            if (!Directory.Exists(m_localApplicationDataPath))
                try {
                    Directory.CreateDirectory(m_localApplicationDataPath);
                } catch (Exception exception) {
                    MessageBox.Show("Could not create local application data folder:\n" + exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            // create tmp images folder if it doesn't exist
            if (!Directory.Exists(m_tmpImagesPath))
                try {
                    Directory.CreateDirectory(m_tmpImagesPath);
                } catch (Exception exception) {
                    MessageBox.Show("Could not create temp folder for images:\n" + exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            // delete all images otherwise
            else {
                DirectoryInfo m_DirInfo = new DirectoryInfo(m_tmpImagesPath);
                try {
                    foreach (FileInfo m_FileInfo in m_DirInfo.GetFiles())
                        m_FileInfo.Delete();
                } catch (Exception exception) {
                    MessageBox.Show("Could not delete images from temp folder:\n" + exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

        }

        public static String getLocalApplicationDataFolder() {

            if (m_localApplicationDataPath == null)
                return Path.GetTempPath();
            return m_localApplicationDataPath;

        }

        public static String getTmpImagesFolder() {

            if (m_tmpImagesPath == null)
                return Path.GetTempPath();
            return m_tmpImagesPath;

        }

    }
}
