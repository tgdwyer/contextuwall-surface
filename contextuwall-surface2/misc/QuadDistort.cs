﻿// Code adapted from: http://www.codeproject.com/Articles/247214/Quadrilateral-Distortion

namespace System.Drawing {
    public class QuadDistort {
        // Draws slower, but ensures no gaps (empty pixels) arise when the image is skewed.
        public static bool InterpolationActive = true;
        // Computes the bounding box of the polygon
        private static Rectangle _ComputeBox(params Point[] oPoints) {
            int nBoxTop = int.MaxValue;
            int nBoxLeft = int.MaxValue;
            int nBoxRight = int.MinValue;
            int nBoxBottom = int.MinValue;
            foreach (Point oPoint in oPoints) {
                nBoxTop = Math.Min(nBoxTop, oPoint.Y);
                nBoxLeft = Math.Min(nBoxLeft, oPoint.X);
                nBoxRight = Math.Max(nBoxRight, oPoint.X);
                nBoxBottom = Math.Max(nBoxBottom, oPoint.Y);
            }
            return new Rectangle(nBoxLeft, nBoxTop, nBoxRight - nBoxLeft, nBoxBottom - nBoxTop);
        }
        //just to avoid the getter and setter of the PointF struct
        //saves a few milliseconds
        private struct FastPointF {
            public float X;
            public float Y;
            public FastPointF(float x, float y) {
                this.X = x;
                this.Y = y;
            }
            public override string ToString() {
                return "[" + this.X + "," + this.Y + "]";
            }
        }
        private struct PointMap {
            public FastPointF From;
            public FastPointF To;
            public PointMap(FastPointF oFrom, FastPointF oTo) {
                this.From = oFrom;
                this.To = oTo;
            }
        }

        public static void DrawBitmap(FastBitmap oTexture, Rectangle textureRect, Point topLeft, Point topRight, Point bottomRight, Point bottomLeft, FastBitmap oCanvas) {
            //top line
            float nDeltaX = topRight.X - topLeft.X;
            float nDeltaY = topRight.Y - topLeft.Y;
            float nNumOfPixels = AbsMax(nDeltaX, nDeltaY);
            if (nNumOfPixels == 0)
                return;
            //nNumOfPixels *= 2;
            float nIncrementX = nDeltaX / nNumOfPixels;
            float nIncrementY = nDeltaY / nNumOfPixels;
            FastPointF oPixel = new FastPointF(topLeft.X, topLeft.Y);

            float nTextureIncrementX = (textureRect.Width / (nNumOfPixels));
            float nTextureX = textureRect.Left;

            PointMap oPointMapInstance = new PointMap();
            oPointMapInstance.To = oPixel;
            FastPointF oFastPointFInstance = new FastPointF(0, textureRect.Top);


            PointMap[] oTopLine = new PointMap[(int) (nNumOfPixels)];
            for (int nCurrentPixel = 0; nCurrentPixel < nNumOfPixels; nCurrentPixel++) {
                //oTopLine[nCurrentPixel] = new PointMap(new FastPointF(nTextureX, 0), oPixel);
                oPointMapInstance.To = oPixel;
                oFastPointFInstance.X = nTextureX;
                oPointMapInstance.From = oFastPointFInstance;
                oTopLine[nCurrentPixel] = oPointMapInstance;



                nTextureX += nTextureIncrementX;
                oPixel.X += nIncrementX;
                oPixel.Y += nIncrementY;
            }

            //bottom line
            nDeltaX = bottomRight.X - bottomLeft.X;
            nDeltaY = bottomRight.Y - bottomLeft.Y;
            nNumOfPixels = AbsMax(nDeltaX, nDeltaY);
            if (nNumOfPixels == 0)
                return;
            //nNumOfPixels *= 2;
            nIncrementX = nDeltaX / nNumOfPixels;
            nIncrementY = nDeltaY / nNumOfPixels;
            oPixel = new FastPointF(bottomLeft.X, bottomLeft.Y);


            nTextureIncrementX = (textureRect.Width / (nNumOfPixels));
            nTextureX = textureRect.Left;

            oPointMapInstance = new PointMap();
            oFastPointFInstance = new FastPointF(0, textureRect.Top + textureRect.Height);

            PointMap[] oBottomLine = new PointMap[(int) (nNumOfPixels)];

            for (int nCurrentPixel = 0; nCurrentPixel < nNumOfPixels; nCurrentPixel++) {
                //oBottomLine[nCurrentPixel] = new PointMap(new FastPointF(nTextureX, nTextureHeight), oPixel);

                oPointMapInstance.To = oPixel;
                oFastPointFInstance.X = nTextureX;
                oPointMapInstance.From = oFastPointFInstance;
                oBottomLine[nCurrentPixel] = oPointMapInstance;

                oPixel.X += nIncrementX;
                oPixel.Y += nIncrementY;
                nTextureX += nTextureIncrementX;
            }

            //cross lines
            PointMap[] oStartLine = oTopLine.Length >= oBottomLine.Length ? oTopLine : oBottomLine;
            PointMap[] oEndLine = oTopLine.Length >= oBottomLine.Length ? oBottomLine : oTopLine;
            float nFactor = (float) oEndLine.Length / (float) oStartLine.Length;

            Rectangle oBox = _ComputeBox(topLeft, topRight, bottomLeft, bottomRight);
            int nBoxX = oBox.X;
            int nBoxY = oBox.Y;
            Boolean[,] oPainted = new Boolean[oBox.Width + 1, oBox.Height + 1];

            for (int s = 0, nStartLineLength = oStartLine.Length; s < nStartLineLength; s++) {
                FastPointF oStart = oStartLine[s].To;
                FastPointF oStartTexture = oStartLine[s].From;
                int nEndPoint = (int) (nFactor * s);
                FastPointF oEnd = oEndLine[nEndPoint].To;
                FastPointF oEndTexture = oEndLine[nEndPoint].From;


                nDeltaX = oEnd.X - oStart.X;
                nDeltaY = oEnd.Y - oStart.Y;
                nNumOfPixels = AbsMax(nDeltaX, nDeltaY);
                //nNumOfPixels *= 2;
                nIncrementX = nDeltaX / nNumOfPixels;
                nIncrementY = nDeltaY / nNumOfPixels;

                float nTextureDeltaX = oEndTexture.X - oStartTexture.X;
                float nTextureDeltaY = oEndTexture.Y - oStartTexture.Y;
                nTextureIncrementX = nTextureDeltaX / (nNumOfPixels);
                float nTextureIncrementY = nTextureDeltaY / (nNumOfPixels);
                FastPointF oDestination = oStart;
                FastPointF oSource = oStartTexture;
                for (int nCurrentPixel = 0; nCurrentPixel < nNumOfPixels; nCurrentPixel++) {
                    RGBColor c = oTexture.GetPixel((int) oSource.X, (int) oSource.Y);
                    if (InterpolationActive)
                        c = Interpolate(oTexture, oSource);

                    oCanvas.SetPixel((int) oDestination.X, (int) oDestination.Y, c);
                    oPainted[(int) (oDestination.X - nBoxX), (int) (oDestination.Y - nBoxY)] = true;
                    oDestination.X += nIncrementX;
                    oDestination.Y += nIncrementY;
                    oSource.X += nTextureIncrementX;
                    oSource.Y += nTextureIncrementY;
                }
            }
            oCanvas.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            //paint missing pixels
            int nCanvasWidth = oCanvas.Width;
            int nCanvasHeight = oCanvas.Height;
            int nBoxHeight = oBox.Height;
            int nBoxWidth = oBox.Width;
            for (int ny = 0; ny < nBoxHeight; ny++)
                for (int nx = 0; nx < nBoxWidth; nx++) {
                    if (oPainted[nx, ny] == true)
                        continue;

                    int nNeigh = 0;
                    RGBColor oColor;
                    int R = 0;
                    int G = 0;
                    int B = 0;
                    if (nx > 0 && oPainted[nx - 1, ny] == true) {
                        oColor = oCanvas.GetPixel((nx + nBoxX) - 1, (ny + nBoxY));
                        R += oColor.R;
                        G += oColor.G;
                        B += oColor.B;
                        nNeigh++;
                    }
                    if (ny > 0 && oPainted[nx, ny - 1] == true) {
                        oColor = oCanvas.GetPixel((nx + nBoxX), (ny + nBoxY) - 1);
                        R += oColor.R;
                        G += oColor.G;
                        B += oColor.B;
                        nNeigh++;
                    }
                    if (nx < nCanvasWidth - 1 && oPainted[nx + 1, ny] == true) {
                        oColor = oCanvas.GetPixel((nx + nBoxX) + 1, (ny + nBoxY));
                        R += oColor.R;
                        G += oColor.G;
                        B += oColor.B;
                        nNeigh++;
                    }
                    if (ny < nCanvasHeight - 1 && oPainted[nx, ny + 1] == true) {
                        oColor = oCanvas.GetPixel((nx + nBoxX), (ny + nBoxY) + 1);
                        R += oColor.R;
                        G += oColor.G;
                        B += oColor.B;
                        nNeigh++;
                    }
                    if (nNeigh == 0)
                        continue;
                    oCanvas.SetPixel((nx + nBoxX), (ny + nBoxY), new RGBColor((byte) (R / nNeigh), (byte) (G / nNeigh), (byte) (B / nNeigh)));
                }


        }
        public static void DrawBitmap(FastBitmap oTexture, Point topLeft, Point topRight, Point bottomRight, Point bottomLeft, FastBitmap oCanvas) {
            DrawBitmap(oTexture, new Rectangle(0, 0, oTexture.Width, oTexture.Height), topLeft, topRight, bottomRight, bottomLeft, oCanvas);
        }
        public static float AbsMax(float val1, float val2) {
            if (val1 < 0)
                val1 *= -1;
            if (val2 < 0)
                val2 *= -1;
            return val1 > val2 ? val1 : val2;
        }
        private static RGBColor _Interpolate = RGBColor.White;
        private static RGBColor Interpolate(FastBitmap oTexture, FastPointF oSource) {
            double nSourceX = oSource.X;
            double nSourceY = oSource.Y;


            double nLeft = (int) (nSourceX);
            double nTop = (int) (nSourceY);
            double nRight = nLeft + 1;
            if (nRight >= oTexture.Width)
                nRight = nLeft;
            double nBottom = nTop + 1;
            if (nBottom >= oTexture.Height)
                nBottom = nTop;

            double nRightRatio = 1.0 - (nRight - nSourceX);
            double nBottomRatio = 1.0 - (nBottom - nSourceY);
            double nLeftRatio = 1.0 - nRightRatio;
            double nTopRatio = 1.0 - nBottomRatio;



            RGBColor oTopLeft = oTexture.GetPixel((int) nLeft, (int) nTop);
            RGBColor oTopRight = oTexture.GetPixel((int) nRight, (int) nTop);
            RGBColor oBottomLeft = oTexture.GetPixel((int) nLeft, (int) nBottom);
            RGBColor oBottomRight = oTexture.GetPixel((int) nRight, (int) nBottom);

            // Red
            byte nTopRed = (byte) ((oTopLeft.R * nLeftRatio) + (oTopRight.R * nRightRatio));
            byte nBottomRed = (byte) ((oBottomLeft.R * nLeftRatio) + (oBottomRight.R * nRightRatio));
            byte nRed = (byte) ((nTopRed * nTopRatio) + (nBottomRed * nBottomRatio));

            // Green
            byte nTopGreen = (byte) ((oTopLeft.G * nLeftRatio) + (oTopRight.G * nRightRatio));
            byte nBottomGreen = (byte) ((oBottomLeft.G * nLeftRatio) + (oBottomRight.G * nRightRatio));
            byte nGreen = (byte) ((nTopGreen * nTopRatio) + (nBottomGreen * nBottomRatio));

            // Blue
            byte nTopBlue = (byte) ((oTopLeft.B * nLeftRatio) + (oTopRight.B * nRightRatio));
            byte nBottomBlue = (byte) ((oBottomLeft.B * nLeftRatio) + (oBottomRight.B * nRightRatio));
            byte nBlue = (byte) ((nTopBlue * nTopRatio) + (nBottomBlue * nBottomRatio));

            _Interpolate.R = nRed;
            _Interpolate.G = nGreen;
            _Interpolate.B = nBlue;
            return _Interpolate;
        }
    }
}
