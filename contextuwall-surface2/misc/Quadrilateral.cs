﻿namespace System.Drawing.Drawing2D {
    // Summary:
    //     Stores a set of four integers that represent the location and size of a quadrilateral
    public struct Quadrilateral {
        public Point[] points;
        private Region _region;

        public SizeF bounds; // dimensions of bounding rect
        public Point location; // top-left of bounding rect
        public Point[] relativePoints; // points relative to top-left of bounding rect

        // Summary:
        //     Represents a Quadrilateral structure with its properties left
        //     uninitialized.
        public static readonly Quadrilateral Empty;

        //
        // Summary:
        //     Initializes a new instance of the Quadrilateral class with the
        //     specified location and size.
        //
        // Parameters:
        //   points:
        //      The four corner points of the quadrilateral, in the order top-left, top-right, bottom-left, bottom-right.
        //   graphics:
        //      The Graphics on which this quadrilateral will be drawn.
        public Quadrilateral(Point[] p, Graphics g) {
            this.points = new Point[] { p[0], p[1], p[2], p[3], p[0] };
            this._region = _createRegion(p);

            this.bounds = _region.GetBounds(g).Size;
            PointF rectLoc = _region.GetBounds(g).Location;
            this.location = new Point((int) rectLoc.X, (int) rectLoc.Y);
            this.relativePoints = new Point[]{
                new Point(p[0].X - location.X, p[0].Y - location.Y),
                new Point(p[1].X - location.X, p[1].Y - location.Y),
                new Point(p[2].X - location.X, p[2].Y - location.Y),
                new Point(p[3].X - location.X, p[3].Y - location.Y),
                new Point(p[0].X - location.X, p[0].Y - location.Y)
            };
        }

        private static Region _createRegion(Point[] points) {
            GraphicsPath gp = new GraphicsPath();
            gp.StartFigure();
            gp.AddLines(points);
            gp.CloseFigure();
            return new Region(gp);
        }

        //
        // Summary:
        //     Tests whether a given point is within the Quadrilateral.
        //
        // Parameters:
        //   p:
        //     The System.Drawing.Point to test.
        //
        // Returns:
        //     This method returns true if p is within the Quadrilateral's encompassing region;
        //     otherwise, false.
        public bool containsPoint(Point p) {
            return _region.IsVisible(p);
        }

        public static Quadrilateral fromRect(Rectangle r, Graphics g) {
            return new Quadrilateral(new Point[]{
                new Point(r.Left, r.Top),
                new Point(r.Right, r.Top),
                new Point(r.Right, r.Bottom),
                new Point(r.Left, r.Bottom)
            }, g);
        }
    }
}
